/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/


//#include <wx/notebook.h>
//#include <wx/valnum.h>


wxBEGIN_EVENT_TABLE(Options, wxDialog)

	// General
	EVT_DIRPICKER_CHANGED(DIR_PATH, Options::On_Dir_Path_Change)
	EVT_DIRPICKER_CHANGED(GIGS_PATH, Options::On_Gigs_Path_Change)
	EVT_CHECKBOX(GIGS_USE_FILES_PATH, Options::OnGigsUseFilePath)

	// OVerwrite velocity
//	EVT_DATAVIEW_ITEM_START_EDITING(VELOCITY_TRANSLATE, Options::OnVelocityTranslateEditing)
//	EVT_DATAVIEW_ITEM_EDITING_DONE(VELOCITY_TRANSLATE, Options::OnVelocityTranslateEditingEnd)
	
	EVT_BUTTON(OPTIONSSAVEASDEFAULT, Options::OnSave)
	EVT_BUTTON(OPTIONSRESTOREDEFAULTS, Options::OnLoad)
	EVT_BUTTON(OPTIONSCANCEL, Options::OnCancel)
	EVT_BUTTON(OPTIONSOK, Options::OnOK)
wxEND_EVENT_TABLE()


wxBEGIN_EVENT_TABLE(Save_SamplerFiles_progress, wxDialog)

	EVT_BUTTON(CREATECANCEL, Save_SamplerFiles_progress::OnCancel)
	EVT_BUTTON(CREATEOK, Save_SamplerFiles_progress::OnOK)
	
	EVT_IDLE(Save_SamplerFiles_progress::OnIdle)
	
	EVT_CLOSE(Save_SamplerFiles_progress::OnClose)
wxEND_EVENT_TABLE()

Options::Options()
       : wxDialog(NULL, wxID_ANY,  _("Options"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
{
	wxBoxSizer* Options_vSizer = new wxBoxSizer(wxVERTICAL);

	wxNotebook* OptionsBook;
		OptionsBook = new wxNotebook( this, wxID_ANY );
		
			// General
			wxPanel *GeneralPanel = new wxPanel( OptionsBook, wxID_ANY );
			
				wxBoxSizer* General_vSizer = new wxBoxSizer(wxVERTICAL);
						
					wxStaticBox *wav_files_box = new wxStaticBox(GeneralPanel, wxID_ANY, "&default wav-files directory:",
							wxDefaultPosition, wxDefaultSize );
					wxStaticBoxSizer* wav_files_Sizer = new wxStaticBoxSizer(wav_files_box, wxVERTICAL);	
						dir_path = new wxDirPickerCtrl(wav_files_box, DIR_PATH,
								temp_dir_path, "Select wav-files direcoty",
								wxDefaultPosition, wxDefaultSize, wxDIRP_USE_TEXTCTRL);
						dir_path->SetPath(temp_dir_path);
					wav_files_Sizer->Add(dir_path, 1, wxEXPAND|wxALL, 10);
					
						recursive = new wxCheckBox(wav_files_box, RECURSIVE, "Set 'recursive scan' as default",
							wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
						recursive->SetValue(options.recursive);
					wav_files_Sizer->Add(recursive, 0, wxGROW | wxLEFT, 10);

						hide_path = new wxCheckBox(wav_files_box, HIDE_PATHS, "Set 'hide path' as default",
							wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
						hide_path->SetValue(options.hide_path);
					wav_files_Sizer->Add(hide_path, 0, wxGROW | wxLEFT | wxBOTTOM, 10);
				General_vSizer->Add(wav_files_Sizer, 0, wxEXPAND|wxALL, 10 );
				
					wxStaticBox *gig_path_box = new wxStaticBox(GeneralPanel, wxID_ANY, "&directory for created gig-files:",
							wxDefaultPosition, 
							wxDefaultSize );
					wxStaticBoxSizer* gig_path_box_Sizer = new wxStaticBoxSizer(gig_path_box, wxVERTICAL);
						gigs_path = new wxDirPickerCtrl(gig_path_box, GIGS_PATH,
								temp_gigs_path, "Select gig-files direcoty",
								wxDefaultPosition, wxDefaultSize,
								wxDIRP_USE_TEXTCTRL);
						gigs_path->SetPath(temp_gigs_path);
						gigs_use_wav_directory = new wxCheckBox(gig_path_box, GIGS_USE_FILES_PATH, 
							"Use wav-files directory", wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
						gigs_use_wav_directory->SetValue(options.gigs_use_wav_directory);
						
						gigs_path->Enable(!gigs_use_wav_directory->GetValue());
	
						gig_path_box_Sizer->Add(gigs_path, 1, wxEXPAND|wxALL, 10);
						gig_path_box_Sizer->Add(gigs_use_wav_directory,	0, wxLEFT, 10 );
//						gig_path_box_Sizer->Add(0, 5, 0 );
						
						overwrite_gigs = new wxCheckBox(gig_path_box, OVERWRITEGIGS, 
							"Set 'overwrite gigs' as default", wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
						overwrite_gigs->SetValue(options.overwrite_gigs);
						gig_path_box_Sizer->Add(overwrite_gigs,	0, wxLEFT | wxBOTTOM, 10 );
				General_vSizer->Add(gig_path_box_Sizer,	0, wxEXPAND|wxALL, 10 );
	
					wxStaticBox *direct_scan_box = new wxStaticBox(GeneralPanel, wxID_ANY, "Direct Scan",
							wxDefaultPosition,wxDefaultSize );
					wxStaticBoxSizer* direct_scan_box_Sizer = new wxStaticBoxSizer(direct_scan_box, wxHORIZONTAL);
							direct_scan = new wxCheckBox(direct_scan_box, DIRECT_SCAN, 
									"Immediately scan for results when entering pattern",
								wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
							direct_scan->SetValue(options.direct_scan);
						direct_scan_box_Sizer->Add(direct_scan,	0, wxALL, 10 );
				General_vSizer->Add(direct_scan_box_Sizer, 0, wxALL, 10 );
				
					wxStaticBox *expand_ranges_box = new wxStaticBox(GeneralPanel, wxID_ANY, "Expand ranges",
							wxDefaultPosition,wxDefaultSize );
					wxStaticBoxSizer* expand_ranges_box_Sizer = new wxStaticBoxSizer(expand_ranges_box, wxHORIZONTAL);
							expand_ranges = new wxCheckBox(expand_ranges_box, EXPAND_RANGES, 
									"Set 'Expand ranges' as default for all Instruments",
								wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
							expand_ranges->SetValue(options.expand_all_ranges);
						expand_ranges_box_Sizer->Add(expand_ranges,	0, wxALL, 10 );
				General_vSizer->Add(expand_ranges_box_Sizer, 0, wxALL, 10 );
				
//					wxStaticBox *prefer_pattern_box = new wxStaticBox(GeneralPanel, wxID_ANY,
//						"Override wav-Info",
//						wxDefaultPosition, wxDefaultSize );
//					wxStaticBoxSizer* prefer_pattern_box_Sizer = new wxStaticBoxSizer(prefer_pattern_box, wxHORIZONTAL);
//					prefer_pattern = new wxCheckBox(GeneralPanel, PREF_PATTERN_TO_WAV_INFO,
//							"Prefer pattern-info to wav-Info",
//							wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
//					prefer_pattern->SetValue(options.overwride_wavInfo);
//					prefer_pattern_box_Sizer->Add(prefer_pattern, 1, wxEXPAND | wxALL, 10 );
//				General_vSizer->Add(prefer_pattern_box_Sizer, 0, wxALL, 10 );
			GeneralPanel->SetSizer(General_vSizer);
				
				
//			// Note
//			wxPanel *NotePanel = new wxPanel( OptionsBook, wxID_ANY );
//			wxBoxSizer* NotePanel_vSizer = new wxBoxSizer(wxVERTICAL);
//
//				// Note-Number
//				wxStaticBox *Transpose_box = new wxStaticBox(NotePanel, wxID_ANY,
//					"&Transpose:", wxDefaultPosition, wxDefaultSize );
//				wxStaticBoxSizer* Transpose_sSizer = new wxStaticBoxSizer(Transpose_box, wxHORIZONTAL);
//
//				Transpose_sSizer->Add(new wxStaticText( Transpose_box, wxID_ANY, "Transpose notes by:",
//						wxDefaultPosition, wxDefaultSize),
//					0, wxALL |wxALIGN_CENTER, 10 );
//
//				Transpose = new wxTextCtrl( Transpose_box, wxID_ANY,
//					std::to_string(options.transpose),
//					wxDefaultPosition, wxDefaultSize,
//					wxTE_PROCESS_ENTER);
//				Transpose->SetMaxLength(3);
//				Transpose_sSizer->Add(Transpose, 0, wxALL |wxALIGN_CENTER, 10 );
//
//				Transpose_sSizer->Add(new wxStaticText( Transpose_box, wxID_ANY, "halftones.",
//						wxDefaultPosition, wxDefaultSize),
//					0, wxALL |wxALIGN_CENTER, 10 );
//			NotePanel_vSizer->Add(Transpose_sSizer, 0, wxALL, 10 );
//
//				// Note-Name
//				wxStaticBox *s_box = new wxStaticBox(NotePanel, wxID_ANY,
//					"&Sign-strings for identifying note-signs using %a:",
//					wxDefaultPosition, wxDefaultSize );
//				wxStaticBoxSizer* Options_sSizer = new wxStaticBoxSizer(s_box, wxHORIZONTAL);
//
//					Options_sSizer->Add(new wxStaticText( s_box, wxID_ANY, "'Sharp'-sign:",
//							wxDefaultPosition,
//							wxDefaultSize),
//						0, wxALL |wxALIGN_CENTER, 10 );
//
//					s_sign_sharp = new wxTextCtrl( s_box, wxID_ANY,
//							options.s_sharp,
//							wxDefaultPosition, wxDefaultSize,
//							wxTE_PROCESS_ENTER);
//					Options_sSizer->Add(s_sign_sharp, 0, wxALL |wxALIGN_CENTER, 10 );
//
//					Options_sSizer->Add(new wxStaticText( s_box, wxID_ANY, "'Flat'-sign:",
//							wxDefaultPosition,
//							wxDefaultSize),
//						0, wxALL |wxALIGN_CENTER, 10 );
//
//					s_sign_flat = new wxTextCtrl( s_box, wxID_ANY,
//							options.s_flat,
//							wxDefaultPosition, wxDefaultSize,
//							wxTE_PROCESS_ENTER);
//					Options_sSizer->Add(s_sign_flat, 0, wxALL |wxALIGN_CENTER, 10 );
//			NotePanel_vSizer->Add(Options_sSizer, 0, wxALL, 10 );
//
//				wxStaticBox *v_box = new wxStaticBox(NotePanel, wxID_ANY,
//					"&Sign-strings for creating note-names from midi-note-numbers:",
//					wxDefaultPosition, wxDefaultSize );
//				wxStaticBoxSizer* Options_pvSizer = new wxStaticBoxSizer(v_box, wxVERTICAL);
//					wxBoxSizer* Options_pSizer = new wxBoxSizer(wxHORIZONTAL);
//						Options_pSizer->Add(new wxStaticText( v_box, wxID_ANY,
//								"'Sharp'-sign:",
//								wxDefaultPosition, wxDefaultSize),
//							0, wxALL | wxALIGN_CENTER_VERTICAL, 10 );
//
//						p_sign_sharp = new wxTextCtrl( v_box, wxID_ANY,
//								options.p_sharp,
//								wxDefaultPosition, wxDefaultSize,
//								wxTE_PROCESS_ENTER);
//						Options_pSizer->Add(p_sign_sharp, 0, wxALL | wxALIGN_CENTER, 10 );
//
//						Options_pSizer->Add(new wxStaticText( v_box, wxID_ANY,
//								"'Flat'-sign:",
//								wxDefaultPosition, wxDefaultSize),
//							0, wxALL | wxALIGN_CENTER_VERTICAL,	10 );
//
//						p_sign_flat = new wxTextCtrl( v_box, wxID_ANY,
//								options.p_flat,
//								wxDefaultPosition, wxDefaultSize,
//								wxTE_PROCESS_ENTER);
//						Options_pSizer->Add(p_sign_flat, 0, wxALL | wxALIGN_CENTER_VERTICAL,	10 );
//					Options_pvSizer->Add(Options_pSizer, 0, wxALL | wxALIGN_LEFT, 0 );
//					create_note_names = new wxCheckBox(v_box, CREATE_NOTE_NAMES,
//							"Always create note-names out of note-numbers.",
//							wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
//					create_note_names->SetValue(options.always_create_notenames);
//					Options_pvSizer->Add(create_note_names,	0, wxALL,	10 );
//			NotePanel_vSizer->Add(Options_pvSizer, 0, wxALL, 10 );
//			NotePanel->SetSizer(NotePanel_vSizer);
//
//
//			// Velocity
//			wxPanel *VelocityPanel = new wxPanel( OptionsBook, wxID_ANY );
//			wxBoxSizer* VelocityPanel_vSizer = new wxBoxSizer(wxVERTICAL);
//
//				wxStaticBox *velocity_translate_box = new wxStaticBox(VelocityPanel, wxID_ANY,
//						"&Translate velocity:", wxDefaultPosition, wxDefaultSize);
//				wxStaticBoxSizer* velocity_translate_box_Sizer = new wxStaticBoxSizer(velocity_translate_box,
//						wxVERTICAL);
//					velocity_translate = new wxDataViewListCtrl( velocity_translate_box, VELOCITY_TRANSLATE,
//							wxDefaultPosition, wxDefaultSize);//,wxDV_ROW_LINES, MidiValueValidator(&));
//					velocity_translate->AppendTextColumn( "find (e.g. 'forte')", wxDATAVIEW_CELL_EDITABLE );
//					velocity_translate->AppendTextColumn( "replace by (0 - 127)", wxDATAVIEW_CELL_EDITABLE);
//
//					for (auto vel_trans_it : options.velocity_translate) {
//						wxVector<wxVariant> data;
//						data.push_back( wxVariant(vel_trans_it.first) );
//						data.push_back( wxVariant(std::to_string(vel_trans_it.second)));
//						velocity_translate->AppendItem( data );
//					}
//					wxVector<wxVariant> data;
//					data.push_back( wxVariant("") );
//					data.push_back( wxVariant("") );
//					velocity_translate->AppendItem( data );
//				velocity_translate_box_Sizer->Add(velocity_translate, 1,
//						wxEXPAND | wxALL, 10 );
//				velocity_translate_box_Sizer->Add(
//						new wxStaticText(velocity_translate_box, wxID_ANY,
//								"Use '*' as wildcard for 'find' to enter a constant value.\n"
//								"This will be applied only if no other 'find'-string is found."),
//						0, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 15 );
////				velocity_translate_box_Sizer->Add(10,0,0);
//				VelocityPanel_vSizer->Add(velocity_translate_box_Sizer, 1, wxEXPAND | wxALL, 10 );
//			VelocityPanel->SetSizer(VelocityPanel_vSizer);

					
		OptionsBook->AddPage(GeneralPanel, "General");
//		OptionsBook->AddPage(NotePanel, "Note");
//		OptionsBook->AddPage(VelocityPanel, "Velocity");


	
		// Bottom-Buttons
		wxBoxSizer* Button_sizer = new wxBoxSizer( wxHORIZONTAL );
			wxButton* load_defaults = new wxButton( this, OPTIONSRESTOREDEFAULTS, "Load defaults");
			load_defaults->SetBitmap(wxArtProvider::GetBitmap(wxART_FILE_OPEN, wxART_BUTTON));
		Button_sizer->Add(load_defaults, 0, wxALL, 10 );				
			
			wxButton* save_as_default = new wxButton( this, OPTIONSSAVEASDEFAULT, "Save as default");
			save_as_default->SetBitmap(wxArtProvider::GetBitmap(wxART_FILE_SAVE, wxART_BUTTON));
		Button_sizer->Add(save_as_default, 0, wxALL, 10 );		
			
			wxButton* Cancel = new wxButton( this, OPTIONSCANCEL, "Cancel");
			Cancel->SetBitmap(wxArtProvider::GetBitmap(wxART_CROSS_MARK, wxART_BUTTON));
		Button_sizer->Add( Cancel, 0, wxALL, 10 );
			
			wxButton* ok = new wxButton( this, OPTIONSOK, "OK");
			ok->SetBitmap(wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON));
		Button_sizer->Add(ok , 0, wxALL, 10 );
			
	
	
	Options_vSizer->Add(OptionsBook, 1, wxEXPAND | wxALL,	10 );
	Options_vSizer->Add(Button_sizer, 0, wxEXPAND | wxALL, 10 );

	this->SetSizerAndFit(Options_vSizer);
}

//void Options::OnVelocityTranslateEditing(wxDataViewEvent& event) {
//	wxDataViewItem item = event.GetItem ();
//	int row	= velocity_translate->ItemToRow (item);
//	int col = event.GetColumn();
//	wxVariant variant;
//	velocity_translate->GetValue(variant, row, col);
//	if (row == velocity_translate->GetItemCount () - 1) {
//		wxVector<wxVariant> data;
//		data.push_back( wxVariant(""));
//		data.push_back( wxVariant("0") );
//		velocity_translate->AppendItem( data );
//	}
//}

//void Options::OnVelocityTranslateEditingEnd(wxDataViewEvent& event) {
//	if (event.IsEditCancelled()) return;
//	int col = event.GetColumn();
//	switch (col) {
//		case 0:
//			break;
//		case 1: {
//			std::string vel_string = event.GetValue().GetString().utf8_string();
//			bool is_int = true;
//			for (auto vel_string_it = vel_string.begin(); vel_string_it != vel_string.end();
//					++vel_string_it) {
//				if (!std::isdigit(*vel_string_it))
//					is_int = false;
//			}
//			if (!is_int)
//				event.Veto();
//			int vel = atoi(vel_string.c_str());
//			if ((vel < 0) or (vel > 127))
//				event.Veto();
//			break;
//		}
//	}
//}

void Options::OnGigsUseFilePath(wxCommandEvent& event) {
	gigs_path->Enable(!gigs_use_wav_directory->GetValue());
}

void Options::On_Dir_Path_Change(wxFileDirPickerEvent& event)
{
    temp_dir_path = event.GetPath();
}

void Options::On_Gigs_Path_Change(wxFileDirPickerEvent& event)
{
    temp_gigs_path = event.GetPath();
}

void Options::OnSave(wxCommandEvent& WXUNUSED(event)) {
	ApplyOptions();
	WriteOptions();
}

void Options::OnLoad(wxCommandEvent& WXUNUSED(event)) {
	GetOptions();
}

void Options::ApplyOptions() {
		// General
		options.dir_path = temp_dir_path;
		options.recursive = recursive->GetValue();
		options.hide_path = hide_path->GetValue();
		
		options.gigs_use_wav_directory = gigs_use_wav_directory->GetValue();
		if (options.gigs_use_wav_directory)
			options.gigs_path = options.dir_path;
		else
			options.gigs_path = temp_gigs_path;
		options.overwrite_gigs = overwrite_gigs->GetValue();
		
		options.direct_scan = direct_scan->GetValue();
		options.expand_all_ranges = expand_ranges->GetValue();
		
//		// Notes
//		options.s_sharp = s_sign_sharp->GetValue().utf8_string();
//		options.s_flat = s_sign_flat->GetValue().utf8_string();
//		options.p_sharp = p_sign_sharp->GetValue().utf8_string();
//		options.p_flat = p_sign_flat->GetValue().utf8_string();
//		options.always_create_notenames = create_note_names->GetValue();
//		options.overwride_wavInfo = prefer_pattern->GetValue();
//		options.transpose = atoi(Transpose->GetValue().c_str());
//
//		//Velocity
//		unsigned int rows = velocity_translate->GetItemCount ();
//		for (unsigned int i = 0; i < rows; i++) {
//			std::string find = velocity_translate->GetTextValue(i, 0).utf8_string();
//			if ( find != "")
//				options.velocity_translate[find] = atoi(velocity_translate->GetTextValue(i, 1).c_str());
//		}
}

void Options::OnOK(wxCommandEvent& WXUNUSED(event)) {
	ApplyOptions();
	Close(true);
}

void Options::OnCancel(wxCommandEvent& WXUNUSED(event)) {

	Close(true);
}



// Create Gigs Progress
Save_SamplerFiles_progress::Save_SamplerFiles_progress()
       : wxDialog(NULL, wxID_ANY,  _("Create Gigs"), wxDefaultPosition, 
    		   wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER) {
	wxBoxSizer* Progress_vSizer = new wxBoxSizer(wxVERTICAL);

		progresslog = new wxTextCtrl( this, PROGRESS_LOG, "",
				wxDefaultPosition, wxSize(480, 480), wxTE_MULTILINE);
		start_char = 0;
	Progress_vSizer->Add(progresslog, 1, wxGROW |	wxALL, 5 );

		progress_gauge = new wxGauge(this, PROGRESS_GAUGE, 1000,
			  wxDefaultPosition, wxDefaultSize);
		progress_gauge->SetValue(0);
	Progress_vSizer->Add(progress_gauge, 0, wxGROW |	wxALL, 5 );

	// OK_Exit_Buttons
	wxBoxSizer *Button_Box_sizer = new wxBoxSizer( wxHORIZONTAL );

			Cancel_Button = new wxButton( this, CREATECANCEL, "Stop");
			Cancel_Button->SetBitmap(wxArtProvider::GetBitmap(wxART_CROSS_MARK, wxART_BUTTON));
		Button_Box_sizer->Add(Cancel_Button, 0, wxALL | wxALIGN_CENTER |	wxALIGN_LEFT, 5 );

		Button_Box_sizer->Add(0,0,1);

			OK_Button = new wxButton( this, CREATEOK, "OK");
			OK_Button->SetBitmap(wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON));
		Button_Box_sizer->Add(OK_Button, 0, wxALL | wxALIGN_CENTER | wxALIGN_LEFT,	5 );

	Progress_vSizer->Add(Button_Box_sizer, 0, wxGROW | wxALL, 5 );

	this->SetSizerAndFit(Progress_vSizer);
}

void Save_SamplerFiles_progress::OnIdle(wxIdleEvent& event) {
	OK_Button->Enable(createprogress >= 1);
	Cancel_Button->Enable(createprogress < 1);
	if (start_char < createlog.size() ) {
		end_char = createlog.size();
		progresslog->AppendText(createlog.substr(start_char, end_char ));
		start_char = end_char;
	}
	progress_gauge->SetValue(createprogress * 1000);
}

void update_progress (float progress) {
	std::cout << "progress:" << progress << std::endl;
	progress_gauge->SetValue(progress * 1000);
}

void Save_SamplerFiles_progress::OnOK(wxCommandEvent& WXUNUSED(event)) {
	Close(true);
}

void Save_SamplerFiles_progress::OnCancel(wxCommandEvent& WXUNUSED(event)) {
	progresslog->AppendText("\n\n===== Aborting =====\n");
	stop_create_thread = true;
	Cancel_Button->Disable();
	while (createprogress < 1)
		sleep(1);
	OK_Button->Enable(true);
}

void Save_SamplerFiles_progress::OnClose(wxCloseEvent& event) {
	progresslog->AppendText("\n\n===== Stopping =====\n");
	stop_create_thread = true;
	Cancel_Button->Disable();
	OK_Button->Disable();
	while (createprogress < 1)
		sleep(1);
	Destroy();
}

// SpecOptions_Tmatrix


wxBEGIN_EVENT_TABLE(SpecOptions_Tmatrix, wxDialog)

	EVT_DATAVIEW_ITEM_EDITING_DONE(SOT_EDIT, SpecOptions_Tmatrix::StopEdit)
	EVT_DATAVIEW_ITEM_START_EDITING(SOT_EDIT, SpecOptions_Tmatrix::StartEdit)
	EVT_BUTTON(SO_CANCEL, SpecOptions_Tmatrix::OnCancel)
	EVT_BUTTON(SO_OK, SpecOptions_Tmatrix::OnOK)
wxEND_EVENT_TABLE()

SpecOptions_Tmatrix::SpecOptions_Tmatrix(SpecOptions_model* spon, unsigned int row)
       : wxDialog(mainframe, wxID_ANY,  _("T-Matrix"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
{
	spom = spon;
	so_row = row;
	wxBoxSizer* SO_Tmatrix_vSizer = new wxBoxSizer(wxVERTICAL);
		wxString sptype = spon->sp_type[row]; sptype = spon->sp_type[row];
		wxString title = "Translate " + sptype
				+ (sptype == "Dimension" ? " " + spon->dim_type[row] + ":" : ":");
		wxStaticBox *SO_Tmatrix_box = new wxStaticBox(this, wxID_ANY,
				title, wxDefaultPosition, wxDefaultSize);
		wxStaticBoxSizer* SO_Tmatrix_box_Sizer = new wxStaticBoxSizer(SO_Tmatrix_box,
				wxVERTICAL);

			wxAddRemoveCtrl* const AddRemove = new wxAddRemoveCtrl(SO_Tmatrix_box);

			TMatrix = new Tmatrix_model(SPtypeByName(spom->sp_type[row].utf8_string()));
			Tmatrix_ctrl = new wxDataViewCtrl(
					AddRemove,
					SOT_EDIT);
			Tmatrix_ctrl->AssociateModel( TMatrix.get() );

			Tmatrix_ctrl->AppendColumn(	// Set if specifier is active
					new wxDataViewColumn (
							"",
							new wxDataViewToggleRenderer("bool",
								wxDATAVIEW_CELL_ACTIVATABLE,
								wxALIGN_CENTER),
							0, 30, wxALIGN_CENTER,
							wxDATAVIEW_COL_RESIZABLE ));

			Tmatrix_ctrl->AppendColumn(
							new wxDataViewColumn (	// look for
							"look for",
							new wxDataViewTextRenderer("string", wxDATAVIEW_CELL_EDITABLE),
							1, wxCOL_WIDTH_DEFAULT, wxALIGN_LEFT,
							wxDATAVIEW_COL_RESIZABLE ));


			switch (SPtypeByName(spon->sp_type[row].utf8_string())) {
				case SP_SAMPLERFILE :
				case SP_INSTRUMENT :
				{
					Tmatrix_ctrl->AppendColumn(	// replace by
									new wxDataViewColumn (
									"translate to",
									new wxDataViewTextRenderer("string", wxDATAVIEW_CELL_EDITABLE),
									2, wxCOL_WIDTH_DEFAULT, wxALIGN_LEFT,
									wxDATAVIEW_COL_RESIZABLE ));
					break;
				}
				default : {
					Tmatrix_ctrl->AppendColumn(	// replace by
									new wxDataViewColumn (
									"translate to",
									new wxDataViewSpinRenderer(0,127, wxDATAVIEW_CELL_EDITABLE),
									2, wxCOL_WIDTH_DEFAULT, wxALIGN_LEFT,
									wxDATAVIEW_COL_RESIZABLE ));
					break;
				}
			}


			for (auto tm_it : spom->tmatrix[so_row]) {
//				long num;
//				tm_it.replace.ToLong(&num);
				TMatrix->Add(tm_it.active, tm_it.find, tm_it.replace);
			}
			TMatrix->Reset (TMatrix->Rows());
			Tmatrix_ctrl->SetMinSize(wxSize(200,80));
			class ListBoxAdaptor : public wxAddRemoveAdaptor
			{
			public:
				explicit ListBoxAdaptor(wxDataViewCtrl* Tmatrix_ctrl) : SOT(Tmatrix_ctrl) { }
				virtual wxWindow* GetItemsCtrl() const { return SOT; }
				virtual bool CanAdd() const { return true; }
				virtual bool CanRemove() const { return SOT->GetSelection() != NULL;}//wxNOT_FOUND; }
				virtual void OnAdd() {
					switch (t_SOT->sp_type) {
					case SP_SAMPLERFILE :
					case SP_INSTRUMENT :
						t_SOT->Add(true, "find", "replace");
						break;
					default :
						t_SOT->Add(true, "find", "64");
						break;
					}
					t_SOT->Reset (t_SOT->Rows());
//					changed = true;
				}
				virtual void OnRemove() {
					t_SOT->DeleteItem(SOT->GetSelection ());
					t_SOT->Reset (t_SOT->Rows());
//					changed = true;
				}
			private:
				wxDataViewCtrl* SOT;
				Tmatrix_model* t_SOT = (Tmatrix_model*) SOT->GetModel();
			};

			AddRemove->SetAdaptor(new ListBoxAdaptor(Tmatrix_ctrl));

		SO_Tmatrix_box_Sizer->Add(AddRemove, 1, wxEXPAND | wxALL, 10 );
	SO_Tmatrix_vSizer->Add(SO_Tmatrix_box_Sizer, 1, wxEXPAND | wxALL, 10 );
		// Bottom-Buttons
		wxBoxSizer* Button_sizer = new wxBoxSizer( wxHORIZONTAL );

			wxButton* Cancel = new wxButton( this, SO_CANCEL, "Cancel");
			Cancel->SetBitmap(wxArtProvider::GetBitmap(wxART_CROSS_MARK, wxART_BUTTON));
		Button_sizer->Add( Cancel, 0, wxALL, 10 );

			wxButton* ok = new wxButton( this, SO_OK, "OK");
			ok->SetBitmap(wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON));
		Button_sizer->Add(ok , 0, wxALL, 10 );

	SO_Tmatrix_vSizer->Add(Button_sizer, 0, wxEXPAND | wxALL, 10 );

	this->SetSizerAndFit(SO_Tmatrix_vSizer);
}

void SpecOptions_Tmatrix::OnOK(wxCommandEvent&) {
	unsigned int rows = TMatrix->Rows();
	spom->tmatrix[so_row].clear();
	for (unsigned int row = 0; row < rows; row++ ) {
		SpecOptions_model::tmatrix_struct tm{};
		tm.active = TMatrix->active[row];
		tm.find = TMatrix->find[row];
		tm.replace = TMatrix->replace[row];
		spom->tmatrix[so_row].push_back(tm);
	}

	if (changed) {
		wxCommandEvent event(RESET_EVENT, RESULT_RESET);
		mainframe->GetEventHandler()->ProcessEvent(event);
	}

    Close(true);
}
void SpecOptions_Tmatrix::OnCancel(wxCommandEvent&) {
	Close(true);
}

void SpecOptions_Tmatrix::StartEdit(wxDataViewEvent& event) {}

void SpecOptions_Tmatrix::StopEdit(wxDataViewEvent& event) {
	if (!event.IsEditCancelled())
		changed = true;
}


// SpecOptions_Note

wxBEGIN_EVENT_TABLE(SpecOptions_Note, wxDialog)
	EVT_RADIOBOX(wxID_ANY, SpecOptions_Note::use_wav_Changed)
	EVT_SPINCTRL(wxID_ANY, SpecOptions_Note::transpose_Changed)
	EVT_BUTTON(SO_CANCEL, SpecOptions_Note::OnCancel)
	EVT_BUTTON(SO_OK, SpecOptions_Note::OnOK)
wxEND_EVENT_TABLE()

SpecOptions_Note::SpecOptions_Note(SpecOptions_model* spon, unsigned int row)
       : wxDialog(NULL, wxID_ANY,  _("Note-Options"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
{
	spom = spon;
	so_row = row;
	wxBoxSizer* SO_Note_vSizer = new wxBoxSizer(wxVERTICAL);
		wxString sptype = spon->sp_type[row]; sptype = spon->sp_type[row];
		wxString title = "";
		wxStaticBox *SO_Note_box = new wxStaticBox(this, wxID_ANY,
				title, wxDefaultPosition, wxDefaultSize);
			wxStaticBoxSizer* SO_Note_box_Sizer = new wxStaticBoxSizer(SO_Note_box,	wxVERTICAL);

				wxArrayString GetNoteNumbers;
				GetNoteNumbers.Add("filenames  ");
				GetNoteNumbers.Add("wav-info\n(if available)");
				Override_wavInfo = new wxRadioBox(
						SO_Note_box,
						wxID_ANY,
						"Get notenumbers from:",
						wxDefaultPosition,
						wxDefaultSize,
						GetNoteNumbers);
				Override_wavInfo->SetSelection(options.overwride_wavInfo ? 0 : 1);
			SO_Note_box_Sizer->Add(Override_wavInfo, 1,	wxALL | wxEXPAND, 10);

				wxBoxSizer* Transpose_sizer = new wxBoxSizer (wxHORIZONTAL);
				Transpose_sizer->Add(new wxStaticText(
							SO_Note_box,
							wxID_ANY,
							"Transpose:"),
						0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 10);

					Transpose = new wxSpinCtrl (
							SO_Note_box,
							wxID_ANY,
							std::to_string(options.transpose),
							wxDefaultPosition, wxDefaultSize, wxSP_VERTICAL,
							-127, 127, 0, "Transpose");

				Transpose_sizer->Add(Transpose, 0, wxEXPAND | wxLEFT, 10);
			SO_Note_box_Sizer->Add(Transpose_sizer, 1, wxEXPAND | wxALL, 10 );


		SO_Note_vSizer->Add(SO_Note_box_Sizer, 1, wxEXPAND | wxALL, 10 );
		// Bottom-Buttons
		wxBoxSizer* Button_sizer = new wxBoxSizer( wxHORIZONTAL );

			wxButton* Cancel = new wxButton( this, SO_CANCEL, "Cancel");
			Cancel->SetBitmap(wxArtProvider::GetBitmap(wxART_CROSS_MARK, wxART_BUTTON));
		Button_sizer->Add( Cancel, 0, wxALL, 10 );

			wxButton* ok = new wxButton( this, SO_OK, "OK");
			ok->SetBitmap(wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON));
		Button_sizer->Add(ok , 0, wxALL, 10 );

		SO_Note_vSizer->Add(Button_sizer, 0, wxEXPAND | wxALL, 10 );

	this->SetSizerAndFit(SO_Note_vSizer);
}

void SpecOptions_Note::OnOK(wxCommandEvent&) {

	if (changed) {
		options.transpose = Transpose->GetValue();
		options.overwride_wavInfo = Override_wavInfo->GetSelection() == 0 ? true : false;
		WriteOptions();
		wxCommandEvent event(RESET_EVENT, RESULT_RESET);
		mainframe->GetEventHandler()->ProcessEvent(event);
	}

	Close(true);
}
void SpecOptions_Note::OnCancel(wxCommandEvent&) {
	Close(true);
}

void SpecOptions_Note::use_wav_Changed(wxCommandEvent& event) {
	changed = true;
}

void SpecOptions_Note::transpose_Changed(wxSpinEvent& event) {
	changed = true;
}

// SpecOptions_Notenames

wxBEGIN_EVENT_TABLE(SpecOptions_Notenames, wxDialog)
	EVT_TEXT(wxID_ANY, SpecOptions_Notenames::TextEdited)
	EVT_BUTTON(SO_CANCEL, SpecOptions_Notenames::OnCancel)
	EVT_BUTTON(SO_OK, SpecOptions_Notenames::OnOK)
wxEND_EVENT_TABLE()

SpecOptions_Notenames::SpecOptions_Notenames(SpecOptions_model* spon, unsigned int row)
       : wxDialog(NULL, wxID_ANY,  _("Notename-Options"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER)
{
	spom = spon;
	so_row = row;
	wxBoxSizer* SO_Notename_vSizer = new wxBoxSizer(wxVERTICAL);
		wxString sptype = spon->sp_type[row]; sptype = spon->sp_type[row];
		wxString title = "";
		wxStaticBox *SO_Notename_box = new wxStaticBox(this, wxID_ANY,
				title, wxDefaultPosition, wxDefaultSize);
		wxStaticBoxSizer* SO_Notename_box_Sizer = new wxStaticBoxSizer(SO_Notename_box,
				wxVERTICAL);

			wxStaticBox *s_box = new wxStaticBox(SO_Notename_box, wxID_ANY,
				"&Identify note-signs by:",
				wxDefaultPosition, wxDefaultSize );
			wxStaticBoxSizer* Options_sSizer = new wxStaticBoxSizer(s_box, wxHORIZONTAL);

				Options_sSizer->Add(new wxStaticText( s_box, wxID_ANY, "'Sharp'-sign:",
						wxDefaultPosition,
						wxDefaultSize),
					0, wxALL |wxALIGN_CENTER, 10 );

				find_sharp_sign = new wxTextCtrl( s_box, wxID_ANY,
						options.s_sharp,
						wxDefaultPosition, wxDefaultSize,
						wxTE_PROCESS_ENTER);
				Options_sSizer->Add(find_sharp_sign, 0, wxALL |wxALIGN_CENTER, 10 );

				Options_sSizer->Add(new wxStaticText( s_box, wxID_ANY, "'Flat'-sign:",
						wxDefaultPosition,
						wxDefaultSize),
					0, wxALL |wxALIGN_CENTER, 10 );

				find_flat_sign = new wxTextCtrl( s_box, wxID_ANY,
						options.s_flat,
						wxDefaultPosition, wxDefaultSize,
						wxTE_PROCESS_ENTER);
				Options_sSizer->Add(find_flat_sign, 0, wxALL |wxALIGN_CENTER, 10 );
		SO_Notename_box_Sizer->Add(Options_sSizer, 0, wxALL, 10 );

//		wxStaticBox *v_box = new wxStaticBox(SO_Notename_box, wxID_ANY,
//			"&Create note-names using:",
//			wxDefaultPosition, wxDefaultSize );
//		wxStaticBoxSizer* Options_pvSizer = new wxStaticBoxSizer(v_box, wxVERTICAL);
//			wxBoxSizer* Options_pSizer = new wxBoxSizer(wxHORIZONTAL);
//				Options_pSizer->Add(new wxStaticText( v_box, wxID_ANY,
//						"'Sharp'-sign:",
//						wxDefaultPosition, wxDefaultSize),
//					0, wxALL | wxALIGN_CENTER_VERTICAL, 10 );
//
//				replace_sharp_sign = new wxTextCtrl( v_box, wxID_ANY,
//						options.p_sharp,
//						wxDefaultPosition, wxDefaultSize,
//						wxTE_PROCESS_ENTER);
//				Options_pSizer->Add(replace_sharp_sign, 0, wxALL | wxALIGN_CENTER, 10 );
//
//				Options_pSizer->Add(new wxStaticText( v_box, wxID_ANY,
//						"'Flat'-sign:",
//						wxDefaultPosition, wxDefaultSize),
//					0, wxALL | wxALIGN_CENTER_VERTICAL,	10 );
//
//				replace_flat_sign = new wxTextCtrl( v_box, wxID_ANY,
//						options.p_flat,
//						wxDefaultPosition, wxDefaultSize,
//						wxTE_PROCESS_ENTER);
//				Options_pSizer->Add(replace_flat_sign, 0, wxALL | wxALIGN_CENTER_VERTICAL,	10 );
//			Options_pvSizer->Add(Options_pSizer, 0, wxALL | wxALIGN_LEFT, 0 );
//			create_note_names = new wxCheckBox(v_box, CREATE_NOTE_NAMES,
//					"Always create note-names (from note-numbers).",
//					wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
//			create_note_names->SetValue(options.always_create_notenames);
//			Options_pvSizer->Add(create_note_names,	0, wxALL,	10 );
//		SO_Notename_box_Sizer->Add(Options_pvSizer, 0, wxALL, 10 );
//	SO_Notename_vSizer->Add(Options_pvSizer, 0, wxALL, 10 );
//	SO_Notename_box->SetSizer(SO_Notename_vSizer);

//			SO_Note_box_Sizer->Add(Override_wavInfo_sizer, 1, wxEXPAND | wxALL, 10 );

	SO_Notename_vSizer->Add(SO_Notename_box_Sizer, 1, wxEXPAND | wxALL, 10 );
		// Bottom-Buttons
		wxBoxSizer* Button_sizer = new wxBoxSizer( wxHORIZONTAL );

			wxButton* Cancel = new wxButton( this, SO_CANCEL, "Cancel");
			Cancel->SetBitmap(wxArtProvider::GetBitmap(wxART_CROSS_MARK, wxART_BUTTON));
		Button_sizer->Add( Cancel, 0, wxALL, 10 );

			wxButton* ok = new wxButton( this, SO_OK, "OK");
			ok->SetBitmap(wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON));
		Button_sizer->Add(ok , 0, wxALL, 10 );

		SO_Notename_vSizer->Add(Button_sizer, 0, wxEXPAND | wxALL, 10 );

	this->SetSizerAndFit(SO_Notename_vSizer);
}

void SpecOptions_Notenames::OnOK(wxCommandEvent&) {
	if (changed) {
		options.s_sharp = find_sharp_sign->GetLineText(0);
		options.s_flat = find_flat_sign->GetLineText(0);
	//	options.p_sharp = replace_sharp_sign->GetLineText(0);
	//	options.p_flat = replace_flat_sign->GetLineText(0);
	//	options.always_create_notenames = create_note_names->GetValue();
		WriteOptions();
		wxCommandEvent event(RESET_EVENT, RESULT_RESET);
		mainframe->GetEventHandler()->ProcessEvent(event);
	}

	Close(true);
}
void SpecOptions_Notenames::OnCancel(wxCommandEvent&) {
	Close(true);
}

void SpecOptions_Notenames::TextEdited(wxCommandEvent& event) {
	changed = true;
}





