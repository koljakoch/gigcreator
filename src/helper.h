/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/


enum SAMPLER_TYPE {
	SAMPLER_TYPE_NONE = 0,
	SAMPLER_TYPE_GIG
};


enum SPTYPE {
	SP_NONE = 0,
	SP_FILENAME,
	SP_SAMPLERFILE,
	SP_INSTRUMENT,
	SP_NOTE,
	SP_NOTENAME,
	SP_REGION,
	SP_DIMENSION,
	SP_STEREO,
	SP_IGNORE,
	SP_SAMPLER
};



bool IsMidiValue(std::string value);
short int get_note_nr(std::string notename);
short get_channel (std::string to_stereo);





// A NOTE_NAME consists of "note[sign]octave"
//const std::string NOTES[] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

