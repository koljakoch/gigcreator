## Project
- Create github-Account
- Create 'clean' folder, link required files/structure to there
- Create GigCreator there
- Write Docs
  - Features
  - Revisioning
  - Manual
  - README
- Archlinux PKGBUILD?
- Test on Ubuntu, PPA?
- Correct code-headings
- GPL

## GigCreator
- ?make fa_list a map by filename in order to keep warnings?
+ Commandline option for dir_path
+ Button Icons
+ Shutdown memoryleak?
? Always create notenames doesnt work when always prefer pattern info is set
+ range not corrext when no notenumber in pattern
+ Note-Velocity-conflict not shown? <- Might get overwritten by later spec-patterns
+ Function of Creating-Buttons
+ rename to gigCreator
? don't clear preview on specinput if no file or sample is selected
- direct scan when program started
+ Create Gigs: show progress
- Options: Add stereo-characters (and apply length to pattern-spec, no need to enter it -> override)
+ !!! exception in Pattern-Input!!!! 
++ when pattern deleted to 0
++ Preview -> no output when pattern goes to the last character in filename
+ Add failure-reason to wav-Info
? tooltip
- Data-model: implement notes and velocities seperately, print ranges
- Options:
-+ General
-++ Expand Ranges as default
-++ default files-path
-++ Select path to save gigs
-+++ Use selected files-path
-+ Samples
-++ Prefer wavInfo to pattern-info (only if both are available)
-?+ override velocity, note with fixed value
-?+ search/print-pattern for %s
-?+ Always create note-name (from taken note-number parameter: wavInfo or gc)
++ restore (saved) defaults
-? remove saved defaults -> hardcoded defaults on next startup
++ save as default
+ show gigs-path
? option to add gigs to linuxsampler-library ?
? eventually move data-model into gigcontainer
+ Overwrite existing gig-files
+ always cut base-path from file-list
+ always cut .wav from file-list
+ Rename gigs, instruments
? make Overwrite-Velocity for all gigs to Overwrite-Options including range
? rename %m to %i, %i to %o?
+ List Gig-Samples as part of gig
+ gig-list as tree-structure, expandeble instruments, Nr of intr. in gig-Name-Inst.-Column
? gig-list and Sample-list multiselectable to remove (right-click)
+ Remove help-Button, move into help-menue
- help-texts as ?string-constants?, to reuse for tool-tips
- translation
+ Implement Preferences = Options
- Save/Load/Remove Spec-Patterns?
? Make files in files-list selectable (use first selected for preview)
+ App-Title
+ App-Icon
- Interface for error-handling
+ Log-window when creating gigs (textctrl)

## PrepareGig
+ implement Instrument-structure
+ organize samples in groups
- check/correct public vs. private
- remove unneeded symbols from gig
- Interface for error-handling

## CreateGig
- 24 bit
- NoteNames
- implement checks, tests
- cleanup
+ expand ranges
+ create instruments based on Name2
- Interface for error-handling

## CLI
+ not applicable anymore

