/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/



	// none_Sampler definition
	struct Region {
		sp_text notename = "C3";
		sp_midival note = 60;
		sp_midival range_low = 60;
		sp_midival range_high = 60;
	};
	struct Instrument {
		sp_text name = "unnamed";
		std::list <Region*> Regions;
	};
	struct SamplerFile {
		sp_text name = "unnamed";
		std::list<Instrument*> Instruments;
	};

class SamplerCreator::Sampler_class {
private:
//	static void call_CreateSamplerFiles(void* ptr) {
//		((SamplerCreator::Sampler_class*)ptr)->_CreateSamplerFiles();
//	}

public:

	enum NODETYPE {
		SC_INVALID = 0,
		SCSC_FILE,
		SCSC_INSTRUMENT,
//		SCGIG_GIG,
//		SCGIG_INSTRUMENT
	};

	SAMPLER_TYPE type = SAMPLER_TYPE_NONE;

	std::list<SamplerFile*>* SamplerFiles = new std::list<SamplerFile*>;
	SamplerDisplay* SC_SamplerDisplayModel{};// = new SamplerDisplay{};
//	uint8_t* progress = new uint8_t(0);
//	std::string* log = new std::string("");
//	uint8_t* progress;
//	std::string* log;
//	std::string* test = new std::string("test");

	Sampler_class (
			SAMPLER_TYPE tstype,
			SamplerResult* sresult,
			SamplerDisplay* SamplerDisplayModel) {
//			uint8_t* sprogress,
//			std::string* slog) {
		type = tstype;
		sresult->samplerFiles = static_cast<void*>(SamplerFiles);
		SC_SamplerDisplayModel = SamplerDisplayModel;
//		progress = sprogress;
//		log = slog;
	};

	Sampler_class (
			SAMPLER_TYPE tstype,
			SamplerDisplay* SamplerDisplayModel) {
		type = tstype;
		SC_SamplerDisplayModel = SamplerDisplayModel;
	};

	virtual bool evaluate_sample(SamplerCreator::Sample*, sp_text* ret);
	virtual sp_text GetInfo (std::pair<int, void*> nodepair);
	virtual wxPanel* NodeCtrl (wxPanel* parent, std::pair<int, void*> nodepair) {return NULL;};
//	static bool CreateSampler_Files(uint8_t* progress, std::string* log);
//	static void CreateSamplerFiles(SamplerCreator::Sampler_class s);
	virtual bool SaveSingleSamplerFile();
	virtual void SaveSamplerFiles(float* progress, std::string* log);
	std::string getWavInfo(sp_file* filename, SF_INFO* sfinfo, SF_INSTRUMENT* sfinst);
	sp_text GetSampleValueText(SPTYPE sptype, Sample* sample) {
		for (auto& spv : *sample) {
			if (spv.first->type == sptype) {
				return *(static_cast<sp_text*>(spv.second));
			}
		}
		return "";
	}
	sp_midival GetSampleValueMidival(SPTYPE sptype, Sample* sample) {
		for (auto& spv : *sample) {
			if (spv.first->type == sptype) {
				return *(static_cast<sp_midival*>(spv.second));
			}
		}
		return -1;
	}
	virtual std::string CreateSamplerDisplayModel ();
	virtual bool IsEditable(std::pair<int, void*> DataLink) const { return true; }
	virtual void SetNodeName (std::pair<int, void*> nodepair, wxString name) {} ;
	virtual void SDNodeCommand (int id, wxDataViewItem item, wxDataViewItemArray* changed_items) {};
	virtual void UpdateSamplerDisplayNode(SamplerDisplay_node *node) {};
	virtual void ExpandAllRanges () {};
};

std::string SamplerCreator::Sampler_class::getWavInfo
(sp_file* filename, SF_INFO* sfinfo, SF_INSTRUMENT* sfinst) {
	SNDFILE* hFile = sf_open(filename->c_str(), SFM_READ, sfinfo);
//	std::string ret = "";
	if (!hFile)
		return "\n== Could not open input wav file \n" + *filename + "\" ==";

	switch (sfinfo->channels) {
		case 1:
		case 2:
			break;
		default:
			return "\n== " + std::to_string(sfinfo->channels) + " "
					"audio channels in WAV file \"" + *filename +
					"\"; this is not supported! ==";
	}

	bool hasSfInst = (sf_command(hFile, SFC_GET_INSTRUMENT,
			sfinst, sizeof(*sfinst)) != SF_FALSE);
	sf_close(hFile);

	if (!hasSfInst)
		return "NoSfInst";

	return "";
}
bool SamplerCreator::Sampler_class::evaluate_sample
(SamplerCreator::Sample* sample, sp_text* ret) {

	SamplerDisplay_node* SD_node;
	SamplerFile* pSF = NULL;
	Instrument* pI = NULL;

	sp_text instrument_name;
	sp_text samplerfile_name = GetSampleValueText(SP_SAMPLERFILE, sample);
	if (samplerfile_name != "") { // SP_SAMPLERFILE found
		for (auto& sF : *SamplerFiles) { // find existing SamplerFile
			if (sF->name == samplerfile_name) { // SamplerFile found
				pSF = sF;
				SD_node = SC_SamplerDisplayModel->GetNode(pSF);
				std::cout << "EvalSample: found:" << SD_node->m_Name.utf8_string() << std::endl;
				instrument_name = GetSampleValueText (SP_INSTRUMENT, sample);
				if (instrument_name !="") { // SP_INSTRUMENT found
					for (auto& inst_it : sF->Instruments) { // find existing instrument
						if (inst_it->name == instrument_name) { // Instrument found
							pI = inst_it;
						}
					}
				} else {
					*ret = "\n=== Could not find Instrument Info ===";
					return false; // no Instrument-info in Sample
				}
			}
		}
	} else return "\n=== Could not find SamplerFile-Info ==="; // no SamplerFile-info in Sample

	if (pSF == NULL) { // create new SamplerFile ...
		pSF = new SamplerFile{};
		pSF->name = samplerfile_name;
		SamplerFiles->push_back(pSF);

		// ... and SamplerDisplay-node
		SD_node = new SamplerDisplay_node(
				NULL,
				samplerfile_name,
				std::pair<int, SamplerFile*>(SCSC_FILE, pSF),
				true);
		SC_SamplerDisplayModel->m_SamplerDisplayNodes.push_back(SD_node);
		std::cout << "EvalSample: created:" << SD_node->m_Name.utf8_string() << std::endl;
	}

	if (pI == NULL) { // create new Instrument ...
		instrument_name = GetSampleValueText (SP_INSTRUMENT, sample);
		pI = new Instrument{};
		pI->name = instrument_name;
		pSF->Instruments.push_back(pI);

		// ... and SamplerDisplay-node
		SD_node->Append(
				new SamplerDisplay_node (
						SD_node,
						instrument_name,
						std::pair<int, Instrument*>(SCSC_INSTRUMENT, pI),
						false)
				);
	}
	// End process SamplerFile-structure.
	// found/created SamplerFile and Instrument are in pSF and pI
	return true;
}
std::string SamplerCreator::Sampler_class::CreateSamplerDisplayModel () {
	return "CreateSamplerSDModel";
}
sp_text SamplerCreator::Sampler_class::GetInfo (std::pair<int, void*> nodepair) {
	sp_text ret = "";
	int nodetype = nodepair.first;

	switch (nodetype) {
		case SCSC_FILE : {
			SamplerFile* sf = static_cast<SamplerFile*>(nodepair.second);
			ret = sf->name;
			ret.append("\nInstrument: " + std::to_string(sf->Instruments.size()));
			return ret;
		}
		case SCSC_INSTRUMENT : {
					Instrument* si = static_cast<Instrument*>(nodepair.second);
					ret = si->name;
					ret.append("\nRegions: " + std::to_string(si->Regions.size()));
					return ret;
		}
		default : break;
	}
	return ret;
}
bool SamplerCreator::Sampler_class::SaveSingleSamplerFile() {
	return true;
}
void SamplerCreator::Sampler_class::SaveSamplerFiles(float* progress, std::string* log) {
//	std::cout << "CreateSampler" << std::endl;
	*log = "=== Start creating SamplerFiles ===\n";
	int nr_SamplerFiles = SamplerFiles->size();
	int a = 0;
	for (auto& SF_it : *SamplerFiles) {
		*progress = 100 * a / nr_SamplerFiles;
		log->append("\n" + std::to_string(a + 1) + ": " + SF_it->name);
		usleep(5000);
		a++;
	}
	*progress = 100;
	log->append("\n\n=== Done ===");

//	return true;
}


class SamplerCreator::gig_sampler : public SamplerCreator::Sampler_class {
public:
	enum NODETYPE {
//		SCSC_FILE = 0,
//		SCSC_INSTRUMENT,
		SC_INVALID = 0,
		SC_NONE,
		SCGIG_GIG,
		SCGIG_INSTRUMENT,
		SCGIG_INSTRUMENTS,
		SCGIG_REGION,
		SCGIG_SAMPLE,
		SCGIG_SAMPLES,
		SCGIG_DIMENSION,
		SCGIG_DIMENSIONS,
		SCGIG_DIMENSIONREGION
	};

	// Intermediate gig-Files-Container
	struct interSFcontainer {
		struct SamplerFile_class {
			struct Sample_class {
				const NODETYPE type = SCGIG_SAMPLE;
				interSFcontainer::SamplerFile_class* parent;
				sp_file* file = NULL;
				int Channels = 0;
				int SamplesPerSecond = 0;
				sp_midival BitDepth = 16;
				sp_midival FrameSize(){
					return Channels * BitDepth / 8;
				}
				sf_count_t frames = 0;
				sp_midival MIDIUnityNote = 60;
				sp_midival FineTune = 0;
				sp_midival Loops = 0;
				gig::loop_type_t LoopType = gig::loop_type_normal;
				long int LoopStart = 0;
				long int LoopEnd = 0;
				uint32_t LoopPlayCount = 0;
				long int LoopSize = 0;

				sp_text name(){
					sp_text ret = "";
					// iterate through all instruments/regions/dimensions/dimensionregions to get all infos
					bool found = false;
					for (auto& instr : parent->Instruments) {
						bool i_found = false;
						for (auto& reg : instr->Regions) {
							bool r_found = false;
							for (auto& dim : reg->Dimensions ) {
								bool d_found = false;
								for (auto& dimreg : dim.second->DimRegs) {
									bool sam_found = false;
									for (auto& sam : dimreg.second) {
										if (sam == this) {
											sam_found = true;
											if (dim.first == gig::dimension_velocity)
												ret.append("_Vel:" + std::to_string(dimreg.first));
										}
									}
									if (sam_found)
										d_found = true;
								}
								if (d_found) {
									r_found = true;
								}
							}
							if (r_found) {
								i_found = true;
								break;
							}
						}
						if (i_found) {
							ret.insert(0, instr->name + "_Note:"
									+ std::to_string(this->MIDIUnityNote));
							found = true;
							break;
						}
					}
					if (!found)
						ret = "Note:" + std::to_string(this->MIDIUnityNote);
					return ret;
				}
				sp_text info = "";
				gig::Sample* link = NULL;
				SamplerDisplay_node* SD_link{};
			};
			struct Instrument_class {
				struct Region_class {
					struct Dimension_class {
//						struct DimensionRegion {
//							const NODETYPE type = SCGIG_DIMENSIONREGION;
//							std::list<Sample_class*> samples;
//						};
						const NODETYPE type = SCGIG_DIMENSION;
						interSFcontainer::SamplerFile_class::Instrument_class::Region_class* parent;
						gig::dimension_t dim = gig::dimension_none;
						SamplerDisplay_node* SD_link{};
//						sp_midival value{};
						std::map<sp_midival, std::set<Sample_class*> > DimRegs{}; // value -> sample
					};
					const NODETYPE type = SCGIG_REGION;
					interSFcontainer::SamplerFile_class::Instrument_class* parent;
//					sp_midival basenote = 60;
					sp_midival low = 60;
					sp_midival high = 60;
					sp_text info = "";
					gig::Region* link;
					SamplerDisplay_node* SD_link{};
					std::map<gig::dimension_t, Dimension_class*> Dimensions{};
//					std::map<Sample_class*,
//						std::map<gig::dimension_t, sp_midival> > SampleDimRgn;
//					std::map<gig::dimension_t,
//						std::map<sp_midival, Sample_class*> > DimRgnSample{};
//					sp_text samplenameprefix(interSFcontainer::SamplerFile_class::
//							Instrument_class::Region_class::Sample_class* sample) {
//						sp_text ret = parent->name;
//						for (auto dim : Dimensions)
//							if (dim.first != gig::dimension_none)
//								for (auto dimreg : dim.second->DimRegs)
//									for (auto sam : dimreg.second)
//										if (sam == sample) {
//											ret.append("_" + std::to_string(dim.first) + ":"
//												+ std::to_string(dimreg.first));
//											break;
//										}
//						return ret;
//					}
				};
				const NODETYPE type = SCGIG_INSTRUMENT;
				interSFcontainer::SamplerFile_class* parent;
				sp_text name = "Instrument-name";
				sp_text info = "";
				gig::Instrument* link{};
				SamplerDisplay_node* SD_link{};
				struct R_Compare {
					bool operator() (Region_class* a, Region_class* b) {
						return a->low < b->low;
					}
				};
				std::set<Region_class*, R_Compare> Regions{};
			};
			const NODETYPE type = SCGIG_GIG;
			sp_text name = "gig-name";
			sp_text info = "";
			gig::File* link{};
			SamplerDisplay_node* SD_link{};
			struct S_Compare {
				bool operator() (Sample_class* a, Sample_class* b) {
					return a->name() < b->name();
				}
			};
			std::list<Sample_class*> Samples{};
			struct I_Compare {
				bool operator() (Instrument_class* a, Instrument_class* b) {
					return a->name < b->name;
				}
			};
			std::set<Instrument_class*, I_Compare> Instruments{};
		};
		sp_text name = "gig-Files";
		std::set <SamplerFile_class*> SamplerFiles{};
	};
	typedef interSFcontainer container_t;
	typedef interSFcontainer::SamplerFile_class samplerfile_t;
	typedef interSFcontainer::SamplerFile_class::Instrument_class
			instrument_t;
	typedef std::set<instrument_t*, interSFcontainer::SamplerFile_class::I_Compare>
			instruments_t;
	typedef interSFcontainer::SamplerFile_class::Instrument_class::
			Region_class region_t;
	typedef interSFcontainer::SamplerFile_class::Instrument_class::
			Region_class::Dimension_class dimension_t;
	typedef interSFcontainer::SamplerFile_class::Sample_class sample_t;
	typedef std::list<sample_t*> samples_t;
	typedef std::pair<sp_midival, std::set<sample_t*> > dimresionregions_t;

	interSFcontainer* SFcontainer{};
	gig_sampler (
			SAMPLER_TYPE tstype,
			SamplerResult* sresult,
			SamplerDisplay* SamplerDisplayModel)
				: SamplerCreator::Sampler_class (
					tstype,
					SamplerDisplayModel) {
		SFcontainer = new interSFcontainer;
		sresult->samplerFiles = static_cast<void*>(SFcontainer);
	}
	static bool comapare_samples (sample_t* a, sample_t* b);

	bool evaluate_sample(SamplerCreator::Sample* sample, sp_text* ret);

	struct sample_created {
		sample_t* sf_sample = NULL;
		bool saved = false;
		gig::Sample* g_sample = NULL;
	};
	typedef std::map<sp_midival, sample_t*> midival_sample;
	typedef std::pair<gig::dimension_t, midival_sample> dimregs_pair_t;
	typedef std::list<dimregs_pair_t> dimregs_t;
	typedef std::map<gig::Sample*, std::set<sample_t*> > to_be_saved_samples_t;

	bool CreateDimRgnsSamples (gig::File* gig,
			gig::Region* reg,
	//		sample_t sam,
			gig::Group* group, uint8_t* iDimBits,
			std::map<gig::dimension_t, dimension_t*>* Dimensions,
			std::map<gig::dimension_t, dimension_t*>::iterator dim_it,
			std::map<gig::dimension_t, dimension_t*>::iterator stereo_dim_it,
//			std::set< sample_t*> DimRegs_Samples,
			samples_t DimRegs_Samples,
			sp_midival velocity,
			to_be_saved_samples_t* tbss);
	bool SaveSingleSamplerFile(samplerfile_t* sf, int gig_nr, int nr_gigs,
			float* progress, std::string* log);
	void SaveSamplerFiles(float* progress, std::string* log);
	gig::Sample* create_gig_sample (SamplerCreator::Sample* sample);
	sp_text SampleInfo (sample_t*);
	sp_text GetInfo (std::pair<int, void*> nodepair);
	wxPanel* NodeCtrl (wxPanel* parent, std::pair<int, void*> nodepair);
	std::string CreateSamplerDisplayModel ();
    bool IsEditable(std::pair<int, void*> DataLink) const {
		int nodetype = DataLink.first;
		switch (nodetype) {
			case SCGIG_GIG :
			case SCGIG_INSTRUMENT :
				return true;
			default: return false;
		}
    }
    void SetNodeName (std::pair<int, void*> nodepair, wxString name);
    void SDNodeCommand (int id, wxDataViewItem item, wxDataViewItemArray* changed_items);
    void Ranges (bool expand, instrument_t* instr);
    void ExpandAllRanges ();
    void UpdateSamplerDisplayNode(SamplerDisplay_node *node);
};

bool SamplerCreator::gig_sampler::evaluate_sample
(SamplerCreator::Sample* sample, sp_text* ret) {

	interSFcontainer::SamplerFile_class* pGig = NULL;
	interSFcontainer::SamplerFile_class::Instrument_class* pIns = NULL;
	interSFcontainer::SamplerFile_class::Instrument_class::Region_class* pReg = NULL;


	sp_file* filename = sample->get_filename();
//	std::cout << "\n\n=========" << *filename << "===============" << std::endl;
	sp_midival note = -1;

	SF_INFO* sfinfo = new SF_INFO{};
	SF_INSTRUMENT* sfinst = new SF_INSTRUMENT{};
	std::string wavinfo = getWavInfo(filename, sfinfo, sfinst);
	if (wavinfo != "" and wavinfo != "NoSfInst") {
		*ret = wavinfo;
		return false;
	}

	sp_text s_info = ""; // sample-info

	sp_text samplerfile_name = "unnamed";
	sp_text instrument_name = "unnamed";
//	for (auto& gig : SFcontainer.SamplerFiles) {
//
//	}

	// Compare against existing gig-files
	samplerfile_name = GetSampleValueText (SP_SAMPLERFILE, sample);
	if (samplerfile_name =="")
		samplerfile_name = options.GIG.Instrument;
	if (SFcontainer->SamplerFiles.size() > 0) {
		for (auto gig : SFcontainer->SamplerFiles) { // find existing SamplerFile
			if (gig->name == samplerfile_name) { // SamplerFile found
				pGig = gig;
			}
		}
	}

	if (pGig == NULL) { // no gig found ==> create one
		pGig = new interSFcontainer::
				SamplerFile_class;
		pGig->name = samplerfile_name;
		// and add it to intSFcontainer
		SFcontainer->SamplerFiles.insert(pGig);
	}



//	std::cout << "Compare gig-instruments:" << std::endl;
	// Compare against existing gig-Instruments
	instrument_name = GetSampleValueText (SP_INSTRUMENT, sample);
	if (instrument_name =="")
		instrument_name = options.GIG.Instrument;
	if (pGig->Instruments.size() > 0) {
		for (auto inst : pGig->Instruments) { // find existing instrument
			if (inst->name == instrument_name) { // Instrument found
				pIns = inst;
			}
		}
	}

	if (pIns == NULL) { // no Instrument found ==> create one and add to gig
		pIns = new interSFcontainer::
				SamplerFile_class::
					Instrument_class;
		pIns->name = instrument_name;
		pIns->parent = pGig;
		pGig->Instruments.insert(pIns);
		}



//	std::cout << "Compare gig-regions:" << std::endl;
	// Compare against existing Regions
	if (!options.overwride_wavInfo && wavinfo !="NoSfInst") {
		note = sfinst->basenote;
		s_info.append("\nThe basenote from wav-info is used.");
	}
	else
		note = GetSampleValueMidival(SP_NOTE, sample);
	if (note == -1) {
		*ret = "\n=== No note found ===";
		return false;
	}
	if (options.transpose != 0) {
		note += options.transpose;
		s_info.append("\nApplied transpose: ");
		s_info.append(options.transpose > 0 ? "+" : "");
		s_info.append(std::to_string(options.transpose));
	}

	if (pIns->Regions.size() > 0) {
		for (auto reg : pIns->Regions) {
			if (reg->low <= note && note <= reg->high) {
				pReg = reg;
//				std::cout << "\tFound: " << (int) reg->low << ":" << (int) reg->high << std::endl;
			}
		}
	}

	if (pReg == NULL) {	// no Region found ==> create one and add to Instrument
		pReg = new region_t{};
		pReg->low = pReg->high = note;
		pReg->parent = pIns;
		pIns->Regions.insert(pReg);
	}


	// If we're still here, we're ready to:

	// Create sample
	auto t_sample = new sample_t{};
	t_sample->file = filename;
	t_sample->parent = pGig;
	t_sample->info.append(s_info);
	t_sample->MIDIUnityNote = note;
	t_sample->Channels = sfinfo->channels;
	t_sample->SamplesPerSecond = sfinfo->samplerate;
	switch (sfinfo->format & 0xff) {
	    case SF_FORMAT_PCM_S8:
	    case SF_FORMAT_PCM_16:
	    case SF_FORMAT_PCM_U8:
		t_sample->BitDepth = 16;
		break;
	    case SF_FORMAT_PCM_24:
		    t_sample->BitDepth = 24;
		break;
	    case SF_FORMAT_PCM_32:
		    case SF_FORMAT_FLOAT:
		    t_sample->BitDepth = 24;//32;
		    break;
		    case SF_FORMAT_DOUBLE:
			t_sample->BitDepth = 24;//64;
			break;
	    default:
			    *ret = "\n=== BitDepth not supported ===";
			    return false;
	}
	t_sample->frames = sfinfo->frames;
	if (wavinfo != "NoSfInst") {
	    t_sample->FineTune = sfinst->detune;
	    if (sfinst->loop_count && sfinst->loops[0].mode != SF_LOOP_NONE) {
		t_sample->Loops = 1;
		switch (sfinst->loops[0].mode) {
		    case SF_LOOP_FORWARD:
			    t_sample->LoopType = gig::loop_type_normal;
			break;
		    case SF_LOOP_BACKWARD:
			t_sample->LoopType = gig::loop_type_backward;
			break;
		    case SF_LOOP_ALTERNATING:
			t_sample->LoopType = gig::loop_type_bidirectional;
			break;
		}
		// check that loop.start is smaller then loop.end (akaiextract seems to swap values)
		if (sfinst->loops[0].start <= sfinst->loops[0].end) {
		    t_sample->LoopStart = sfinst->loops[0].start;
		    t_sample->LoopEnd = sfinst->loops[0].end;
		} else {
		    t_sample->LoopStart = sfinst->loops[0].end;
		    t_sample->LoopEnd = sfinst->loops[0].start;
		}
		t_sample->LoopPlayCount = sfinst->loops[0].count;
		t_sample->LoopSize = t_sample->LoopEnd - t_sample->LoopStart + 1;
	    }
	}
    	// check if file is stereo
    	uint8_t channels = sfinfo->channels;
    	if (channels > 2) {
    		*ret = "\n=== Too many channels in file: " + std::to_string(channels) + " ===";
    		return false;
    	}




    	bool collision = true;
    	if (sample->size() < 1) {
    		collision = false;
    	}

    	bool has_dimension = false;
    	bool has_samplechannel_dimension = false;
		for (auto spv : *sample) {
			if (spv.first->type == SP_DIMENSION) {
				has_dimension = true;

				sample_prop_dimension* spdim = (sample_prop_dimension*) spv.first;
				gig::dimension_t sdim = spdim->dim;
				if (sdim == gig::dimension_samplechannel and channels > 1) {
		    		*ret = "\n=== The sample-file has " + std::to_string(channels)
		    				+ "channels. Only Mono-files can be added to a single gig-channel ===";
		    		return false;
				}
				sp_midival dimval = *((sp_midival*) spv.second);

				// check for smaplechannel-dimension
				if (sdim == gig::dimension_samplechannel)
					has_samplechannel_dimension = true;
//				std::cout << "Sample-Dimension:" << GIG_DimName[sdim] << ":" << (int) dimval << std::endl;
				bool dim_found = false;
				if (pReg->Dimensions.size() < 1)
					collision = false;
				for (auto sc_dm : pReg->Dimensions) {
//					std::cout << "\tCheck Dimensions:" << GIG_DimName[sc_dm.first] << std::endl;
					if (sc_dm.first == sdim) { // dimension found
//						std::cout << "\t\tFound!" << std::endl;
//						for (auto sc_val : sc_dm.second->DimRegs)
//							std::cout << "\t\t\tDimValue:" << (int) sc_val.first << std::endl;
						dim_found = true;
						if (sc_dm.second->DimRegs.count(dimval) == 0) // no DimensionRegion collision
							collision = false;
//						else
//							std::cout << "Collision!" << std::endl;
						break;
					}
				} // END iterate pReg-Dimensions
				if (!dim_found) { // sample's dimension not in sc-Region -> create new dimension
					dimension_t* t_dimension = new dimension_t;
					t_dimension->dim = sdim;
					t_dimension->parent = pReg;
//					t_dimension->DimRegs[dimval].insert(t_sample); // add Sample to DimensionRegion
					pReg->Dimensions[sdim] = t_dimension;
					collision = false;
				}




			} // END if SP_DIMENSION
//			if (!collision)
//				break;
		} // END iterate spv

//		if (channels > 1)// and !has_samplechannel_dimension)
		if (channels > 1) {
			// check if there is already a samplechannel-dimension
			if (!has_samplechannel_dimension)
				collision = false;
			if (pReg->Dimensions.count(gig::dimension_samplechannel) == 0) {
				dimension_t* t_dimension = new dimension_t;
				t_dimension->dim = gig::dimension_samplechannel;
				t_dimension->parent = pReg;
				pReg->Dimensions[gig::dimension_samplechannel] = t_dimension;
			}

//			if (pReg->Dimensions.count(gig::dimension_samplechannel)

			for (uint8_t ch = 0; ch < channels; ch++)
				pReg->Dimensions[gig::dimension_samplechannel]->DimRegs[ch].insert(t_sample);
		}


//			if (pReg->Dimensions.count(gig::dimension_samplechannel) < channels)
//				collision = false;


		if (collision and has_dimension) { // all dimension-values match
//			std::cout << "COLLISION!!!" << std::endl;
			*ret = "\n=== Dimension-Collision ===";
			return false;
		}

		if (!has_dimension) {
			if (pReg->Dimensions.count(gig::dimension_none) > 0) {
				*ret = "\n=== Cannot add sample with no dimensions to existing region with no dimensions ===";
				return false;
			}
			dimension_t* t_dimension = new dimension_t;
			t_dimension->dim = gig::dimension_none;
			t_dimension->parent = pReg;
//					t_dimension->DimRegs[dimval].insert(t_sample); // add Sample to DimensionRegion
			pReg->Dimensions[gig::dimension_none] = t_dimension;
			pReg->Dimensions[gig::dimension_none]->DimRegs[0].insert(t_sample);
		}


		pGig->Samples.push_back(t_sample);






		// now add sample to dimregs
		for (auto spv : *sample)
			if (spv.first->type == SP_DIMENSION) {
				sample_prop_dimension* spdim = (sample_prop_dimension*) spv.first;
				gig::dimension_t sdim = spdim->dim;
				sp_midival dimval = *(sp_midival*) spv.second;
				for (auto& sc_dm : pReg->Dimensions)
					if (sc_dm.first == sdim)
						sc_dm.second->DimRegs[dimval].insert(t_sample);
			}


//	 print dimensionregions
//	std::cout << "\n--------- Print Dimensions -----------" << std::endl;
//	for (auto& sc_dm : pReg->Dimensions) {
//		std::cout << "Dimension: " << GIG_DimName[sc_dm.first] << std::endl;
//		for (auto dr : sc_dm.second->DimRegs) {
//			std::cout << "\tDimReg:" << (int)dr.first << std::endl;
//			for (auto val : dr.second)
//				std::cout << "\t\tfiles:" << cut_path(*(val->file)) << std::endl;
//		}
//	}


	return true;
}

bool SamplerCreator::gig_sampler::comapare_samples (sample_t* a, sample_t* b) {
	return a->MIDIUnityNote < b->MIDIUnityNote;
}

void SamplerCreator::gig_sampler::UpdateSamplerDisplayNode(SamplerDisplay_node *node) {
	std::pair<int, void*> nodepair = node->m_DataLink;
	int nodetype = nodepair.first;


	switch (nodetype) {
	case SCGIG_INSTRUMENT : {
		break;
	}
	case SCGIG_REGION : {
		region_t* reg = static_cast<region_t*>(nodepair.second);
		int nr_dims = reg->Dimensions.size();
		node->m_Name = std::to_string(reg->low) + ":" + std::to_string(reg->high);
		node->m_Numbers = "   " + std::to_string(nr_dims) + " dimension"
				+ (nr_dims > 1 ? "s" : "");
		return;
	}
	default : return;
	}
}

std::string SamplerCreator::gig_sampler::CreateSamplerDisplayModel () {
	SamplerDisplay_node* gig_SD_node{};
	SamplerDisplay_node* ins_SD_node{};
	SamplerDisplay_node* reg_SD_node{};
	SamplerDisplay_node* sam_SD_node{};
	SamplerDisplay_node* dim_SD_node{};
	SamplerDisplay_node* Samples_SD_node{};
	SamplerDisplay_node* dimreg_SD_node{};
	SamplerDisplay_node* Instruments_SD_node{};
	int t_gigs = SFcontainer->SamplerFiles.size();
	int t_instruments = 0;
	int t_samples = 0;
	for (auto& gig : SFcontainer->SamplerFiles) {
		gig_SD_node = new SamplerDisplay_node(NULL, gig->name,
				std::pair<int, void*>(SCGIG_GIG, gig), true);
		gig->SD_link = gig_SD_node;
		int nr_insts = gig->Instruments.size();
		int nr_samples = gig->Samples.size();
		t_samples += nr_samples;
		t_instruments += nr_insts;
		gig_SD_node->m_Numbers = std::to_string(nr_insts) + " instrument" + (nr_insts > 1 ? "s" : "")
				+ ", " + std::to_string(nr_samples) + " sample" + (nr_samples > 1 ? "s" : "");
			Samples_SD_node  = new SamplerDisplay_node(gig_SD_node,
					"Samples", std::pair<int, void*>(SCGIG_SAMPLES, &gig->Samples), true);
			Samples_SD_node->m_Numbers = " " + std::to_string(gig->Samples.size());
			gig->Samples.sort(comapare_samples);
			for (auto& sam : gig->Samples) {
				sam_SD_node = new SamplerDisplay_node(Samples_SD_node,
						sam->name(), std::pair<int, void*>(SCGIG_SAMPLE, sam), false);
				sam->SD_link = sam_SD_node;
				Samples_SD_node->Append(sam_SD_node);
			}
		gig_SD_node->Append(Samples_SD_node);

			Instruments_SD_node = new SamplerDisplay_node(gig_SD_node, "Instruments",
					std::pair<int, void*>(SCGIG_INSTRUMENTS, &gig->Instruments), true);
			Instruments_SD_node->m_Numbers = " " + std::to_string(gig->Instruments.size());
			for (auto& ins : gig->Instruments) {
				ins_SD_node = new SamplerDisplay_node(Instruments_SD_node, ins->name,
						std::pair<int, void*>(SCGIG_INSTRUMENT, ins), true);
				ins->SD_link = ins_SD_node;
				int nr_regs = ins->Regions.size();
				ins_SD_node->m_Numbers = "  " + std::to_string(nr_regs) + " region"
						+ (nr_regs > 1 ? "s" : "");
				for (auto& reg : ins->Regions) {
					reg_SD_node = new SamplerDisplay_node(ins_SD_node,
							std::to_string(reg->low) + ":" + std::to_string(reg->high),
							std::pair<int, void*>(SCGIG_REGION, reg), true);
					reg->SD_link = reg_SD_node;
					int nr_dims = reg->Dimensions.size();
					reg_SD_node->m_Numbers = "   " + std::to_string(nr_dims) + " dimension"
							+ (nr_dims > 1 ? "s" : "");
					for (auto& dim : reg->Dimensions) {
						dim_SD_node = new SamplerDisplay_node(reg_SD_node,
								GIG_DimName[dim.first],
								std::pair<int, void*>(SCGIG_DIMENSION, dim.second), true);
						dim.second->SD_link = dim_SD_node;
						dim_SD_node->m_Numbers = "    " + std::to_string(dim.second->DimRegs.size());
						for (auto& dimreg : dim.second->DimRegs) {
							dimreg_SD_node = new SamplerDisplay_node(dim_SD_node,
									std::to_string(dimreg.first),
									std::pair<int, void*>(SCGIG_DIMENSIONREGION, &dimreg), false);
							dim_SD_node->Append(dimreg_SD_node);
						}
						reg_SD_node->Append(dim_SD_node);
					}
					ins_SD_node->Append(reg_SD_node);
				}
				Instruments_SD_node->Append(ins_SD_node);
			}
		gig_SD_node->Append(Instruments_SD_node);
		SC_SamplerDisplayModel->m_SamplerDisplayNodes.push_back(gig_SD_node);
	}
	return std::to_string(t_gigs) + " gig" + (t_gigs > 1 ? "s, " : ", ") + std::to_string(t_instruments)
			+ " instrument" + (t_instruments > 1 ? "s, " : ", ")
			+ std::to_string(t_samples) + " sample" + (t_samples > 1 ? "s" : "");
}

void SamplerCreator::gig_sampler::SetNodeName (std::pair<int, void*> nodepair, wxString name) {
	int nodetype = nodepair.first;
	switch (nodetype) {
		case SCGIG_GIG : {
			samplerfile_t* sf = static_cast<samplerfile_t*>(nodepair.second);
			sf->name = name;
			break;
		}
		case SCGIG_INSTRUMENT : {
			instrument_t* si = static_cast<instrument_t*>(nodepair.second);
			si->name = name;
			break;
		}
	}
}

sp_text SamplerCreator::gig_sampler::SampleInfo (sample_t* ss) {
	sp_text ret = "";
	ret = "Sample-name:\t" + ss->name() + "\n" + *(ss->file);
	ret.append("\n\nMIDIUnityNote:\t\t" + std::to_string(ss->MIDIUnityNote));
//					+ "\nDimensions: " + std::to_string(ss->Dimensions.size()));
	ret.append("\nChannels:\t\t\t" + std::to_string(ss->Channels));
	ret.append("\nSamplesPerSecond:\t" + std::to_string(ss->SamplesPerSecond));
	ret.append("\nBitDepth:\t\t\t" + std::to_string(ss->BitDepth));
	ret.append("\nFrameSize:\t\t" + std::to_string(ss->FrameSize()));
	ret.append("\nFrames:\t\t\t" + std::to_string(ss->frames));
	ret.append("\nFineTune:\t\t\t" + std::to_string(ss->FineTune));
	ret.append("\nLoops:\t\t\t");
	ret.append((ss->Loops > 0 ? "yes" : "no"));
	if (ss->Loops > 0) {
	  ret.append("\n\tLoopType:\t\t" + std::to_string(ss->LoopType));
	  ret.append("\n\tLoopStart:\t" + std::to_string(ss->LoopStart));
	  ret.append("\n\tLoopEnd:\t\t" + std::to_string(ss->LoopEnd));
	  ret.append("\n\tLoopPlayCount:\t" + std::to_string(ss->LoopPlayCount));
	  ret.append("\n\tLoopSize:\t\t" + std::to_string(ss->LoopSize));
	}
	if (ss->info != "")
		ret.append("\n\n== additional info ==\n" + ss->info);
	return ret;
}
sp_text SamplerCreator::gig_sampler::GetInfo (std::pair<int, void*> nodepair) {
	sp_text ret = "";
	int nodetype = nodepair.first;

	switch (nodetype) {
		case SC_INVALID : {
			return *(static_cast<sp_text*>(nodepair.second));
		}

		case SCGIG_GIG : {
			samplerfile_t* sf = static_cast<samplerfile_t*>(nodepair.second);
			ret = "gig-name: " + sf->name;
			ret.append("\ngig-Instruments: " + std::to_string(sf->Instruments.size()));
			if (sf->info != "")
				ret.append("\n\n=== additional info ===\n" + sf->info);
			return ret;
		}
		case SCGIG_INSTRUMENTS : {
			std::set<instrument_t*>* insts = static_cast<std::set<instrument_t*>*>(nodepair.second);
			ret = std::to_string(insts->size()) + " instrument"
					+ (insts->size() > 1 ? "s" : "") + " of gig '"
					+ (*insts->begin())->parent->name + "'.";
			return ret;
		}
		case SCGIG_INSTRUMENT : {
			instrument_t* si = static_cast<instrument_t*>(nodepair.second);
			ret = "Instrument-name: " + si->name;
			ret.append("\nRegions: " + std::to_string(si->Regions.size()));
			if (si->info != "")
				ret.append("\n\n=== additional info ===\n" + si->info);
			return ret;
		}
		case SCGIG_REGION : {
			region_t* sr = static_cast<region_t*>(nodepair.second);
			ret = "Region: " + std::to_string(sr->low) + ":"
					+ std::to_string(sr->high);
			ret.append("\nDimensions: " + std::to_string(sr->Dimensions.size()));
			if (sr->info != "")
				ret.append("\n\n=== additional info ===\n" + sr->info);
			return ret;
		}
		case SCGIG_DIMENSION : {
			dimension_t* sd = static_cast<dimension_t*>(nodepair.second);
			ret = "Dimension: " + GIG_DimName[sd->dim] +
					"\nDimensionRegions: " + std::to_string(sd->DimRegs.size());
			return ret;
		}
		case SCGIG_DIMENSIONREGION : {
			dimresionregions_t* dimregs = static_cast<dimresionregions_t*>(nodepair.second);
			ret = std::to_string(dimregs->second.size()) + " Samples:";
			for (auto& sam : dimregs->second) {
				ret.append("\n====================\n");
				ret.append(SampleInfo(sam));
			}
//				ret.append("\n" + cut_path(*(sam->file)));
			return ret;
		}
		case SCGIG_SAMPLES : {
			std::set <sample_t*>* samples = static_cast<std::set <sample_t*>*>(nodepair.second);
			ret = std::to_string(samples->size()) + " sample"
					+ (samples->size() > 1 ? "s" : "") + " of gig '"
					+ (*samples->begin())->parent->name + "'.";
			return ret;
		}
		case SCGIG_SAMPLE : {
			sample_t* ss = static_cast<sample_t*>(nodepair.second);
			ret = SampleInfo(ss);
			return ret;
		}

		default : break;
	}
	return ret;
}

wxPanel* SamplerCreator::gig_sampler::NodeCtrl (wxPanel* parent, std::pair<int, void*> nodepair) {
	wxPanel* panel = new wxPanel(parent);
	wxBoxSizer *panel_vsizer = new wxBoxSizer( wxVERTICAL );
		wxStaticBox *Control_box = new wxStaticBox(panel, wxID_ANY, "&Controls");
		wxStaticBoxSizer *Control_box_sizer = new wxStaticBoxSizer(Control_box, wxVERTICAL );

		wxStaticBox *Info_box = new wxStaticBox(panel, wxID_ANY, "&Info", wxDefaultPosition,wxDefaultSize);
		wxStaticBoxSizer *Info_box_sizer = new wxStaticBoxSizer(Info_box, wxVERTICAL );
			wxTextCtrl* Info = new wxTextCtrl (Info_box, SAMPLERINFO,
					"No info",
					wxDefaultPosition, wxDefaultSize,
					wxTE_MULTILINE | wxTE_READONLY | wxHSCROLL);

	bool use_ctrl = false;
	int nodetype = nodepair.first;

	switch (nodetype) {
		case SC_INVALID : {
			std::string ret = *(static_cast<sp_text*>(nodepair.second));
			Info->SetValue(ret);
			break;
		}
		case SCGIG_GIG : {
			// Info
			samplerfile_t* sf = static_cast<samplerfile_t*>(nodepair.second);
			std::string ret = "=== gig ===\n\nName:\t\t" + sf->name;
			ret.append("\nInstruments:\t" + std::to_string(sf->Instruments.size()));
			if (sf->info != "")
				ret.append("\n\n== additional info ==\n" + sf->info);
			Info->SetValue(ret);
			break;
		}
		case SCGIG_INSTRUMENTS : {
			// Ctrl
			use_ctrl = true;
//				wxCheckBox* Expand_Regions = new wxCheckBox (
//						Control_box,
//						wxID_ANY,
//						"Expand Regions");
				wxStaticBox* Ranges_box = new wxStaticBox (Control_box, wxID_ANY, "&Ranges");
				wxStaticBoxSizer *Ranges_box_sizer = new wxStaticBoxSizer(Ranges_box, wxHORIZONTAL );
					wxButton* Expand_Ranges = new wxButton (
							Ranges_box,
							SD_NODE_EXPANDRANGES,
							"Expand");
				Ranges_box_sizer->Add(Expand_Ranges, 0, wxGROW | wxALL, 5);
					wxButton* Contract_Ranges = new wxButton (
							Ranges_box,
							SD_NODE_CONTRACTRANGES,
							"Contract");
				Ranges_box_sizer->Add(Contract_Ranges, 0, wxGROW | wxALL, 5);

			Control_box_sizer->Add(Ranges_box_sizer, 1, wxGROW | wxALL, 5 );
			// Info
			std::set<instrument_t*>* insts = static_cast<std::set<instrument_t*>*>(nodepair.second);
			std::string ret = std::to_string(insts->size()) + " instrument"
					+ (insts->size() > 1 ? "s" : "") + " of gig '"
					+ (*insts->begin())->parent->name + "'.";
			Info->SetValue(ret);
			break;
		}
		case SCGIG_INSTRUMENT : {
			instrument_t* si = static_cast<instrument_t*>(nodepair.second);
			// Ctrl
			use_ctrl = true;
//				wxCheckBox* Expand_Regions = new wxCheckBox (
//						Control_box,
//						wxID_ANY,
//						"Expand Regions");
				wxStaticBox* Ranges_box = new wxStaticBox (Control_box, wxID_ANY, "&Ranges");
				wxStaticBoxSizer *Ranges_box_sizer = new wxStaticBoxSizer(Ranges_box, wxHORIZONTAL );
					wxButton* Expand_Ranges = new wxButton (
							Ranges_box,
							SD_NODE_EXPANDRANGES,
							"Expand");
				Ranges_box_sizer->Add(Expand_Ranges, 0, wxGROW | wxALL, 5);
					wxButton* Contract_Ranges = new wxButton (
							Ranges_box,
							SD_NODE_CONTRACTRANGES,
							"Contract");
				Ranges_box_sizer->Add(Contract_Ranges, 0, wxGROW | wxALL, 5);

			Control_box_sizer->Add(Ranges_box_sizer, 1, wxGROW | wxALL, 5 );
			// Info
			std::string ret = "=== Instrument ===\n\nName:\t" + si->name;
			ret.append("\nRegions:\t" + std::to_string(si->Regions.size()));
			if (si->info != "")
				ret.append("\n\n== additional info ==\n" + si->info);
			Info->SetValue(ret);
			break;
		}
		case SCGIG_REGION : {
			region_t* sr = static_cast<region_t*>(nodepair.second);
			// Info
			std::string ret = "=== Region ===\n\nlow:\t\t\t" + std::to_string(sr->low) + "\nhigh:\t\t"
					+ std::to_string(sr->high);
			ret.append("\nDimensions:\t" + std::to_string(sr->Dimensions.size()));
			if (sr->info != "")
				ret.append("\n\n== additional info ==\n" + sr->info);
			Info->SetValue(ret);
			break;
		}
		case SCGIG_DIMENSION : {
			dimension_t* sd = static_cast<dimension_t*>(nodepair.second);
			std::string ret = "=== Dimension ===\n\nType:\t\t\t" + GIG_DimName[sd->dim] +
					"\nDimensionRegions:\t" + std::to_string(sd->DimRegs.size()) + "\n";
			for (auto& dimreg : sd->DimRegs) {
			    ret.append("\n" + std::to_string(dimreg.first) + ":  -----------");
			    for (auto& sam : dimreg.second) {
				ret.append("\n\t" + sam->name());
			    }
			}
			Info->SetValue(ret);
			break;
		}
		case SCGIG_DIMENSIONREGION : {
			dimresionregions_t* dimregs = static_cast<dimresionregions_t*>(nodepair.second);
			int nr_samples = dimregs->second.size();
			std::string ret = std::to_string(nr_samples) + " Sample" + (nr_samples != 1 ? "s:" : ":") + "\n";
			int sam_nr = 1;
			for (auto& sam : dimregs->second) {
				ret.append("\n" + std::to_string(sam_nr) + ".)        ------------------------------\n");
				ret.append(SampleInfo(sam) + "\n");
				sam_nr++;
			}
			Info->SetValue(ret);
			break;
		}
		case SCGIG_SAMPLES : {
			samples_t* samples = static_cast<samples_t*>(nodepair.second);
			std::string ret = std::to_string(samples->size()) + " sample"
					+ (samples->size() > 1 ? "s" : "") + " of gig '"
					+ (*samples->begin())->parent->name + "'.";
			Info->SetValue(ret);
			break;
		}
		case SCGIG_SAMPLE : {
			sample_t* ss = static_cast<sample_t*>(nodepair.second);
			std::string ret = "=== Sample ===\n\n" + SampleInfo(ss);
			Info->SetValue(ret);
			break;
		}
		default : {
			break;
		}
	}
	if (use_ctrl)
		panel_vsizer->Add(Control_box_sizer, 0, wxGROW | wxALL, 0 );
	else
		Control_box->Destroy();

	Info_box_sizer->Add(Info, 1, wxGROW | wxALL, 5 );
	Info_box_sizer->Layout();
	panel_vsizer->Add(Info_box_sizer, 1, wxGROW | wxALL, 0 );
	panel->SetSizerAndFit(panel_vsizer);
	return panel;
}

void SamplerCreator::gig_sampler::Ranges (bool expand, instrument_t* instr) {

	// lamda for getting basenote for range (assumption: all samples in region have the same basenote!)
	auto get_basenote = [&](region_t* reg) {
		dimension_t* first_dimension = (reg->Dimensions.begin()->second);
		sample_t* first_sample = *(first_dimension->DimRegs.begin()->second.begin());
		return first_sample->MIDIUnityNote;
	};

	if (expand) {
		if (instr->Regions.size() < 2)
			return;
//		std::cout << "Expand Ranges of: " << instr->name << std::endl;;
		region_t* first_region = *(instr->Regions.begin());
		sp_midival next_note_lower = first_region->low;
		for (auto reg_it = instr->Regions.begin(); reg_it != instr->Regions.end(); ++reg_it) {
			region_t* reg = *reg_it;
//			std::cout << "Process region: " << (int) reg->low << std::endl;
			sp_midival note = get_basenote (reg);

			reg->low = next_note_lower;
			auto next_reg_it = std::next(reg_it);
			if (next_reg_it == instr->Regions.end())
				break;
			sp_midival notes_between = get_basenote(*next_reg_it) - note;
			reg->high = note + notes_between / 2;
			next_note_lower = note + notes_between / 2 + notes_between % 2;
		}
		instr->info.append("\nRanges expanded");
	}
	else {
		if (instr->Regions.size() < 1)
			return;
//		std::cout << "Contracting Ranges of: " << instr->name << std::endl;;
		for (auto reg_it = instr->Regions.begin(); reg_it != instr->Regions.end(); ++reg_it) {
			region_t* reg = *reg_it;
//			std::cout << "Process region: " << (int) reg->low << std::endl;
			sp_midival note = get_basenote (reg);
			reg->low = reg->high = note;
		}
		instr->info.append("\nRanges contracted");
	}
}
void SamplerCreator::gig_sampler::ExpandAllRanges () {
	for (auto& gig : SFcontainer->SamplerFiles)
		for (auto& instr : gig->Instruments)
			Ranges (true, instr);
}

void SamplerCreator::gig_sampler::SDNodeCommand (int id, wxDataViewItem item, wxDataViewItemArray* changed_items) {
	SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
	wxDataViewItemArray changed_nodes {};
	std::cout << "SDNodeCommand:" << (int) id << std::endl;
	switch (id) {
		case SD_NODE_EXPANDRANGES :
		case SD_NODE_CONTRACTRANGES : {

			int nodetype = node->m_DataLink.first;

			// Get all instruments to apply exp/contr ranges to
			std::list<instrument_t*> instruments;
			switch (nodetype) {
				case SCGIG_INSTRUMENT : {
					instruments.push_back(static_cast<instrument_t*>(node->m_DataLink.second));
					break;
				}
				case SCGIG_INSTRUMENTS : {
					SamplerDisplay_nodePtrArray sd_insts = node->GetChildren();
					for (auto& ins : sd_insts)
						instruments.push_back(static_cast<instrument_t*>(ins->m_DataLink.second));
					break;
				}
				default : return;
			}

			// now apply
			for (auto& instr : instruments) {
				Ranges(id == SD_NODE_EXPANDRANGES, instr);
				for (unsigned int i = 0; i < instr->SD_link->GetChildCount(); i++) {
			        SamplerDisplay_node *child = instr->SD_link->GetNthChild(i);
			        UpdateSamplerDisplayNode(child);
					changed_items->Add( wxDataViewItem( (void*) child ) );
				}
			}
			break;
		}
		default : return;
	}
}
bool SamplerCreator::gig_sampler::CreateDimRgnsSamples (gig::File* gig,
		gig::Region* reg,
		gig::Group* group, uint8_t* iDimBits,
		std::map<gig::dimension_t, dimension_t*>* Dimensions,
		std::map<gig::dimension_t, dimension_t*>::iterator dim_it,
		std::map<gig::dimension_t, dimension_t*>::iterator stereo_dim_it,
//		std::set< sample_t*>
		samples_t DimRegs_Samples,
		sp_midival velocity,
		to_be_saved_samples_t* tbss) {
	uint8_t dim_number = 0;
	gig::Sample* stereo_sample = NULL;
	if (dim_it->first == gig::dimension_samplechannel
			and stereo_dim_it == Dimensions->end()
			) {
		stereo_dim_it = dim_it;
		if (std::next(dim_it) != Dimensions->end())
			++dim_it;
	}

	for (auto& dimreg : dim_it->second->DimRegs) { // pair<sp_midival, set<sample*> >
		samples_t temp_samples;
		for (auto dr_sam : DimRegs_Samples)
			for (auto sam : dimreg.second)
				if (sam == dr_sam)
					temp_samples.push_back(dr_sam);
		if (dim_it->second->DimRegs.size() > 1) {
			int dimindex = -1;
			for (unsigned int d = 0; d < reg->Dimensions; d++) {
				if (reg->pDimensionDefinitions[d].dimension == dim_it->first) {
					dimindex = d;
					break;
				}
			}
			if (dimindex == -1) {
				std::cout << "Could not resolve Dimension-Index" << std::endl;
				return false;
			}

			// set iDimBit
			*(iDimBits + dimindex) = dim_number;
		}

		// keep velocity-value
		if (dim_it->first == gig::dimension_velocity)
			velocity = dimreg.first;

		std::map<gig::dimension_t, dimension_t*>::iterator nxt = std::next(dim_it, 1);
		if (nxt == Dimensions->end())
			nxt = stereo_dim_it;
		if (nxt != Dimensions->end() and dim_it != stereo_dim_it) {
			bool got = CreateDimRgnsSamples (gig, reg, group,
					iDimBits,
					Dimensions,
					nxt,
					stereo_dim_it,
					temp_samples,
					velocity,
					tbss);
			if (!got) {
				std::cout << "CDRS = false" << std::endl;
				return false;
			}
			dim_number++;
			continue;
		}

		// end of this recursion
		gig::DimensionRegion* dimRgn = reg->GetDimensionRegionByBit(iDimBits);
		if (!dimRgn) {
			std::cout << "Could not getDimensionRegionByBit" << std::endl;
			return false;
		}

		// set velocity limits
		bool has_velocity = false;
		for (auto& vel_dim : *Dimensions) // check if SamDimRgn has velocity
			if (vel_dim.first == gig::dimension_velocity) {
				has_velocity = true;
				break;
			}
		if (has_velocity) { // apply velocity-values
			dimRgn->VelocityUpperLimit = velocity;// gig v2
			int dimindex = -1;
			for (unsigned int d = 0; d < reg->Dimensions; d++) {
				if (reg->pDimensionDefinitions[d].dimension ==
						gig::dimension_velocity) {
					dimindex = d;
					break;
				}
			}
			dimRgn->DimensionUpperLimits[dimindex] = velocity; // gig v3 and above
		}


		// now create Sample
		if (temp_samples.size() != 1) {
			std::cout << "No samples left!" << std::endl;
			return false;
		}
		sample_t* sf_sam = *temp_samples.begin();
		if (sf_sam->link == NULL) { // sample not created yet -> create it
			if (stereo_sample == NULL) {
				gig::Sample* sam = gig->AddSample();
				if (!sam) {
					std::cout << "Could not AddSample to gig" << std::endl;
					return false;
				}
				sam->pInfo->Name = sf_sam->name();
				const long frames = sf_sam->frames;
				if (dim_it->first == gig::dimension_samplechannel) {
					sam->Channels = dim_it->second->DimRegs.size();
					stereo_sample = sam;
				}
				else
					sam->Channels = sf_sam->Channels;
				sam->SamplesPerSecond = sf_sam->SamplesPerSecond;
				sam->BitDepth = sf_sam->BitDepth;
				sam->FrameSize = sam->Channels * sam->BitDepth / 8;
				sam->MIDIUnityNote = sf_sam->MIDIUnityNote;
				sam->FineTune = sf_sam->FineTune;
				if (sf_sam->Loops > 0) {
					sam->Loops = sf_sam->Loops;
					sam->LoopType = sf_sam->LoopType;
					sam->LoopStart = sf_sam->LoopStart;
					sam->LoopEnd = sf_sam->LoopEnd;
					sam->LoopPlayCount = sf_sam->LoopPlayCount;
					sam->LoopSize = sf_sam->LoopSize;
				}
				sam->Resize(frames);
				group->AddSample(sam);
				sf_sam->link = sam;
				(*tbss)[sam].insert(sf_sam);
			}
			else {
				(*tbss)[stereo_sample].insert(sf_sam);
				sf_sam->link = stereo_sample;
			}
		} // END createSample
		gig::Sample* sam = sf_sam->link;
		dimRgn->pSample = sam;
		if (sam) {
			dimRgn->UnityNote = sam->MIDIUnityNote;
			if (sam->Loops) {
				DLS::sample_loop_t loop;
				loop.Size = sizeof(loop);
				loop.LoopType   = sam->LoopType;
				loop.LoopStart  = sam->LoopStart;
				loop.LoopLength = sam->LoopEnd - sam->LoopStart;
				dimRgn->AddSampleLoop(&loop);
			}
		}
		dimRgn->FineTune = sam->FineTune;
		dim_number++;
	}
	return true;
}
bool SamplerCreator::gig_sampler::SaveSingleSamplerFile(samplerfile_t* sf, int gig_nr, int nr_gigs,
		float* progress, std::string* log) {
	to_be_saved_samples_t tbss;
	gig::File* gig = new gig::File;
	gig->pInfo->Name = sf->name;
	sf->link = gig;
	std::string gigfile = options.gigs_path + sep + gig->pInfo->Name + ".gig";
	log->append("\n" + gigfile + "\nCreating gig-structure");

	size_t instr_nr = 0;
	for (auto& sf_instr : sf->Instruments) {
		gig::Instrument* instr = gig->AddInstrument();
		instr->pInfo->Name = sf_instr->name;
		sf_instr->link = instr;

		gig::Group* group = gig->AddGroup();
		group->Name = sf_instr->name;
		for (auto& sf_reg : sf_instr->Regions) {
			gig::Region* reg = instr->AddRegion();

			reg->SetKeyRange(sf_reg->low, sf_reg->high);
			sf_reg->link = reg;

			// create gig-dimensions
			for (auto dim : sf_reg->Dimensions) {
				if (dim.second->DimRegs.size() > 1) {
					gig::dimension_def_t gdim;
					gdim.dimension = dim.first;
					gdim.zones = dim.second->DimRegs.size();
					int zoneBits = gdim.zones - 1;
					for (gdim.bits = 0; zoneBits > 1; gdim.bits += 2, zoneBits >>= 2);
					gdim.bits += zoneBits;
					reg->AddDimension(&gdim);
				}
			}
			// now start recursive DimensionRegion/Sample-creation
			uint8_t iDimBits[8] = {};
			bool got = CreateDimRgnsSamples (gig, reg,
					group,
					iDimBits,
					&sf_reg->Dimensions,
					sf_reg->Dimensions.begin(),
					sf_reg->Dimensions.end(), // will keep iterator to stereo-dimension
					sf->Samples, // all samples of gig
					-1, // will keep velocity-value
					&tbss); // list of to_be_saved-samples
			if (!got)
				return false;
		} // End Region
		instr_nr++;
		*progress = (float) (gig_nr + (instr_nr / (float)sf->Instruments.size()) * 0.2) / (float)nr_gigs;

		wxWakeUpIdle ();
	} // End Instruments

	// Cleanup groups
	int i = 0;
	gig::Group* tmp_group = gig->GetGroup (i);
	while (tmp_group != NULL) {
		if (tmp_group->GetSample (0) == NULL) {
			gig->DeleteGroup (tmp_group);
			tmp_group = gig->GetGroup (0);
			continue;
		}
		i++;
		tmp_group = gig->GetGroup (i);
	}

//	// cout structure
//	std::cout << "Gig-structure:" << std::endl;
//	int inst_nr = gig->CountInstruments();
//	std::cout << "Instruments: " << inst_nr << std::endl;
//	for (int ins = 0; ins < inst_nr; ins++) {
//		gig::Instrument* g_ins = gig->GetInstrument(ins);
//		std::cout << "\t" << g_ins->pInfo->Name << std::endl;
//		int reg_nr = g_ins->CountRegions();
//		std::cout << "Regions: " << (int) reg_nr << std::endl;
//		for (int reg = 0; reg < reg_nr; reg++) {
//			gig::Region* g_reg = g_ins->GetRegionAt(reg);
//			std::cout << "\t\t" << (int) g_reg->KeyRange.low << std::endl;
//			int dim_nr = g_reg->Dimensions;
//
//			std::cout << "\t\t\tDimensions: " << (int) dim_nr << std::endl;
//		}
//	}

//	// cout samples
//	std:: cout << "\n\ngig-Samples:" << std::endl;
//	for (auto& tbs_sam : tbss) {
//		std::cout << "\t" << tbs_sam.first->pInfo->Name << " : Channels:" << tbs_sam.first->Channels << std::endl;
//		for (auto& sc_sam : tbs_sam.second)
//			std::cout << "\t\t" << sc_sam->name() << std::endl;
//	}

	gig->Save(gigfile);

	// now save samples
	log->append("\nSaving " + std::to_string(tbss.size()) + " samples...");
	wxWakeUpIdle ();
	size_t sam_nr = 0;
	for (auto& tbs : tbss) {
		if (stop_create_thread)
			break;
		gig::Sample* gsample = tbs.first;
		uint8_t nr_sample_files = tbs.second.size();
		uint8_t ch_it = 0;
		SNDFILE* infile[nr_sample_files];
		SF_INFO sfinfo ;
		memset (&sfinfo, 0, sizeof (sfinfo)) ;
		for (auto& sf_sample : tbs.second) {
			infile [ch_it] = sf_open (sf_sample->file->c_str(), SFM_READ, &sfinfo);
			sf_command(infile [ch_it], SFC_SET_SCALE_FLOAT_INT_READ, 0, SF_TRUE);
			ch_it++;
		}
		uint8_t channels = tbs.first->Channels;
		sfinfo.channels = channels;
		int read_len = 0;
		int ch, k ;
		sf_count_t cnt = sfinfo.frames;
		const int BUFFER_LEN = 4096;
		switch (tbs.first->BitDepth) {
			case 16 : {
				short* inbuffer = new short[channels * BUFFER_LEN] ;
				// Make sure, outbuffer is big enough to hold diverting infiles (frames) when merging
				short* outbuffer = new short[2 * channels * BUFFER_LEN];
				int max_read_len = 0;
				while(cnt > 0) {
					max_read_len = 0;
					for (ch = 0 ; ch < nr_sample_files ; ch++) {
						read_len = (short) sf_readf_short (infile [ch], inbuffer, BUFFER_LEN) ;
						if (read_len < BUFFER_LEN)
							memset (inbuffer + read_len, 0, sizeof (inbuffer [0]) * (BUFFER_LEN - read_len)) ;

						max_read_len = max_read_len > read_len ? max_read_len : read_len;
						for (k = 0 ; k < channels * BUFFER_LEN ; k++)
							outbuffer[k * nr_sample_files + ch] = inbuffer[k] ;
					};
					gsample->Write(outbuffer, max_read_len);
					cnt -= max_read_len;
					}
				delete[] inbuffer;
				delete[] outbuffer;
				break;
			}
			case 24: {
				int* inbuffer = new int[channels * BUFFER_LEN] ;
				// Make sure, outbuffer is big enough to hold diverting infiles (frames) when merging
				int* tempbuffer = new int [2* channels * BUFFER_LEN];
				uint8_t outbuffer[2* channels * BUFFER_LEN * 3];
				int max_read_len = 0;
				while (cnt) {
					max_read_len = 0;
					for (ch = 0 ; ch < nr_sample_files ; ch ++) {
						read_len = (int) sf_readf_int (infile [ch], inbuffer, BUFFER_LEN) ;
						if (read_len < BUFFER_LEN)
							memset (inbuffer + read_len, 0, sizeof (inbuffer [0]) * (BUFFER_LEN - read_len)) ;
						max_read_len = max_read_len > read_len ? max_read_len : read_len;
						for (k = 0 ; k < channels * BUFFER_LEN ; k++)
							tempbuffer [k * nr_sample_files + ch] = inbuffer [k] ;
					};
					int j = 0;
					for (int i = 0 ; i < max_read_len * channels; i++) {
						outbuffer[j++] = tempbuffer[i] >> 8;
						outbuffer[j++] = tempbuffer[i] >> 16;
						outbuffer[j++] = tempbuffer[i] >> 24;
					}
					gsample->Write(outbuffer, max_read_len);
					cnt -= max_read_len;
					}
				break;
			}
//			case 32 :
//			case 64 : {
//				double* inbuffer = new double[channels * BUFFER_LEN] ;
//				// Make sure, outbuffer is big enough to hold diverting infiles (frames) when merging
//				double* outbuffer = new double[2 * channels * BUFFER_LEN];
//				int max_read_len = 0;
//				while(cnt > 0) {
//					max_read_len = 0;
//					for (ch = 0 ; ch < nr_sample_files ; ch++) {
//						read_len = (int) sf_readf_double (infile [ch], inbuffer, BUFFER_LEN) ;
//						if (read_len < BUFFER_LEN)
//							memset (inbuffer + read_len, 0, sizeof (inbuffer [0]) * (BUFFER_LEN - read_len)) ;
//
//						max_read_len = max_read_len > read_len ? max_read_len : read_len;
//						for (k = 0 ; k < channels * BUFFER_LEN ; k++)
//							outbuffer[k * nr_sample_files + ch] = inbuffer[k] ;
//					};
//					gsample->Write(outbuffer, max_read_len);
//					cnt -= max_read_len;
//					}
//				delete[] inbuffer;
//				delete[] outbuffer;
//				break;
//			}
		}

		for (ch = 0 ; ch < nr_sample_files ; ch ++) {
			sf_close (infile [ch]) ;
		}

		sam_nr++;
		*progress = (float) (gig_nr + 0.2 + (sam_nr / (float)tbss.size()) * 0.8) / (float)nr_gigs;
		wxWakeUpIdle ();
	} // End tbss-loop
	log->append("ok!");
	wxWakeUpIdle ();
	return true;
}
void SamplerCreator::gig_sampler::SaveSamplerFiles(float* progress, std::string* log) {
	*log = "=== Start creating gig-Files ===";
	int nr_gigs = SFcontainer->SamplerFiles.size();
	int a = 0;
	for (auto& gig_it : SFcontainer->SamplerFiles) {
		if (stop_create_thread) {
			log->append("\n====== Stopping =======");
			wxWakeUpIdle ();
			break;
		}
		log->append("\n\n(" + std::to_string(a + 1) + "/" + std::to_string(nr_gigs) + ") " + gig_it->name);
		wxWakeUpIdle ();
		SaveSingleSamplerFile(gig_it, a, nr_gigs, progress, log);
		wxWakeUpIdle ();
		a++;
	}
	*progress = 1;
	log->append("\n\n=== Done ===");
	wxWakeUpIdle ();
}

