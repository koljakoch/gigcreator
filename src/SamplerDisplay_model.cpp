/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/


#include "wx/wxprec.h"


#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

#include "wx/dataview.h"
//#include "SamplerDisplay.h"

// ----------------------------------------------------------------------------
// SamplerDisplay
// ----------------------------------------------------------------------------

SamplerDisplay::SamplerDisplay()
{
}

bool SamplerDisplay::isGig( const wxDataViewItem &item ) const {
    SamplerDisplay_node *node = (SamplerDisplay_node*) SamplerDisplay::GetParent(item).GetID();
    if (!node)      // happens if item.IsOk()==false
        return true;

    return false;
}

wxString SamplerDisplay::GetID( const wxDataViewItem &item ) const {
//    SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
//    if (!node)      // happens if item.IsOk()==false
        return wxEmptyString;
//
//    return node->m_id;
}

std::string SamplerDisplay::GetName( const wxDataViewItem &item ) const {
    SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
    if (!node)      // happens if item.IsOk()==false
        return "";

    return node->m_Name.utf8_string();
}

void SamplerDisplay::Delete( const wxDataViewItem &item ) {
    SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
    if (!node)      // happens if item.IsOk()==false
        return;

    wxDataViewItem parent( node->GetParent() );
    if (!parent.IsOk())
    {
        wxLogError( "Cannot remove the root item!" );
        return;
    }
    node->GetParent()->GetChildren().Remove( node );

    // free the node
    delete node;

    // notify control
    ItemDeleted( parent, item );
}

void SamplerDisplay::DeleteAll() {
	m_SamplerDisplayNodes.clear();
	Cleared();
}

int SamplerDisplay::Compare( const wxDataViewItem &item1, const wxDataViewItem &item2,
			unsigned int column, bool ascending ) const {
    wxASSERT(item1.IsOk() && item2.IsOk());
        // should never happen

    if (IsContainer(item1) && IsContainer(item2))
    {
        wxVariant value1, value2;
        GetValue( value1, item1, 0 );
        GetValue( value2, item2, 0 );

        wxString str1 = value1.GetString();
        wxString str2 = value2.GetString();
        int res = str1.Cmp( str2 );
        if (res) return res;

        // items must be different
        wxUIntPtr litem1 = (wxUIntPtr) item1.GetID();
        wxUIntPtr litem2 = (wxUIntPtr) item2.GetID();

        return litem1-litem2;
    }

    return wxDataViewModel::Compare( item1, item2, column, ascending );
}

bool SamplerDisplay::HasContainerColumns (const wxDataViewItem & item ) const {
	return true;
}


SamplerDisplay_node* SamplerDisplay::GetNode (void* SamplerFile_node) {
	for (auto& SamplerDisplayNode : m_SamplerDisplayNodes) {
		if (SamplerDisplayNode->m_DataLink.second == SamplerFile_node)
			return SamplerDisplayNode;
		wxDataViewItemArray kids_array{};
//		unsigned int kids =
		GetChildren(wxDataViewItem(SamplerDisplayNode) ,
						 kids_array );
//		std::cout << "GetNode0:" << std::to_string(kids) << std::endl;
		for (auto kid = kids_array.begin(); kid != kids_array.end(); ++kid) {
	//	for (unsigned int i = 0; i < kids; i++) {
	//		wxDataViewItem wdi = (wxDataViewItem) *kid;
			SamplerDisplay_node* kid_node = (SamplerDisplay_node*)kid->GetID();//wdi;
//			std::cout << "GetNode1:" << std::flush << kid_node->m_Name << std::endl;
			if (kid_node->m_DataLink.second == SamplerFile_node) {
//				std::cout << "GetNode:found:" << std::endl;
				return kid_node;
			}
		}
	}
	return NULL;
}

void SamplerDisplay::GetValue( wxVariant &variant,
		 const wxDataViewItem &item, unsigned int col ) const {
    wxASSERT(item.IsOk());

    SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
    switch (col)
    {
    case 0:
        variant = node->m_Name;
        break;
    case 1:
		variant = node->m_Numbers;
		break;
    default:
    	std::cout << "Wrong Column: " << col << std::endl;
    	wxLogError( "SamplerDisplay::GetValue: wrong column %d", col );
    }
}

bool SamplerDisplay::SetValue( const wxVariant &variant,
		 const wxDataViewItem &item, unsigned int col ) {
    wxASSERT(item.IsOk());

    SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
    switch (col)
    {
        case 0:
            node->m_Name = variant.GetString();
            return true;
//        case 1:
//            node->m_instr = variant.GetLong();
//            return true;
//        case 2:
//            node->m_samples = variant.GetLong();
//            return true;
//        case 3:
//            node->m_vel_range = variant.GetString();
//            return true;
//        case 4: {
//            std::cout << "Chkrng set:" << node->m_expand_range << std::endl;
//            setExpandRanges( node, item, variant);
//            return true;
//        }
        default:
            wxLogError( "SamplerDisplay::SetValue: wrong column %d", col );
    }
    return false;
}

void SamplerDisplay::setExpandRanges( SamplerDisplay_node *node, const wxDataViewItem &item, const wxVariant &variant ) {
//    node->m_note_range = variant.GetString();
//    wxDataViewCheckIconText checkIconText;
//    checkIconText << variant;
//    node->m_note_range = checkIconText.GetText();
//    node->m_expand_range =
//        checkIconText.GetCheckedState() == wxCHK_CHECKED;
//    if (node->m_gc_link) {
//    	if (isGig(item)) {
//    		wxDataViewItemArray instr_array;
//    	    unsigned int count = node->GetChildren().GetCount();
//    	    for (unsigned int pos = 0; pos < count; pos++)
//    	    {
//    	        SamplerDisplay_node *child = node->GetChildren().Item( pos );
//    	        child->m_expand_range =
//    	                checkIconText.GetCheckedState() == wxCHK_CHECKED;
//    	        ItemChanged( wxDataViewItem( (void*) child ) );
////        		gigcontainer::gig::Instrument* temp_Instr = (gigcontainer::gig::Instrument*) child->m_gc_link;
////        		temp_Instr->expand_range = child->m_expand_range;
//    	    }
//    	} else {
//    		// set parent gig expand range to false
//    		if (!node->m_expand_range) {
//    			SamplerDisplay_node* parent = node->GetParent();
//    	        parent->m_expand_range = false;
//    			ItemChanged( wxDataViewItem( (void*) parent ) );
//    		}
////    		gigcontainer::gig::Instrument* temp_Instr = (gigcontainer::gig::Instrument*) node->m_gc_link;
////			temp_Instr->expand_range = node->m_expand_range;
//    	}
//    }
//    std::cout << "DataModel:gc->GetRange" << std::endl;
////    gc->GetNoteRange();
    
}

bool SamplerDisplay::IsEnabled( const wxDataViewItem &item,
		  unsigned int col ) const {
    wxASSERT(item.IsOk());
    return true;
}

wxDataViewItem SamplerDisplay::GetParent( const wxDataViewItem &item ) const {
    // the invisible root node has no parent
    if (!item.IsOk())
        return wxDataViewItem(0);
    SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
    return wxDataViewItem( (void*) node->GetParent() );
}

bool SamplerDisplay::IsContainer( const wxDataViewItem &item ) const {
    if (!item.IsOk())
        return true;
    SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
    return node->IsContainer();
}

unsigned int SamplerDisplay::GetChildren( const wxDataViewItem &parent,
			wxDataViewItemArray &array ) const {
    SamplerDisplay_node *node = (SamplerDisplay_node*) parent.GetID();
    if (!node)
    {
        for (auto m_SamplerNodes_it = m_SamplerDisplayNodes.begin();
        		m_SamplerNodes_it != m_SamplerDisplayNodes.end(); ++m_SamplerNodes_it)
        	array.Add( wxDataViewItem( (void*) *m_SamplerNodes_it ) );
        return m_SamplerDisplayNodes.size();
    }

    if (node->GetChildCount() == 0)
    {
        return 0;
    }

    unsigned int count = node->GetChildren().GetCount();
    for (unsigned int pos = 0; pos < count; pos++)
    {
        SamplerDisplay_node *child = node->GetChildren().Item( pos );
        array.Add( wxDataViewItem( (void*) child ) );
    }
    return count;
}

//unsigned int SamplerDisplay::GetAllChildren( const wxDataViewItem &parent,
//			wxDataViewItemArray &array ) const {
//    SamplerDisplay_node *node = (SamplerDisplay_node*) parent.GetID();
//    if (!node)
//    {
//        for (auto m_SamplerNodes_it = m_SamplerDisplayNodes.begin();
//        		m_SamplerNodes_it != m_SamplerDisplayNodes.end(); ++m_SamplerNodes_it) {
//        	array.Add( wxDataViewItem( (void*) *m_SamplerNodes_it ) );
//        }
//        return m_SamplerDisplayNodes.size();
//    }
//
//    if (node->GetChildCount() == 0)
//    {
//        return 0;
//    }
//
//    unsigned int count = node->GetChildren().GetCount();
//    for (unsigned int pos = 0; pos < count; pos++)
//    {
//        SamplerDisplay_node *child = node->GetChildren().Item( pos );
//        array.Add( wxDataViewItem( (void*) child ) );
//    }
//    return count;
//}



// SpecOptions
//IMPLEMENT_VARIANT_OBJECT(wxBitmap)

SpecOptions_model::SpecOptions_model () {}

//unsigned int SpecOptions_model::GetChildren( const wxDataViewItem &parent,
//			wxDataViewItemArray &array ) const {
////	SpecOptions_node *node = (SpecOptions_node*) parent.GetID();
//
//        for (auto m_SamplerNodes_it = SpecOptions_nodes.begin();
//        		m_SamplerNodes_it != SpecOptions_nodes.end(); ++m_SamplerNodes_it)
//        	array.Add( wxDataViewItem( (void*) *m_SamplerNodes_it ) );
//        return SpecOptions_nodes.size();
//}

void SpecOptions_model::Add(bool a_active,
wxString a_specstring,
wxString a_sp_type,
wxString a_dim_type,
wxString a_values,
wxString a_preview) {
	active.push_back(a_active);
	specstring.Add(a_specstring, 1);
	sp_type.Add(a_sp_type, 1);
	dim_type.Add(a_dim_type, 1);
	values.Add(a_values, 1);
	preview.Add(a_preview, 1);
	tmatrix.push_back({});
}

void SpecOptions_model::DeleteItem( const wxDataViewItem &item )
{
	unsigned int row = GetRow( item );
	if (row >= active.size())
		return;
	active.erase( active.begin()+row );

	specstring.RemoveAt(row);
	sp_type.RemoveAt(row);
	dim_type.RemoveAt(row);
	values.RemoveAt(row);
	preview.RemoveAt(row);
}


void SpecOptions_model::GetValueByRow( wxVariant &variant,
				unsigned int row, unsigned int col ) const {
	switch (col)
	{
	case 0:
		variant = active[row];
		break;
	case 1:
		variant = "%";
		break;
	case 2:
		variant = specstring[row];
		break;
	case 3:
		variant = sp_type[row];
		break;
	case 4:
		variant = IsEnabledByRow(row,4);
		break;
	case 5:
		if (IsEnabledByRow(row, 5))
			variant = dim_type[row];
		else
			variant = "";
		break;
	case 6:
		if (IsEnabledByRow(row, 6))
			variant = values[row];
		else
			variant = "";
		break;
	case 7:
		variant = (values[row] == SO_Value[1]);
		break;
	case 8:
		variant = preview[row];
		break;
	default:
		std::cout << "Wrong Column: " << col << std::endl;
		wxLogError( "SamplerDisplay::GetValue: wrong column %d", col );
	}
}

 bool SpecOptions_model::SetValueByRow( const wxVariant &variant,
                            unsigned int row, unsigned int col ) {
	 switch (col)
	 {
		 case 0:
			 active[row] = variant.GetBool();
			 return true;
		 case 1:
			return true;
		 case 2:
			 specstring[row] = variant.GetString();
			 return true;
		 case 3:
			 sp_type[row] = variant.GetString();
			 return true;
		 case 4:
			 return true;
		 case 5:
			 dim_type[row] = variant.GetString();
			 return true;
		 case 6:
			values[row] = variant.GetString();
			return true;
		 case 7:
	//        	node->t_matrix = variant.GetBool();
			return true;
		 case 8:
			preview[row] = variant.GetString();
			return true;
		 default:
			 wxLogError( "SamplerDisplay::SetValue: wrong column %d", col );
	 }
	 return false;
}

bool SpecOptions_model::IsEnabledByRow (unsigned int row, unsigned int col) const {
	if (col == 0)
		return true;
	if (active[row] == false)
		return false;

	switch (col) {
	case 4:
		if (sp_type[row] == SPTypes[SP_NOTE] or
				sp_type[row] == SPTypes[SP_NOTENAME])
			return true;
		else
			return false;
	case 5:
		if (sp_type[row] == SPTypes[SP_DIMENSION])
			return true;
		else return false;
	case 6:
		if (sp_type[row] == SPTypes[SP_IGNORE])
			return false;
		else return true;
	case 7:
		if (values[row] == SO_Value[1])
			return true;
		else
			return false;
	case 8:
		return false;
	default : return true;

	}
	return true;
}

bool O_ButtonRenderer::ActivateCell(const wxRect& WXUNUSED(cell),
                          wxDataViewModel *model,
                          const wxDataViewItem& item,
                          unsigned int WXUNUSED(col),
                          const wxMouseEvent *mouseEvent)
{
	SpecOptions_model* spon = (SpecOptions_model*) model;
	unsigned int row = spon->GetRow( item );
	std::cout << spon->dim_type[row] << std::endl;
	//        wxString position;
//        if ( mouseEvent )
//            position = wxString::Format("via mouse at %d, %d", mouseEvent->m_x, mouseEvent->m_y);
//        else
//            position = "from keyboard";
//        wxLogMessage("MyCustomRenderer ActivateCell() %s", position);
	if (spon->sp_type[row] == SPTypes[SP_NOTE]) {
		SpecOptions_Note* SOT_Frame = new SpecOptions_Note(spon , row);
		SOT_Frame->ShowModal();
		SOT_Frame->Destroy();
	}
	else if (spon->sp_type[row] == SPTypes[SP_NOTENAME]) {
			SpecOptions_Notenames* SOT_Frame = new SpecOptions_Notenames(spon , row);
			SOT_Frame->ShowModal();
			SOT_Frame->Destroy();
		}
    return false;
}

bool T_ButtonRenderer::ActivateCell(const wxRect& WXUNUSED(cell),
                          wxDataViewModel *model,
                          const wxDataViewItem& item,
                          unsigned int WXUNUSED(col),
                          const wxMouseEvent *mouseEvent)
{
	SpecOptions_model* spon = (SpecOptions_model*) model;
	unsigned int row = spon->GetRow( item );
	std::cout << spon->dim_type[row] << std::endl;
	//        wxString position;
//        if ( mouseEvent )
//            position = wxString::Format("via mouse at %d, %d", mouseEvent->m_x, mouseEvent->m_y);
//        else
//            position = "from keyboard";
//        wxLogMessage("MyCustomRenderer ActivateCell() %s", position);

	SpecOptions_Tmatrix* SOT_Frame = new SpecOptions_Tmatrix(spon , row);
	SOT_Frame->ShowModal();

//	wxCommandEvent event(RESET_EVENT, RESULT_RESET);
//	event.SetEventObject(this);
//	ProcessWindowEvent(event);

	SOT_Frame->Destroy();
    return true;
}

Tmatrix_model::Tmatrix_model (SPTYPE i_sp_type) {
	sp_type = i_sp_type;
}

void Tmatrix_model::GetValueByRow( wxVariant &variant,
				unsigned int row, unsigned int col ) const {
	switch (col)
	{
	case 0:
		variant = active[row];
		break;
	case 1:
		variant = find[row];
		break;
	case 2:
		switch (sp_type) {
			case SP_SAMPLERFILE :
			case SP_INSTRUMENT :
			{
				variant = replace[row];
				break;
			}
			default : {
			long num;
			replace[row].ToLong(&num);
			variant = num;
			break;
			}
		}
			break;
	default:
		std::cout << "Wrong Column: " << col << std::endl;
		wxLogError( "SamplerDisplay::GetValue: wrong column %d", col );
	}
}

bool Tmatrix_model::SetValueByRow( const wxVariant &variant,
                            unsigned int row, unsigned int col ) {
	 switch (col)
	 {
		 case 0:
			 active[row] = variant.GetBool();
			 return true;
		 case 1:
			find[row] = variant.GetString();
			return true;
		 case 2:

				switch (sp_type) {
					case SP_SAMPLERFILE :
					case SP_INSTRUMENT :
					{
						replace[row] = variant.GetString();
						return true;
					}
					default : {
						 int num = variant.GetInteger();
						 replace[row] = wxString::Format(wxT("%i"), num); //variant.GetString();
						 return true;
					}
				}
				break;
		 default:
			 wxLogError( "SamplerDisplay::SetValue: wrong column %d", col );
	 }
	 return false;
}

bool Tmatrix_model::IsEnabledByRow (unsigned int row, unsigned int col) const {
	if (col == 0)
		return true;
	if (active[row] == false)
		return false;
	return true;
}


void Tmatrix_model::DeleteItem( const wxDataViewItem &item )
{
	unsigned int row = GetRow( item );
	if (row >= active.size())
		return;
	active.erase( active.begin()+row );

	find.RemoveAt(row);
	replace.RemoveAt(row);
}





