/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/


#include "wx/hashmap.h"
#include "wx/vector.h"
#include <wx/renderer.h>

# include <gig.h>
//#include "SamplerCreator.h"


WX_DECLARE_HASH_MAP(unsigned, wxString, wxIntegerHash, wxIntegerEqual,
	IntToStringMap);

class SamplerDisplay_node;
WX_DEFINE_ARRAY_PTR( SamplerDisplay_node*, SamplerDisplay_nodePtrArray );

//class SpecOptions_node;
//WX_DEFINE_ARRAY_PTR( SpecOptions_node*, SpecOptions_nodePtrArray );

class SamplerDisplay_node {
public:
    SamplerDisplay_node( SamplerDisplay_node* parent,
    		const wxString &Name,
			std::pair<int, void*>  DataLink,
			const bool IsContainer)
    {
        m_parent = parent;
        m_Name = Name;
        m_DataLink = DataLink;
        m_IsContainer = IsContainer;
    }

    ~SamplerDisplay_node()
    {
        // free all our children nodes
        size_t count = m_children.GetCount();
        for (size_t i = 0; i < count; i++)
        {
            SamplerDisplay_node *child = m_children[i];
            delete child;
        }
    }

    bool IsContainer() const
        { return m_IsContainer; }

    SamplerDisplay_node* GetParent()
        { return m_parent; }
    SamplerDisplay_nodePtrArray& GetChildren()
        { return m_children; }
    SamplerDisplay_node* GetNthChild( unsigned int n )
        { return m_children.Item( n ); }
    void Insert( SamplerDisplay_node* child, unsigned int n)
        { m_children.Insert( child, n); }
    void Append( SamplerDisplay_node* child )
        { m_children.Add( child ); }
    unsigned int GetChildCount() const
        { return m_children.GetCount(); }
    void Delete_children () {
//    	for (auto& child : m_children)
//    		delete child;
    	m_children.Clear();
    }

public:     // public to avoid getters/setters
    wxString 	m_Name{};
    std::pair<int, void*> 		m_DataLink{};
    bool 	m_IsContainer{};
    wxString	m_Numbers{};
private:
    SamplerDisplay_node          *m_parent;
    SamplerDisplay_nodePtrArray   m_children;
};

class SamplerDisplay: public wxDataViewModel
{
public:
    SamplerDisplay();
    ~SamplerDisplay()
    {}
    bool isGig( const wxDataViewItem &item ) const;
    wxString GetID( const wxDataViewItem &item ) const;

    // helper methods to change the model
    void setExpandRanges( SamplerDisplay_node *node, const wxDataViewItem &item, const wxVariant &variant );
    std::string GetName( const wxDataViewItem &item ) const;
    void Delete( const wxDataViewItem &item );
    void DeleteAll();

    int Compare( const wxDataViewItem &item1, const wxDataViewItem &item2,
                 unsigned int column, bool ascending ) const wxOVERRIDE;

    // implementation of base class virtuals to define model

    virtual unsigned int GetColumnCount() const wxOVERRIDE
    {
        return 2;
    }

    virtual wxString GetColumnType( unsigned int col ) const wxOVERRIDE
    {
        if ((col == 1) or (col == 2))
            return "long";
        if (col == 4)
        	return  	wxDataViewCheckIconTextRenderer::GetDefaultType();
        return "string";
    }

    SamplerDisplay_node* GetNode (void* SamplerFile_node);

    virtual bool HasContainerColumns (const wxDataViewItem & item ) const wxOVERRIDE; 
    virtual void GetValue( wxVariant &variant,
                           const wxDataViewItem &item, unsigned int col ) const wxOVERRIDE;
    virtual bool SetValue( const wxVariant &variant,
                           const wxDataViewItem &item, unsigned int col ) wxOVERRIDE;

    virtual bool IsEnabled( const wxDataViewItem &item,
                            unsigned int col ) const wxOVERRIDE;

    virtual wxDataViewItem GetParent( const wxDataViewItem &item ) const wxOVERRIDE;
    virtual bool IsContainer( const wxDataViewItem &item ) const wxOVERRIDE;
    virtual unsigned int GetChildren( const wxDataViewItem &parent,
                                      wxDataViewItemArray &array ) const wxOVERRIDE;
    
    std::list<SamplerDisplay_node*> m_SamplerDisplayNodes;
};

// SpecOptions

std::map<uint8_t, std::string> SO_Value = {
		{0, "use found value"},
		{1, "use T-matrix"}
};

std::map<SPTYPE, std::string> SPTypes = {
		{SP_SAMPLERFILE, "Name"},
		{SP_INSTRUMENT, "Instrumentname"},
		{SP_NOTE, "Notenumber"},
		{SP_NOTENAME, "Notename"},
		{SP_DIMENSION, "Dimension"},
		{SP_IGNORE, "Ignore"}
};

SPTYPE SPtypeByName (std::string type) {
	for (auto spt : SPTypes)
		if (spt.second == type)
			return spt.first;
	return SP_NONE;
}

std::map<gig::dimension_t, std::string> GIG_DimName = {
    	{gig::dimension_none, "Not in use"},
    	{gig::dimension_samplechannel, "Samplechannel"},
    	{gig::dimension_layer, "Layer"},
    	{gig::dimension_velocity, "Velocity"},
    	{gig::dimension_channelaftertouch, "Channel aftertouch"},
    	{gig::dimension_releasetrigger, "Release trigger"},
    	{gig::dimension_keyboard, "Keyswitching"},
    	{gig::dimension_roundrobin, "Roundrobin"},
    	{gig::dimension_random, "Random"},
    	{gig::dimension_smartmidi, "Smartmidi"},
    	{gig::dimension_roundrobinkeyboard, "Roundrobin keyboard"},
    	{gig::dimension_modwheel, "Moulation wheel, CC 1"},
    	{gig::dimension_breath, "Breath, CC 2"},
    	{gig::dimension_foot, "Foot Pedal, CC 4"},
    	{gig::dimension_portamentotime, "Portamentotime, CC 5"},
    	{gig::dimension_effect1, "Effect Controller 1, CC 12"},
    	{gig::dimension_effect2, "Effect Controller 2, CC 13"},
    	{gig::dimension_genpurpose1, "General Purpose 1, CC 16"},
    	{gig::dimension_genpurpose2, "General Purpose 2, CC 17"},
    	{gig::dimension_genpurpose3, "General Purpose 3, CC 18"},
    	{gig::dimension_genpurpose4, "General Purpose 4, CC 19"},
    	{gig::dimension_sustainpedal, "Sustain pedal, CC 64"},
    	{gig::dimension_portamento, "Portamento, CC 65"},
    	{gig::dimension_sostenutopedal, "Sostenuto pedal, CC 66"},
    	{gig::dimension_softpedal, "Soft pedal, CC 67"},
    	{gig::dimension_genpurpose5, "General Purpose 5, CC 80"},
    	{gig::dimension_genpurpose6, "General Purpose 6, CC 81"},
    	{gig::dimension_genpurpose7, "General Purpose 7, CC 82"},
    	{gig::dimension_genpurpose8, "General Purpose 8, CC 83"},
    	{gig::dimension_effect1depth, "Effect 1 depth, CC 91"},
    	{gig::dimension_effect2depth, "Effect 2 depth, CC 92"},
    	{gig::dimension_effect3depth, "Effect 3 depth, CC 93"},
    	{gig::dimension_effect4depth, "Effect 4 depth, CC 94"},
    	{gig::dimension_effect5depth, "Effect 5 depth, CC 95"}
};

gig::dimension_t gDimByName (std::string type) {
	for (auto spt : GIG_DimName)
		if (spt.second == type)
			return spt.first;
	return gig::dimension_none;
}

//class SpecOptions_node {
//public:
//	SpecOptions_node ( SpecOptions_node* son_parent,
//			bool son_active,
//			std::string son_specstring,
//			std::string son_sp_type,
//			std::string son_dim_type,
//			std::string son_values,
////			bool son_t_matrix,
//			std::string son_preview
//			) {
//		parent = son_parent;
//		active = son_active;
//		specstring = son_specstring;
//		sp_type = son_sp_type;
//		dim_type = son_dim_type;
//		values = son_values;
////		t_matrix = son_t_matrix;
//		preview = son_preview;
//	}
//
//	bool active;
//	std::string specstring;
//	std::string sp_type;
//	std::string dim_type;
//	std::string values;
////	bool t_matrix;
//	std::string preview;
//
//	bool IsContainer() const { return false; }
////	bool IsDimension() const {
////		return sp_type = "Dimension";
////	}
//	SpecOptions_node* GetParent() { return parent; }
//
//private:
//	SpecOptions_node* parent;
//};


class SpecOptions_model: public wxDataViewVirtualListModel {

public:
	SpecOptions_model();
	~SpecOptions_model(){}
//	std::list<SpecOptions_node*> SpecOptions_nodes;
	wxVector<bool> active;
	wxArrayString specstring;
	wxArrayString sp_type;
	wxArrayString dim_type;
	wxArrayString values;
	wxArrayString preview;
	struct tmatrix_struct {
		bool active = false;
		wxString find = "";
		wxString replace = "";
	};
	wxVector<std::list<tmatrix_struct> > tmatrix;

	void Add(bool a_active,
	wxString a_specstring,
	wxString sp_type,
	wxString dim_type,
	wxString values,
	wxString preview);

    virtual unsigned int GetColumnCount() const wxOVERRIDE { return 9; }
    virtual wxString GetColumnType( unsigned int col ) const wxOVERRIDE
    {
        switch (col) {
        	case 0 : return "bool";	// active
			case 1 : return "string";	// %
			case 2 : return "string";	// SpecString
			case 3 : return "string";	// SP-Type
			case 4 : return "bool"; // T-Matrix
			case 5 : return "string";	// Dimension
			case 6 : return "string"; // Values
			case 7 : return "bool"; // T-Matrix
			case 8 : return "string"; // Preview
        }
		return "bool";
    }
    unsigned int Rows() { return active.size(); }
    virtual void GetValueByRow( wxVariant &variant,
				unsigned int row, unsigned int col ) const wxOVERRIDE;
    virtual bool SetValueByRow( const wxVariant &variant,
				unsigned int row, unsigned int col ) wxOVERRIDE;
    virtual bool IsEnabledByRow (unsigned int row, unsigned int col) const wxOVERRIDE;
    void DeleteItem( const wxDataViewItem &item );
};


class O_ButtonRenderer: public wxDataViewCustomRenderer
{
public:
    explicit O_ButtonRenderer(wxDataViewCellMode mode)
        : wxDataViewCustomRenderer("bool", mode, wxALIGN_BOTTOM)//wxALIGN_CENTER)
       { }

    virtual bool Render( wxRect rect, wxDC *dc, int state ) wxOVERRIDE
    {

//        const wxSize sizeCheck = GetTextExtent("Opt");

        int renderFlags = wxALIGN_BOTTOM;

        if ( state & wxDATAVIEW_CELL_PRELIT )
            renderFlags |=
            		wxCONTROL_CURRENT;
        if ( state & wxDATAVIEW_CELL_SELECTED )
        	renderFlags |=
        			wxCONTROL_PRESSED;

        if (!active)
        	return true;
//        	renderFlags |= wxCONTROL_DISABLED;

//        wxRect rectCheck(rect.GetPosition(), sizeCheck);
//        rectCheck = rectCheck.CentreIn(rect, wxHORIZONTAL);

        wxRendererNative::Get().DrawPushButton
                                (
                                    GetView(),
									*dc,
//									"T-matrix",
//									rectCheck,
									rect,
									wxALIGN_BOTTOM
//									wxALIGN_LEFT|wxALIGN_TOP,
//									wxCONTROL_DISABLED
//									renderFlags
//									wxHDR_SORT_ICON_NONE
                                );

//        RenderText(m_value,
//                   0, // no offset
//                   wxRect(dc->GetTextExtent(m_value)).CentreIn(rect),
//                   dc,
//				   renderFlags);
        return true;
    }

    virtual bool ActivateCell(const wxRect& WXUNUSED(cell),
                              wxDataViewModel *model,
                              const wxDataViewItem& item,
                              unsigned int WXUNUSED(col),
                              const wxMouseEvent *mouseEvent) wxOVERRIDE;

    virtual wxSize GetSize() const wxOVERRIDE
    {
        return GetView()->FromDIP(wxSize(60, 20));
    }

    virtual bool SetValue( const wxVariant &value ) wxOVERRIDE
    {
    	active = value.GetBool ();
        return true;
    }

    virtual bool GetValue( wxVariant &WXUNUSED(value) ) const wxOVERRIDE { return true; }

#if wxUSE_ACCESSIBILITY
    virtual wxString GetAccessibleDescription() const wxOVERRIDE
    {
        return m_value;
    }
#endif // wxUSE_ACCESSIBILITY

    virtual bool HasEditorCtrl() const wxOVERRIDE { return true; }

    virtual wxWindow*
    CreateEditorCtrl(wxWindow* parent,
                     wxRect labelRect,
                     const wxVariant& value) wxOVERRIDE
    {
        wxTextCtrl* text = new wxTextCtrl(parent, wxID_ANY, value,
                                          labelRect.GetPosition(),
                                          labelRect.GetSize(),
                                          wxTE_PROCESS_ENTER);
        text->SetInsertionPointEnd();

        return text;
    }

    virtual bool
    GetValueFromEditorCtrl(wxWindow* ctrl, wxVariant& value) wxOVERRIDE
    {
        wxTextCtrl* text = wxDynamicCast(ctrl, wxTextCtrl);
        if ( !text )
            return false;

        value = text->GetValue();

        return true;
    }

private:
//    wxString m_value;
    bool active = false;
};

class T_ButtonRenderer: public wxDataViewCustomRenderer
{
public:
    explicit T_ButtonRenderer(wxDataViewCellMode mode)
        : wxDataViewCustomRenderer("bool", mode, wxALIGN_CENTER)
       { }

    virtual bool Render( wxRect rect, wxDC *dc, int state ) wxOVERRIDE
    {

//        const wxSize sizeCheck = GetTextExtent("Opt");

        int renderFlags = 0;

        if ( state & wxDATAVIEW_CELL_PRELIT )
            renderFlags |=
            		wxCONTROL_CURRENT;
        if ( state & wxDATAVIEW_CELL_SELECTED )
        	renderFlags |=
        			wxCONTROL_PRESSED;

        if (!active) {
//        	renderFlags |= wxCONTROL_DISABLED;

        	return true;
        }

//        wxRect rectCheck(rect.GetPosition(), sizeCheck);
//        rectCheck = rectCheck.CentreIn(rect, wxVERTICAL);

        wxRendererNative::Get().DrawPushButton
                                (
                                    GetView(),
									*dc,
//									"T-matrix",
									rect,
//									wxALIGN_LEFT|wxALIGN_TOP,
//									wxCONTROL_DISABLED
									renderFlags
//									wxHDR_SORT_ICON_NONE
                                );

//        RenderText(m_value,
//                   0, // no offset
//                   wxRect(dc->GetTextExtent(m_value)).CentreIn(rect),
//                   dc,
//				   renderFlags);
        return true;
    }

    virtual bool ActivateCell(const wxRect& WXUNUSED(cell),
                              wxDataViewModel *model,
                              const wxDataViewItem& item,
                              unsigned int WXUNUSED(col),
                              const wxMouseEvent *mouseEvent) wxOVERRIDE;

    virtual wxSize GetSize() const wxOVERRIDE
    {
        return GetView()->FromDIP(wxSize(60, 20));
    }

    virtual bool SetValue( const wxVariant &value ) wxOVERRIDE
    {
    	active = value.GetBool ();
        return true;
    }

    virtual bool GetValue( wxVariant &WXUNUSED(value) ) const wxOVERRIDE { return true; }

#if wxUSE_ACCESSIBILITY
    virtual wxString GetAccessibleDescription() const wxOVERRIDE
    {
        return m_value;
    }
#endif // wxUSE_ACCESSIBILITY

    virtual bool HasEditorCtrl() const wxOVERRIDE { return true; }

    virtual wxWindow*
    CreateEditorCtrl(wxWindow* parent,
                     wxRect labelRect,
                     const wxVariant& value) wxOVERRIDE
    {
        wxTextCtrl* text = new wxTextCtrl(parent, wxID_ANY, value,
                                          labelRect.GetPosition(),
                                          labelRect.GetSize(),
                                          wxTE_PROCESS_ENTER);
        text->SetInsertionPointEnd();

        return text;
    }

    virtual bool
    GetValueFromEditorCtrl(wxWindow* ctrl, wxVariant& value) wxOVERRIDE
    {
        wxTextCtrl* text = wxDynamicCast(ctrl, wxTextCtrl);
        if ( !text )
            return false;

        value = text->GetValue();

        return true;
    }

private:
//    wxString m_value;
    bool active = false;
};


class SpecifierRenderer: public wxDataViewCustomRenderer
{
public:
    explicit SpecifierRenderer(wxDataViewCellMode mode)
        : wxDataViewCustomRenderer("string", mode, wxALIGN_LEFT)
       { }



    virtual bool Render( wxRect rect, wxDC *dc, int state ) wxOVERRIDE
    {

        const wxSize sizeCheck = GetTextExtent("Opt");

        int renderFlags = 0;

        if ( state & wxDATAVIEW_CELL_PRELIT )
            renderFlags |=
            		wxCONTROL_CURRENT;
        if ( state & wxDATAVIEW_CELL_SELECTED )
        	renderFlags |=
        			wxCONTROL_PRESSED;

//        if (!active)
//        	renderFlags |= wxCONTROL_DISABLED;

        wxRect rectCheck(rect.GetPosition(), sizeCheck);

        RenderText(m_value, 0, rect, dc, state);

        return true;
    }

    virtual bool ActivateCell(const wxRect& WXUNUSED(cell),
                              wxDataViewModel *model,
                              const wxDataViewItem& item,
                              unsigned int WXUNUSED(col),
                              const wxMouseEvent *mouseEvent) wxOVERRIDE {
    	return true;
    }

    virtual wxSize GetSize() const wxOVERRIDE
    {
        return GetView()->FromDIP(wxSize(60, 20));
    }

    virtual bool SetValue( const wxVariant &value ) wxOVERRIDE
    {
    	m_value = value.GetString ();
        return true;
    }

    virtual bool GetValue( wxVariant &WXUNUSED(value) ) const wxOVERRIDE { return true; }

//#if wxUSE_ACCESSIBILITY
//    virtual wxString GetAccessibleDescription() const wxOVERRIDE
//    {
//        return m_value;
//    }
//#endif // wxUSE_ACCESSIBILITY

    virtual bool HasEditorCtrl() const wxOVERRIDE { return true; }

    virtual wxWindow*
    CreateEditorCtrl(wxWindow* parent,
                     wxRect labelRect,
                     const wxVariant& value) wxOVERRIDE
    {
        wxTextCtrl* text = new wxTextCtrl(parent, wxID_ANY, value,
                                          labelRect.GetPosition(),
                                          labelRect.GetSize(),
                                          wxTE_PROCESS_ENTER);
        text->SetInsertionPointEnd();
        text->SetMaxLength(1);
        return text;
    }

    virtual bool
    GetValueFromEditorCtrl(wxWindow* ctrl, wxVariant& value) wxOVERRIDE
    {
        wxTextCtrl* text = wxDynamicCast(ctrl, wxTextCtrl);
        if ( !text )
            return false;

        value = text->GetValue();

        return true;
    }

private:
    wxString m_value;
//    bool active = false;
};



class Tmatrix_model: public wxDataViewVirtualListModel {
public:
	Tmatrix_model(SPTYPE i_sp_type);
	~Tmatrix_model(){};
	wxVector<bool> active;
	wxArrayString find;
	wxArrayString replace;
	SPTYPE sp_type;
	void Add(bool a_active,
		wxString a_find,
		wxString a_replace) {
		active.push_back(a_active);
		find.Add(a_find);
		replace.Add(a_replace);
	}
	virtual unsigned int GetColumnCount() const wxOVERRIDE { return 3; }
	virtual wxString GetColumnType( unsigned int col ) const wxOVERRIDE
	    {
	        switch (col) {
	        	case 0 : return "bool";	// active
				case 1 : return "string";	// find
				case 2 : return "string";	// replace
	        }
			return "string";
	    }
    virtual void GetValueByRow( wxVariant &variant,
				unsigned int row, unsigned int col ) const wxOVERRIDE;
    virtual bool SetValueByRow( const wxVariant &variant,
				unsigned int row, unsigned int col ) wxOVERRIDE;
    virtual bool IsEnabledByRow (unsigned int row, unsigned int col) const wxOVERRIDE;
    void DeleteItem( const wxDataViewItem &item );
    unsigned int Rows() { return active.size(); }
};


