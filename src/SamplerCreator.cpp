/********************************************************************************
 * 										*
 * 		Copyright (C) 2021 	Kolja Koch				*
 * 							<prog@koljakoch.de>	*
 * 										*
 * 		This file is part of gigCreator					*
 * 										*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or	*
 *     	(at your option) any later version					*					*
 *     										*
 *     	This program is distributed in the hope that it will be useful,		*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		*
 *     	GNU General Public License for more details.				*
 *     										*
 *     	You should have received a copy of the GNU General Public License	*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     										*
 ********************************************************************************/

//#include "PrepareGig.h"

class sample_prop {

public:
	std::string name = "";
	SPTYPE type = SP_NONE;
	std::list<std::pair<sp_text, sp_text> > t_matrix;
	sample_prop (SPTYPE ttype, std::string tname, std::list<std::pair<sp_text, sp_text> > tt_matrix) {
		type = ttype;
		name = tname;
		t_matrix = tt_matrix;
	};
	virtual bool set (sp_value value, sp_value* return_val) {
		sp_text* text = static_cast<sp_text*>(value);
		if (*text != "") {
//			sp_text* retval = new sp_text(*text);
//			*return_val = static_cast<sp_value>(retval);
			*return_val = new sp_text(*text);
			return true;
		}
		else
			return false;
	}
	virtual bool compare (sp_value first, sp_value second) {
		sp_text* tfirst = static_cast<sp_text*>(first);
		sp_text* tsecond = static_cast<sp_text*>(second);
		if (*tfirst == *tsecond) {
		    return true;
		}
		return false;
	}
	virtual bool add (sp_value first, sp_value* second) {
		std::cout << "?????????" << std::endl;
		return true;
	}
	virtual bool valid (sp_value value) {
		return true;
	}
	virtual bool findforwards (sp_text find, sp_text* found) {
//		std::cout << "SP:Findforwards = false" << std::endl;
		return false;
	}
	virtual bool findbackwards (sp_text find, sp_text* found, size_t* start) {
		return false;
	}
};

class sample_prop_midival : public sample_prop {
public:
	sample_prop_midival (SPTYPE type, std::string name,
			std::list<std::pair<sp_text, sp_text> > tt_matrix) : sample_prop (type, name, tt_matrix) {}
	bool set (sp_value value, sp_value* return_val) {
		sp_text* text = static_cast<sp_text*>(value);
		for (auto val_it : *text) {
			if (!isdigit(val_it))
				return false;
		}
		int val = atoi(text->c_str());
//		if (type == SP_NOTE)
//			val += options.transpose;
		if (val < 0 || val > 127)
			return false;
		*return_val = new sp_midival(val);
		return true;
	}
	bool compare (sp_value first, sp_value second) {
		sp_midival* tfirst = static_cast<sp_midival*>(first);
		sp_midival* tsecond = static_cast<sp_midival*>(second);
		if (*tfirst == *tsecond)
			return true;
		return false;
	}
	bool valid (sp_value value) {
		sp_midival* val = static_cast<sp_midival*>(value);
		if (*val < 0 || *val > 127)
			return false;
		return true;
	}
};

class sample_prop_file : public sample_prop {
public:
	sample_prop_file (SPTYPE type, std::string name) : sample_prop (type, name, {}) {}
	virtual bool set (sp_value value, sp_value* return_val) {
		sp_file* file = static_cast<sp_file*>(value);
		if (*file != "") {
//			sp_text* retval = new sp_text(*text);
//			*return_val = static_cast<sp_value>(retval);
			*return_val = new sp_file(*file);
			return true;
		}
		else
			return false;
	}
	virtual bool compare (sp_value first, sp_value second) {
		sp_file* tfirst = static_cast<sp_file*>(first);
		sp_file* tsecond = static_cast<sp_file*>(second);
//		std::cout << "Compare: " << name << ":" << *tfirst << ":" << *tsecond << std::endl;
		if (*tfirst == *tsecond) {
//			std::cout << "Match: " << name << ":" << *tfirst << std::endl;
			return true;
		}
		return false;
	}
	virtual bool add (sp_value first, sp_value* second) {
		std::cout << "?????????" << std::endl;
		return false;
	}
	virtual bool valid (sp_value value) {
		return true;
	}
};

sample_prop_file* SamplerCreator::file_sp = new sample_prop_file (SP_FILENAME, "Files");

class sample_prop_dimension : public sample_prop {
public:
	gig::dimension_t dim;
	sample_prop_dimension (SPTYPE type, gig::dimension_t tdim, std::string name,
			std::list<std::pair<sp_text, sp_text> > tt_matrix)
			: sample_prop (type, name, tt_matrix) {
		dim = tdim;
	}
	bool set (sp_value value, sp_value* return_val) {
		sp_text* val = static_cast<sp_text*>(value);
		int intval = -1;
		if (t_matrix.size() > 0) {
			for (auto t : t_matrix) {
	//			std::cout << "Dimension-t_matrix:" << t.first << ":" << (int) t.second << "::" << *val << std::endl;
				if (t.first == *val) {
					intval = atoi(t.second.c_str());
					break;
				}
				if (t.first == "#") {
					for (auto val_it : *val) {
						if (!isdigit(val_it))
							return false;
					}
					intval = atoi(val->c_str());
					break;
				}
			}
		}
		else {
			for (auto val_it : *val)
				if (!isdigit(val_it))
					return false;
			intval = atoi(val->c_str());
		}
		if (intval < 0 || intval > 127)
			return false;
		*return_val = new sp_dimval(intval);
		return true;
	}
	bool compare (sp_value first, sp_value second) {
		sp_dimval* tfirst = static_cast<sp_dimval*>(first);
		sp_dimval* tsecond = static_cast<sp_dimval*>(second);
		if (*tfirst == *tsecond)
			return true;
		return false;
	}
	bool valid (sp_value value) {
		sp_dimval* val = static_cast<sp_dimval*>(value);
		if (*val < 0 || *val > 127)
			return false;
		return true;
	}
	bool findforwards (sp_text find, sp_text* found) {
//		sp_text ret = "";
		if (t_matrix.size() > 0) {
			for (auto t : t_matrix) {
//				std::cout << "findforwads: check <" << t.first << "> against <"
//						<< find.substr(0, t.first.length()) << std::endl;
				if (t.first == find.substr(0, t.first.length())) {
					*found = t.first;
					return true;
				}
			}
		}
		return false;
	}
	bool findbackwards (sp_text find, sp_text* found, size_t* start) {
		std::map<size_t, std::pair<sp_text, size_t> > founds; // map<found_length, pair<found_string, begin>
		for (auto t : t_matrix) {
			int begin = find.length() - t.first.length();
			while (begin >= 0) {
				if (t.first == find.substr(begin, t.first.length())) {
				  founds[t.first.length()].first = t.first;
				  founds[t.first.length()].second = begin;
				}
				begin--;
			}
		}
		if (founds.size() == 0)
			return false;
		*found = founds.rbegin()->second.first;
		*start = founds.rbegin()->second.second;
		return true;
	}
};

class sample_prop_notename : public sample_prop {
public:
	sample_prop_notename (SPTYPE type, std::string name,
			std::list<std::pair<sp_text, sp_text> > tt_matrix) : sample_prop (type, name, tt_matrix) {}
	bool set (sp_value value, sp_value* return_val) {
		sp_text* text = static_cast<sp_text*>(value);
		if (valid(value)) {
			*return_val = new sp_text(*text);
			return true;
		}
		return false;
	}
	bool compare (sp_value first, sp_value second) {
		sp_text* tfirst = static_cast<sp_text*>(first);
		sp_text* tsecond = static_cast<sp_text*>(second);
		if (*tfirst == *tsecond) {
//			std::cout << "NoteName match:" << *tfirst << ":" << *tsecond << std::endl;
			return true;
		}
		return false;
	}
	bool valid (sp_value value) {
		sp_text* notename = static_cast<sp_text*>(value);
//		std::cout << "NoteName: " << *notename << std::endl;
//		std::cout << "NoteNr: " << get_note_nr(*notename) << std::endl;
		if (get_note_nr(*notename) < 0)
			return false;
		return true;
	}
	int get_note_nr(sp_text notename) const;
private:
	const std::string NOTES[12] = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

};



bool create_sample_prop (sp_text name, SPTYPE type, gig::dimension_t dim,
		std::list<std::pair<sp_text, sp_text> > t_matrix,
//		std::list<sp_midival> t_matrix,
		sample_prop** sp) {
	switch (type) {
		case SP_SAMPLERFILE :
		case SP_INSTRUMENT :
		case SP_IGNORE :
			{
				*sp = new sample_prop (type, name, t_matrix);
	//				std::cout << "Sample_Prop:" << (**sp).name << std::endl;
				return true;
			}
		case SP_DIMENSION : {
			*sp = new sample_prop_dimension (type, dim, name, t_matrix);
//			sample_prop_dimension** sp = &newsp;
			return true;
		}
		case SP_NOTE : {
			*sp = new sample_prop_midival (type, name, t_matrix);
			return true;
		}
		case SP_NOTENAME : {
			*sp = new sample_prop_notename (type, name, t_matrix);
			return true;
		}
		default : return false;
	}
}


class SamplerCreator::sample_prop_val : public std::pair<sample_prop*, sp_value> {
public:
	spv_pair type_string () const {
		spv_pair ret {};
		std::string ret_value = "";
		switch (this->first->type) {
			case SP_SAMPLERFILE :
			case SP_INSTRUMENT :
			case SP_IGNORE :
			case SP_NOTENAME :
			{
				sp_text* value = static_cast<sp_text*>(this->second);
				ret_value = *value;

				break;
			}
			case SP_DIMENSION :
			case SP_NOTE :
			{
				sp_midival* value = static_cast<sp_midival*>(this->second);
				ret_value = std::to_string(*value);
				break;
			}
			case SP_STEREO :
			{
				sp_steroval* value = static_cast<sp_steroval*>(this->second);
				ret_value = *value;
				break;
			}
			case SP_FILENAME :
			{
				sp_file* value = static_cast<sp_file*>(this->second);
				ret_value = *value;
				break;
			}
			default : return ret;
		}
		ret.first = this->first;
		ret.second = ret_value;
		return ret;
	}
	void remove();
};



class SamplerCreator::Sample : public std::list<sample_prop_val> { // list< sample_prop, value >
public:
	std::string info = "";

    std::string* instr_name {};
    std::string* gig_name {};

    bool pattern_valid = false;
//    uint8_t channels = 0;
	spv_list sample_sps_list () const {
		spv_list ssps_list {};
		for (auto& spv : *this) {
			ssps_list.push_back(spv.type_string());
		}
		return ssps_list;
	}
	bool validate ();// {
	uint8_t nr_files () {
		for (auto& spv : *this)
			if (spv.first->type == SP_FILENAME) {
//				sp_files* files = static_cast<sp_files*>(spv.second);
				return 1;
			}
		return 0;
	}
	void print () {
		std::cout << "\n********** SAMPLE **********" << std::endl;
		for (auto& sps : sample_sps_list())
			std::cout << sps.first->name << ":\t" << sps.second << std::endl;
		std::cout << "Pattern_Valid: " << this->pattern_valid << std::endl;
//		std::cout << "Validate:" << this->validate() << std::endl;
		std::cout << "******************************" << std::endl;
	}
	std::string name() const {
		sp_text notename = "";
		for (auto& spv : *this) {
			switch (spv.first->type) {
				case SP_NOTENAME : {
					notename = *(static_cast<sp_text*>(spv.second));
					break; }
				default: break;
			}
		}
		return *gig_name + "_" + *instr_name + "_" + notename + "_";// + std::to_string(velocity);
	}
	std::string val_str (SPTYPE type) const {
		for (auto& sps : *this)
			if (sps.first->type == type)
				return sps.type_string().second;
		return "";
	}
	void remove();


	// note
	sp_midival get_note() const;
	sp_midival get_velocity() const;
	sp_midival note_range_low = -1;
	sp_midival note_range_high = -1;

	// files
	SF_INFO sfinfo {};
	SF_INSTRUMENT sfinst {};
	bool hasSfInst = false;
	std::string getWavInfo(std::string filename, SF_INFO* sfinfo, SF_INSTRUMENT* sfinstr);
	std::string compare_WavInfo (SF_INFO c_sfinfo, SF_INSTRUMENT c_sfinstr);
	std::string set_file (sp_file filename);
	std::string add_file (file_pair filepair);
	sp_file* get_filename() const;
};


std::string cut_path(const std::string file) {
	
  std::string temp_file = file;
  // cut base-path
  size_t i = file.rfind(options.dir_path, file.length());
  if (i != std::string::npos)
	    temp_file = file.substr(i + options.dir_path.size() + 1, file.npos);
  while (temp_file[0] == sep)
	    temp_file.erase(0,1);
  if (!options.hide_path) return temp_file.substr(0, temp_file.length() - 4);

  // cut relative path
  i = temp_file.rfind(sep, temp_file.length());
  if (i != std::string::npos) {
    return(temp_file.substr(i+1, temp_file.length() - i - 5));
  }
  return(temp_file.substr(0, temp_file.length() - 4));
}


std::string SamplerCreator::GetWavInfo(std::string filename) const {
  std::string ret{};
  SF_INFO sfinfo = {};
  SF_INSTRUMENT sfinst = {};
  SNDFILE* hFile = sf_open(filename.c_str(), SFM_READ, &sfinfo);
  if (!hFile)
	return "\n== Could not open input wav file \n" + filename + " ==";

  switch (sfinfo.channels) {
      case 1:
      case 2:
	break;
      default:
	return "\n== " + std::to_string(sfinfo.channels) + " "
	    "audio channels in WAV file \"" + filename +
	    "\"; this is not supported! ==";
  }

  bool hasSfInst = (sf_command(hFile, SFC_GET_INSTRUMENT,
		  &sfinst, sizeof(sfinst)) != SF_FALSE);
  sf_close(hFile);

  ret.append("\nChannels:\t\t\t" + std::to_string(sfinfo.channels));
  ret.append("\nSamplesPerSecond:\t" + std::to_string(sfinfo.samplerate));
  switch (sfinfo.format & 0xff) {
      case SF_FORMAT_PCM_S8:
      case SF_FORMAT_PCM_16:
      case SF_FORMAT_PCM_U8:
	  ret.append("\nBitDepth:\t\t\t" + std::to_string(16));
	  break;
      case SF_FORMAT_PCM_24:
	  ret.append("\nBitDepth:\t\t\t" + std::to_string(24));
	  break;
      case SF_FORMAT_PCM_32:
	  case SF_FORMAT_FLOAT:
	  ret.append("\nBitDepth:\t\t\t" + std::to_string(32) + "(=> 24)");
	  break;
      case SF_FORMAT_DOUBLE:
	  ret.append("\nBitDepth:\t\t\t" + std::to_string(64) + "(=> not supported)");
	  break;
      default:
	  ret.append("\nBitDepth:\t\t\tnot supported");
	  break;
  }
  ret.append("\nFrames:\t\t\t" + std::to_string(sfinfo.frames));
  if (hasSfInst) {
      ret.append("\n\nBaseNote:\t\t\t" + std::to_string(sfinst.basenote));
      ret.append("\nFineTune:\t\t\t" + std::to_string(sfinst.detune));
      if (sfinst.loop_count && sfinst.loops[0].mode != SF_LOOP_NONE) {
	  switch (sfinst.loops[0].mode) {
	      case SF_LOOP_FORWARD:
		  ret.append("\n\tLoopType:\t\tloop_type_normal");
		  break;
	      case SF_LOOP_BACKWARD:
		  ret.append("\n\tLoopType:\t\tloop_type_backward");
		  break;
	      case SF_LOOP_ALTERNATING:
		  ret.append("\n\tLoopType:\t\tloop_type_bidirectional");
		  break;
	  }
	  ret.append("\n\tLoopStart:\t" + std::to_string(sfinst.loops[0].start));
	  ret.append("\n\tLoopEnd:\t\t" + std::to_string(sfinst.loops[0].end));
	  ret.append("\n\tLoopCount:\t" + std::to_string(sfinst.loops[0].count));
      }
  }
  return ret;
}

SamplerCreator::Sample* SamplerCreator::CreateSample (std::string filename, sp_text specstring) {

//	std::cout << "\n======= Create Sample ========" << std::endl;

	// lambda for getting specifier
  auto get_spec = [&](unsigned int &si) {
    pattern_struct t_pattern;
    if (si < specstring.size() - 1) {
	si++;
	std::string spec_number ="";
	while (isdigit(specstring[si])) {
	    spec_number.append(1,specstring[si]);
	    si++;
	}
	if (spec_number.length() > 0)
	  t_pattern.length = atoi(spec_number.c_str());
	std::string speci(1, specstring[si]); // convert specstring[si] (= char) to string
	if ((options.Specifiers.count(speci)) and (si < specstring.size()))
	  t_pattern.id = specstring[si];
	else {
	    t_pattern.length = spec_number.size() + 1;
	    t_pattern.id = "DELIMITER";
	    t_pattern.must_be = "%" + spec_number;
	}
    }
    else {
	t_pattern.length = 1;
	t_pattern.id = "DELIMITER";
	t_pattern.must_be = "%";
	si++;
    }
    return t_pattern;
  }; // END lambda for getting specifier

  sp_text cut_filename = cut_path(filename);
  Sample* result = new Sample();
  result->info.append(cut_filename + "\n");
  result->info.append("\n === wav-Info === \n"
      + GetWavInfo(filename) + "\n\n ============\n");
  std::list<pattern_struct> pattern;
  std::list<pattern_struct>::iterator pat_it = pattern.begin();
  // Get patterns from specstring
  for (unsigned int si = 0; si < specstring.size(); si++) {
      if (specstring[si] == '%') {
	  pattern_struct t_pattern = get_spec(si);
	  pattern.push_back (t_pattern);
	  if (pattern.size() > 1)
	    ++pat_it;
	  else
	    pat_it = pattern.begin();
      }
      else {
	  if ((pattern.size() == 0) or (pat_it->id != "DELIMITER")) {
	      pattern_struct t_pattern;
	      t_pattern.id = "DELIMITER";
	      pattern.push_back(t_pattern);
	      if (pattern.size() > 1)
		++pat_it;
	      else
		pat_it = pattern.begin();
	  }
	  pat_it->must_be.append(1,specstring[si]);
	  if (pat_it->length == -1)
	    pat_it-> length = 1;
	  else
	    pat_it->length += 1;
      }
  } // END get Patterns from Specstring

  // iterate forward through filename and find pattern->start
  bool skip_next = false;	// if no length (%s): next pattern's start cannot be found (if %)
  pat_it = pattern.begin();
  unsigned int fi;
  for (fi = 0; fi < cut_filename.size(); fi++) {
      if (pat_it->id == "DELIMITER") {	// no %s -> is delimiter
	  if (pat_it->length <= (signed int)(cut_filename.size() - fi)) { // length fits in filename
	      if (cut_filename.substr(fi, pat_it->length) == pat_it->must_be) {
		  pat_it->start = fi;
		  pat_it->end = fi + pat_it->length - 1;
		  fi = fi + pat_it->length - 1;
		  pat_it++;
		  if (pat_it == pattern.end()) break;
		  skip_next = false;	// pattern found -> next pattern's start can be found
		  continue;
	      }
	      else // delimiter not found
		continue;
	  }
	  else { // delimiter's length doesn't fit in filename
	      result->info.append("\n!=PATTERN MISMATH=!\n"
		  "Forward: "
		  "Delimiter doesn't fit in filename:\n"
		  "Delimiter: <" + pat_it->must_be + ">\n"
		  "compared with: <" + cut_filename.substr(fi, cut_filename.npos)
		  + ">");
	      result->set_file(filename);
	      return result;
	  }
      }
      else { // is specifier (%s)
	  if (!skip_next) pat_it->start = fi;
	  if (pat_it->length <= 0)
	    skip_next = true;
	  else
	    fi = fi + pat_it->length - 1;
	  pat_it++;
	  if (pat_it == pattern.end()) break;
      }
  } // END Forward ITeration

//  std::cout << "Pattern after Forwards:" << std::endl;
//  for (auto& pat : pattern ) {
//      std::cout << "id:" << pat.id << ":start:" << (int)pat.start << ":end:" << (int)pat.end
//	  << ":length:" << (int)pat.length << ":must_be:" << pat.must_be << std::endl;
//  }

  // check patterns forwards against t_matrix
  for (auto pat = pattern.begin(); pat != pattern.end(); ++pat) {
      if (pat->id != "DELIMITER") {
	  if (pat->start >= 0) {
	      sp_text found;
	      if ((options.Specifiers.count(pat->id))
		    and (options.Specifiers[pat->id]->findforwards
			 (cut_filename.substr(pat->start, cut_filename.npos), &found))) {
		  pat->length = found.length();
	      }
	      if (pat->length >= 0) {
		  pat->end = pat->start + pat->length - 1;
		  auto next = std::next(pat);
		  if (next != pattern.end() and pat->end + 1 < (int) cut_filename.length())
		    next->start = pat->end + 1;
	      }
	  }
      }
  }

//  std::cout << "\nPattern after check t_matrix:" << std::endl;
//  for (auto& pat : pattern ) {
//      std::cout << "id:" << pat.id << ":start:" << (int)pat.start << ":end:" << (int)pat.end
//	  << ":length:" << (int)pat.length << ":must_be:" << pat.must_be << std::endl;
//  }

  // iterate backwards through filename
  auto r_pat_it = pattern.rbegin();
  for (int fi = cut_filename.size() - 1; fi >= 0; fi--) {
	  // check if pattern-info already complete
      if (r_pat_it != pattern.rend()
	    and r_pat_it->start >= 0
	    and r_pat_it->length >= 0
	    and r_pat_it->end >= 0) {
	  fi = r_pat_it->start;
	    ++r_pat_it;
	    continue;
	  }
	  if (r_pat_it == pattern.rend())
	    break;
	  if (r_pat_it->id == "DELIMITER") { // is delimiter
	      if (r_pat_it->length - 1 <= fi) { // delimiter's length fits in filename
		  if (cut_filename.substr(fi - r_pat_it->length + 1, r_pat_it->length) == r_pat_it->must_be) {
		      r_pat_it->end = fi;
		      fi = fi - r_pat_it->length + 1;
		      ++r_pat_it;
		      if (r_pat_it == pattern.rend()) break;
		      continue;
		  } else {
		      result->info.append("\n!=PATTERN MISMATH=!\n"
			  "Backwards: "
			  "delimiter doesn't match:\n"
			  "delimiter: <" + r_pat_it->must_be + ">\n"
			  "compared with: <" + cut_filename.substr(fi, r_pat_it->must_be.size())
			  + ">");
		      result->set_file(filename);
		      return result;
		  }
	      } else { // delimiter's length doesn't fit in filename
		  result->info.append("\n!=PATTERN MISMATH=!\n"
		      "Backwards: "
		      "last delimiter doesn't fit in filename:\n"
		      "delimiter: <" + r_pat_it->must_be + ">\n"
		      "compared with: <" + cut_filename.substr(fi, cut_filename.npos)
		      + ">");
		  result->set_file(filename);
		  return result;
	      }
	  }
	  else { // is specifier (%s)
	      r_pat_it->end = fi;
	      if (std::next(r_pat_it) == pattern.rend()) break;
	      if (r_pat_it->length > 0 )
		fi = fi - r_pat_it->length + 1;
	      else if (r_pat_it->start > 0 ) fi = r_pat_it->start;
		++r_pat_it;
	  }
  } // END backward iteration

//  std::cout << "\nPattern after Backwards:" << std::endl;
//  for (auto& pat : pattern ) {
//      std::cout << "id:" << pat.id << ":start:" << (int)pat.start << ":end:" << (int)pat.end
//	  << ":length:" << (int)pat.length << ":must_be:" << pat.must_be << std::endl;
//  }

  // check patterns backwards against t_matrix
  for (auto pat = pattern.rbegin(); pat != pattern.rend(); ++pat) {
      if (pat->id != "DELIMITER") {
	  if (pat->end >= 0) {
	      sp_text found;
	      size_t begin;
	      size_t start = 0;
	      if (pat->length > 0)
		start = pat->end - pat->length + 1;
	      if (options.Specifiers.count(pat->id)) {
		  if (options.Specifiers[pat->id]->findbackwards
		    (cut_filename.substr(start, pat->end + 1), &found, &begin)) {
		      pat->length = found.length();
		      pat->start = begin + start;
		      pat->end = pat->start + pat->length - 1;
		      auto prev = std::next(pat);
		      if (prev != pattern.rend())
			prev->end = pat->start - 1;
		      auto next = std::prev(pat);
		      if (next != pattern.rbegin())
			next->start = pat->end + 1;
		  }
	      }
	  }
      }
  }

//	std::cout << cut_filename << std::endl;
//	std::cout << "\nPattern after Backwards-t_matrix:" << std::endl;
//
//	for (auto& pat : pattern ) {
//		std::cout << "id:" << pat.id << ":start:" << (int)pat.start << ":end:" << (int)pat.end
//				<< ":length:" << (int)pat.length << ":must_be:" << pat.must_be
//				<< ":" << ":is:";
//		if (pat.start >= 0) {
//			if (pat.length >=0 and pat.start + pat.length <=  (int) cut_filename.length())
//				std::cout << cut_filename.substr(pat.start, pat.length);
//			else if (pat.end >= 0 and pat.end >= pat.start)
//				std::cout << cut_filename.substr(pat.start, pat.end - pat.start + 1);
//		}
//		else
//			if (pat.length >= 0 and pat.end >= 0 and pat.length + pat.end <= (int) cut_filename.length())
//				std::cout << cut_filename.substr(pat.end - pat.length, pat.length);
//		std::cout << std::endl;
//	}



  // check pattern-sanity
  for (std::list<pattern_struct>::iterator pat_it = pattern.begin(); pat_it != pattern.end(); ++pat_it) {

	  // check if NONE of start and end are available
	  if ((pat_it->start < 0) and
			  (pat_it->end <= 0)) {
		  result->info.append("\n!=PATTERN MISMATH=!\n"
		  "Afterwards: "
		  "neither length nor start for pattern found.\n");
		  result->set_file(filename);
		  return result;
	  }

	  // We need start and length
	  if ((pat_it->start >= 0) and
			  (pat_it->end >= 0) and
			  (pat_it->length <= 0)) {
		  pat_it->length = pat_it->end - pat_it->start + 1;
	  }

	  else if ((pat_it->start < 0) and
			  (pat_it->end >= 0) and
			  (pat_it->length > 0)) {
		  pat_it->start = pat_it->end - pat_it->length + 1;
	  }

	  // check if ALL of start, length, end are available
	  if ((pat_it->start >= 0) and
			  (pat_it->length > 0) and
			  (pat_it->end >= 0))
		  continue;
	  else {
		  result->info.append("\n!=PATTERN MISMATH=!\n"
		  "Afterwards: "
		  "could not resolve pattern parameter:\n");
		  result->set_file(filename);
		  return result;
	  }
  }

//  for (auto& pat : pattern ) {
//	  std::cout << "id:" << pat.id << ":start:" << (int)pat.start << ":end:" << (int)pat.end
//			  << ":length:" << (int)pat.length << ":must_be:" << pat.must_be
//			  << ":" << ":is:";
//	  if (pat.start >= 0) {
//		  if (pat.length >=0 and pat.start + pat.length <=  (int) cut_filename.length())
//			  std::cout << cut_filename.substr(pat.start, pat.length);
//		  else if (pat.end >= 0 and pat.end >= pat.start)
//			  std::cout << cut_filename.substr(pat.start, pat.end - pat.start);
//	  }
//	  else
//		  if (pat.length >= 0 and pat.end >= 0 and pat.length + pat.end <= (int) cut_filename.length())
//			  std::cout << cut_filename.substr(pat.end - pat.length, pat.length);
//	  std::cout << std::endl;
//  }

	// Get Value from Pattern
  std::map<std::string, std::string> patterns;
  for (auto pat_it = pattern.begin(); pat_it != pattern.end(); ++pat_it) {
	  if (pat_it->id != "DELIMITER") { // != delimiter
		  patterns[pat_it->id] += cut_filename.substr(pat_it->start, pat_it->length);
	  }
  }
  // Check sanity and construct SpecResult with info
  result->pattern_valid = true;
  for (auto p_it : patterns) {
      if ((options.Specifiers.count(p_it.first)) and (options.Specifiers[p_it.first]->type != SP_IGNORE)) {
	  sp_value temp {};
	  sample_prop* t_spv = options.Specifiers[p_it.first];
	  if (t_spv->set(&p_it.second, &temp)) {
	      sample_prop_val temp_spv;
	      temp_spv.first = t_spv;
	      temp_spv.second = temp;
	      result->push_back(temp_spv);
	      result->info.append("\n" + t_spv->name + ":\t" + p_it.second);
	  }
	  else {
	      result->pattern_valid = false;
	      result->info.append("\n" + t_spv->name + ":\t[" + p_it.second + "]");
	  }
      }
  }
  result->info.append("\n");
  result->set_file(filename);
  return result;
}

int sample_prop_notename::get_note_nr(sp_text notename) const{
  int note = -1;
  int sign = 0;
  int oct = 0;
  for (int i = 0; i < 12; i++)
	  if (NOTES[i].size() == 1)
		  if (toupper(notename[0]) == toupper(NOTES[i][0]))
			  note = i;
  if (note == -1)
	  return -10;
  notename.erase(0,1);
  if (notename.compare(0,options.s_sharp.length(), options.s_sharp) == 0) {	// is sharp-sign?
	  sign = 1;
	  notename.erase(0,options.s_sharp.length());
  }
  else if (notename.compare(0,options.s_flat.length(), options.s_flat) == 0) { // is flat-sign?
	  sign = -1;
	  notename.erase(0,options.s_flat.length());
  }
  if (!regex_match (notename, std::regex("\\d?[ ]*")))
	  return -10;
  oct = atoi(notename.c_str());
  return 24 + note + sign + oct*12;
}

sp_file* SamplerCreator::Sample::get_filename () const {
  for (auto& spv : *this)
    if (spv.first->type == SP_FILENAME)
      return static_cast<sp_file*>(spv.second);
  return NULL;
}

std::string SamplerCreator::Sample::set_file (sp_file filename) {
  std::string ret = "";
  sp_value newfile {};
  if (file_sp->set(&filename, &newfile)) {
      sample_prop_val spv {};
      spv.first = static_cast<sample_prop*>(file_sp);
      spv.second = newfile;
      this->push_back(spv);
  }
  else
    ret.append("\n== file could not be set! ==");
  return ret;
}

std::string SamplerCreator::Sample::compare_WavInfo (SF_INFO c_sfinfo, SF_INSTRUMENT c_sfinst) {
  std::string ret = "";
  // compare SF_INFO
  if (sfinfo.channels != c_sfinfo.channels)
	  ret.append("\n=== file-channels don't match! ===");
  if (sfinfo.samplerate != c_sfinfo.samplerate)
	  ret.append("\n=== samplerates don't match! ===");
  if (sfinfo.format != c_sfinfo.format)
	  ret.append("\n=== format doesn't match! ===");

  // compare SF_INSTRUMENT
  if (sfinst.basenote != c_sfinst.basenote)
	  ret.append("\n=== basenotes don't match! ===");
  if (sfinst.detune != c_sfinst.detune)
	  ret.append("\n=== detune doesn't match! ===");
  if (sfinst.loop_count != c_sfinst.loop_count)
	  ret.append("\n=== loop_counts don't match! ===");
  if (sfinst.loops[0].mode != c_sfinst.loops[0].mode)
	  ret.append("\n=== loop-modes don't match! ===");
  if (sfinst.loops[0].start != c_sfinst.loops[0].start)
	  ret.append("\n=== loop-starts don't match! ===");
  if (sfinst.loops[0].end != c_sfinst.loops[0].end)
	  ret.append("\n=== loop-ends don't match! ===");
  return ret;
}

std::string SamplerCreator::Sample::add_file (file_pair filepair) {
  std::string ret = "";
  SF_INFO c_sfinfo {};
  SF_INSTRUMENT c_sfinst {};
  ret = getWavInfo (filepair.second, &c_sfinfo, &c_sfinst);
  if (ret == "") { // no problems with wavInfo
      std::string comp_info = compare_WavInfo(c_sfinfo, c_sfinst);
      if ( comp_info == "")
	for (auto& spv : *this) {
	    if (spv.first->type == SP_FILENAME) {
		if (file_sp->add(&filepair, &spv.second))
		  break;
		else ret.append("\n== file could not be added! ==");
	    }
	}
      else
	ret.append("\n == file-infos do not match existing infos: ==" + comp_info);
  }
  return ret;
}

std::string SamplerCreator::Sample::getWavInfo(std::string filename, SF_INFO* sfinfo, SF_INSTRUMENT* sfinst) {
  SNDFILE* hFile = sf_open(filename.c_str(), SFM_READ, sfinfo);
  std::string ret = "";
  if (!hFile) {
	  ret.append("\n== Could not open input wav file \n" + filename + "\" ==");
	  return ret;
  }
  hasSfInst = (sf_command(hFile, SFC_GET_INSTRUMENT,
		  sfinst, sizeof(*sfinst)) != SF_FALSE);
  sf_close(hFile);
  switch (sfinfo->channels) {
	  case 1:
	  case 2:
		  break;
	  default:
		  ret.append("\n== " + std::to_string(this->sfinfo.channels) + " "
				  "audio channels in WAV file \"" + filename +
				  "\"; this is not supported! ==");
		  return ret;
  }
  return ret;
}

sp_midival SamplerCreator::Sample::get_note() const {
  for (auto& spv : *this) {
      if (spv.first->type == SP_NOTE)
	return (*(static_cast<sp_midival*>(spv.second)));
  }
  return -1;
}

sp_midival SamplerCreator::Sample::get_velocity() const {
  for (auto& spv : *this) {
      if (spv.first->type == SP_DIMENSION) {
	  sample_prop_dimension* dimension = static_cast<sample_prop_dimension*>(spv.first);
	  if (dimension->dim == gig::dimension_velocity)
	    return *(static_cast<sp_midival*>(spv.second));
      }
  }
  return -1;
}

void SamplerCreator::Sample::remove() {
  for (auto& spv_it : *this) {
      spv_it.remove();
  }
}

void SamplerCreator::sample_prop_val::remove() {
  SPTYPE type = this->first->type;
  switch (type) {
    case SP_SAMPLERFILE :
    case SP_INSTRUMENT :
    case SP_IGNORE :
    case SP_NOTENAME :
      {
	sp_text* value = static_cast<sp_text*>(this->second);
	delete value;
	break;
      }
    case SP_FILENAME :
      {
	sp_file* file = static_cast<sp_file*>(this->second);
	delete file;
	break;
      }
    case SP_DIMENSION :
    case SP_NOTE : {
      sp_midival* value = static_cast<sp_midival*>(this->second);
	delete value;
	break;
    }
    default : break;
  }
}


#include "Sampler_class.cpp"


SamplerCreator::SamplerCreator (
    SAMPLER_TYPE tsampler_type,
    std::set<FILENAME> ifiles,
    std::list<SPECSTRING> ispecstrings,
    SamplerDisplay* iSamplerDisplayModel) {
  sampler_type = tsampler_type;
  samplerresult = new SamplerResult{};
  specstrings = ispecstrings;
  SamplerDisplayModel = iSamplerDisplayModel;

  switch (sampler_type) {
  case SAMPLER_TYPE_GIG :
	  Sampler = new gig_sampler (
	  sampler_type ,
	  samplerresult,
	  SamplerDisplayModel);
	  break;
  default :
	  Sampler = new Sampler_class (
	    sampler_type ,
	    samplerresult,
	    SamplerDisplayModel);
	  break;
  }
  for (auto& ifile : ifiles) {
      files.insert(ifile);
  }
}


RESULT SamplerCreator::GetResult(SamplerResult** result) {

	SamplerDisplay_node* InValid = NULL;


	sp_text eval_result = "";
	bool eval_valid = false;
	RESULT ret = "";
	if (files.size() <= 0)
		return "No files to scan";
	if (specstrings.size() <= 0)
		return "No patterns for scanning";


	// create process_files
	init_process_files();
	std::list <FILENAME> to_be_deleted;
	for (auto spec_str : specstrings) {
		if (InValid != NULL)
		  InValid->Delete_children();
		for (auto& file : process_files) {
		    Sample* result_sample;
		    result_sample = CreateSample (file, spec_str);
		    if (result_sample->pattern_valid) {	// pattern is valid
			eval_valid = Sampler->evaluate_sample(result_sample, &eval_result);
			if (eval_valid)  // sample is added ok
			  to_be_deleted.push_back(file);
		    }
		    if (!result_sample->pattern_valid || !eval_valid) {	// pattern is invalid
			if (InValid == NULL) {
			InValid = new SamplerDisplay_node(
			    NULL,
			    "[invalid]",
			    std::pair<int, void*>(0, new std::string ("Invalid files")),
			    true);
			SamplerDisplayModel->m_SamplerDisplayNodes.push_front(InValid);
			}
			sp_file* filename = result_sample->get_filename();
			InValid->Append(
			    new SamplerDisplay_node(
				InValid,
				cut_path(*filename),
				std::pair<int, void*>(0, new sp_text (result_sample->info + eval_result)),
				false)
			  );
		    } // END pattern is invalid
		} // End process files
		for (auto& tbd : to_be_deleted)
		  process_files.erase(tbd);
	}
	*result = samplerresult;
	if (options.expand_all_ranges)
	  Sampler->ExpandAllRanges();
	ret = Sampler->CreateSamplerDisplayModel ();
	if (InValid != NULL) {
	    if (InValid->GetChildCount() < 1)
	      SamplerDisplayModel->m_SamplerDisplayNodes.pop_front();
	    else
	      InValid->m_Numbers = std::to_string(InValid->GetChildCount()) + " files";
	}
	return ret;
}

std::map<std::string, std::string> SamplerCreator::GetPreview
		(std::list<std::string> SpecstringList, std::string file) {
	Sample* result_sample;
	for (auto& specstring : SpecstringList) {
		result_sample = CreateSample (file, specstring);
		if (result_sample->pattern_valid) {
			break;
		}
	}
	std::map<std::string, std::string> preview{};
	for (auto& spv : *result_sample) {
		for (auto& options_sp : options.Specifiers) {
			if (spv.first == options_sp.second) {
				preview[options_sp.first] = spv.type_string().second;
				break;
			}
		}
	}
	return preview;
}






