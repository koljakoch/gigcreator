/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/


#include <wx/notebook.h>
#include <wx/valnum.h>
#include <map>

#include <wx/spinctrl.h>


struct options_struct {

	// General
	std::string dir_path = wxGetHomeDir().utf8_string();
	bool recursive = false;
	bool hide_path = true;

	std::string gigs_path = wxGetHomeDir().utf8_string();
	bool gigs_use_wav_directory = true;
	bool overwrite_gigs = false;

	bool expand_all_ranges = true;
	bool overwride_wavInfo = false;
	bool direct_scan = false;

	std::string specinput = "";

	// Notes
	short int transpose = 0;
	short int override_note = -1;
	bool do_override_note = false;
	bool always_create_notenames = true;

	std::string s_sharp = " ";
	std::string s_flat = "b";
	std::string p_sharp = "#";
	std::string p_flat = "b";

	// Velocity
	std::map<std::string, short> velocity_translate;

	//Specifiers
	std::map<std::string, sample_prop*> Specifiers;

	struct GIG_struct {
		std::string Instrument = "unnamed";
		std::string Gig = "unnamed";
	} GIG;




} options;

class Options : public wxDialog {
    	
    public:
    	Options();
    	void OnOK(wxCommandEvent&);
    	void OnCancel(wxCommandEvent&);


    private:
		wxDECLARE_EVENT_TABLE();
		
		// General
		wxCheckBox* expand_ranges;
		wxDirPickerCtrl* dir_path;
		wxString temp_dir_path = options.dir_path;
		wxCheckBox *recursive;
		wxCheckBox *hide_path;
		
		wxDirPickerCtrl* gigs_path;
		wxString temp_gigs_path = options.gigs_path;
		wxCheckBox* gigs_use_wav_directory;
		wxCheckBox* overwrite_gigs;
		wxCheckBox* direct_scan;
		
//		// Notes
//		wxTextCtrl* Transpose;
//
//		wxTextCtrl* s_sign_sharp;
//    	wxTextCtrl* s_sign_flat;
//		wxTextCtrl* p_sign_sharp;
//		wxTextCtrl* p_sign_flat;
//		wxCheckBox* create_note_names;
//		wxCheckBox* prefer_pattern;
//
//		// Velocity
//		wxDataViewListCtrl* velocity_translate;
//		void OnVelocityTranslateEditing(wxDataViewEvent& event);
//		void OnVelocityTranslateEditingEnd(wxDataViewEvent& event);

//		void OverrideNoteInput(wxCommandEvent& event);
		void OnGigsUseFilePath(wxCommandEvent& event);
		
		void On_Dir_Path_Change(wxFileDirPickerEvent& event);
		void On_Gigs_Path_Change(wxFileDirPickerEvent& event);
		
		void ApplyOptions();
		void OnLoad(wxCommandEvent& WXUNUSED(event));
		void OnSave(wxCommandEvent& WXUNUSED(event));
    };

class Save_SamplerFiles_progress : public wxDialog {
public:
	Save_SamplerFiles_progress();
	unsigned int start_char;

private:
	void OnCancel(wxCommandEvent& WXUNUSED(event));
	void OnOK(wxCommandEvent& WXUNUSED(event));
	void OnIdle(wxIdleEvent& event);
	void OnClose(wxCloseEvent& event);
	
	wxButton* OK_Button;
	wxButton* Cancel_Button;
	
	
	unsigned int end_char = 0;
	
	wxDECLARE_EVENT_TABLE();
};

class SpecOptions_Tmatrix : public wxDialog {

    public:
		SpecOptions_Tmatrix(SpecOptions_model* spon, unsigned int row);
    	void OnOK(wxCommandEvent&);
    	void OnCancel(wxCommandEvent&);
    	wxObjectDataPtr<Tmatrix_model> TMatrix;



    private:
		wxDECLARE_EVENT_TABLE();
		bool changed = false;
		wxDataViewCtrl *Tmatrix_ctrl;
		SpecOptions_model* spom;
		unsigned int so_row;
		void ApplyOptions();
		void StartEdit(wxDataViewEvent& event);
		void StopEdit(wxDataViewEvent& event);
    };

class SpecOptions_Note : public wxDialog {

    public:
	SpecOptions_Note(SpecOptions_model* spon, unsigned int row);
    	void OnOK(wxCommandEvent&);
    	void OnCancel(wxCommandEvent&);

    private:
		wxDECLARE_EVENT_TABLE();
		bool changed = false;
		wxSpinCtrl* Transpose;
	 	wxRadioBox* Override_wavInfo;
		SpecOptions_model* spom;
		unsigned int so_row;
//		void ApplyOptions();
		void transpose_Changed(wxSpinEvent& event);
		void use_wav_Changed(wxCommandEvent& event);
    };


class SpecOptions_Notenames : public wxDialog {

    public:
	SpecOptions_Notenames(SpecOptions_model* spon, unsigned int row);
    	void OnOK(wxCommandEvent&);
    	void OnCancel(wxCommandEvent&);

    private:
		wxDECLARE_EVENT_TABLE();
		bool changed = false;
		wxTextCtrl* find_sharp_sign;
		wxTextCtrl* find_flat_sign;
//		wxTextCtrl* replace_sharp_sign;
//		wxTextCtrl* replace_flat_sign;
//		wxCheckBox* create_note_names;
		SpecOptions_model* spom;
		unsigned int so_row;
//		void ApplyOptions();
		void TextEdited(wxCommandEvent& event);
    };


