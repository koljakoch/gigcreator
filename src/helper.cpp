/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/
std::string Init_SP() {

//	sample_prop* sp {};
//
//	if (create_sample_prop ("gig-name", SP_SAMPLERFILE,	&sp))
//		options.Specifiers["n"] = sp;
//	else return "Init SP_SAMPLERFILE failed";
//
//	if (create_sample_prop ("instr-name", SP_INSTRUMENT, &sp))
//		options.Specifiers["m"] = sp;
//	else return "Init Instr_name failed";
//
//	if (create_sample_prop ("Velocity", SP_DIMENSION,
//			gig::dimension_velocity, {
//					{"Pianissimo", 33},
//					{"Piano", 64},
//					{"MezzoPiano", 80},
//					{"MezzoForte", 101},
//					{"Forte", 127},
//					{"#",0}}, &sp))
//		options.Specifiers["v"] = sp;
//	else return "Init Velocity failed";
//
//	if (create_sample_prop ("SustainPedal", SP_DIMENSION,
//			gig::dimension_sustainpedal, {
//					{"PedalOff", 0},
//					{"PedalOn", 127},
//					{"#",0}}, &sp))
//		options.Specifiers["p"] = sp;
//	else return "Init SustainPedal failed";
//
//	if (create_sample_prop ("Random", SP_DIMENSION,
//			gig::dimension_random , {
//					{"#",0}}, &sp))
//		options.Specifiers["d"] = sp;
//	else return "Init Random failed";
//
//
//
//
//	if (create_sample_prop ("Note", SP_NOTE, &sp))
//		options.Specifiers["r"] = sp;
//	else return "Init Note failed";
//
//	if (create_sample_prop ("Notename", SP_NOTENAME, &sp))
//		options.Specifiers["a"] = sp;
//	else return "Init NoteName failed";
//
////	if (create_sample_prop ("Stereo", SP_STEREO, "L", "R", &sp))
////		options.Specifiers["s"] = sp;
////	else return "Init Stereo failed";
//
//	if (create_sample_prop ("Stereo", SP_DIMENSION,
//			gig::dimension_samplechannel, {{"L",0}, {"R", 1}}, &sp))
//		options.Specifiers["s"] = sp;
//	else return "Init Stereo failed";
//
//
//	if (create_sample_prop ("Ignore", SP_IGNORE, &sp))
//		options.Specifiers["i"] = sp;
//	else return "Init Ignore failed";

	return "";
}

bool IsMidiValue(std::string value) {
	for (auto val_it = value.begin(); val_it != value.end(); ++val_it)
		if (!isdigit(*val_it))
			return false;
	unsigned short int int_val = atoi(value.c_str());
	if ((int_val < 0 ) or (int_val >127))
		return false;
	return true;
}

short int get_note_nr(std::string notename) {

//	// cut trailing spaces
//	std::string::reverse_iterator r_notename_it=notename.rbegin();
//	while (*r_notename_it == ' ')
//		notename.pop_back();
//	int note = -1;
//	int sign = 0;
//	int oct = 0;
//
//	// check for NoteName
//	for (int i = 0; i < 12; i++)
//		if (NOTES[i].size() == 1)
//			if (toupper(notename[0]) == toupper(NOTES[i][0])) {
//				note = i;
//				break;
//			}
//	if (note == -1) return -1;
//
//	notename.erase(0,1);
//
//	//check for sign
//	if (notename.compare(0,options.s_sharp.size(), options.s_sharp) == 0) {	// is sharp-sign?
//		sign = 1;
//		notename.erase(0,options.s_sharp.size());
//	}
//	else if (notename.compare(0,options.s_flat.size(), options.s_flat) == 0) { // is flat-sign?
//		sign = -1;
//		notename.erase(0,options.s_flat.size());
//	}
//
//	// check for octave
//	if (!regex_match (notename, std::regex("\\d?[ ]*")))
//		return -1;
//	oct = atoi(notename.c_str());
//	return 24 + note + sign + oct*12;
	return 60;
}

short get_channel (std::string to_stereo) {
	if (to_stereo[0] == 'L')
		return 0;
	if (to_stereo[0] == 'R')
			return 1;
	return 2;
}



