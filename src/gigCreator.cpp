/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/


#include "wx/wxprec.h"
#include "wx/filepicker.h"
#include <wx/listctrl.h>
#include "wx/dataview.h"
#include <wx/treelist.h>
#include <wx/dir.h>
#include <wx/addremovectrl.h>
//#include <wx/wrapsizer.h>
#include "wx/artprov.h"

#include "wx/image.h"
#include "wx/sysopt.h"
#include "wx/html/htmlwin.h"
#include "wx/html/htmlproc.h"
#include "wx/fs_inet.h"
#include "wx/filedlg.h"
#include "wx/utils.h"
#include "wx/clipbrd.h"
#include "wx/dataobj.h"
#include "wx/stopwatch.h"

#include <wx/event.h>
//#include <wx/richtext/richtextbuffer.h>
#include <list>
// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif


#include <fstream>    
#include <iostream>

#include <thread>

#include "./gigCreator.h"
//gigcontainer* gc = new gigcontainer();
#include "SamplerDisplay_model.cpp"

//#include "PrepareGig.cpp"
#include "options.cpp"
//#include "CreateGig.cpp"
#include "helper.cpp"
#include "SamplerCreator.cpp"
//#include "./PrepareGig.h"
//gigcontainer* gc = new gigcontainer();
//#include "gig_model.cpp"


//#include <wx/cmdline.h>



//#include "./PrepareGig.cpp"
//#include "./options.cpp"
//#include "./CreateGig.cpp"
#include "./gigCreator.xpm"


// ----------------------------------------------------------------------------
// globals
// ----------------------------------------------------------------------------
//SamplerCreator* SC = new SamplerCreator(SAMPLER_TYPE_GIG);
//SamplerCreator* prev_SC = new SamplerCreator(SAMPLER_TYPE_GIG);

std::set<FILENAME> InputFiles;

int gigs_list_item = 0;
std::string gigs_list_id = ""; 

//gigcontainer* gc_prev = new gigcontainer();
wxString Gigs_list_info = "Gigs";
wxString Sample_Info_text = "Samples";

std::string init_dir_path = "";

std::string config_filename = wxGetHomeDir().utf8_string() + DIR_SEPARATOR + ".gigCreator.cfg";
std::string so_filename = wxGetHomeDir().utf8_string() + DIR_SEPARATOR + ".SamplerCreatorSpecOpt.cfg";

// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------

wxString GIG_LIST_INFO = "Samples";
const std::string ALLINSTRUMENTS = "ALLINSTRUMENTS";
std::string APP = "gigCreator";
std::string Init_Pattern_Info =
	"%n = Name\n"
	"%m = Instr.\n"
	"%v = Velocity\n"
	"%r = Note-Nr\n"
	"%a = Note-Name\n"
	"%s = to_Stereo:";


wxDEFINE_EVENT(RESET_EVENT, wxCommandEvent);


wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)
    EVT_MENU(Menue_Quit,  MainFrame::OnQuit)
    EVT_MENU(Menue_About, MainFrame::OnAbout)
	EVT_MENU(HELP,  MainFrame::OnHelp)
	EVT_MENU(MOPTIONS,  MainFrame::OnOptions)
	
	// select files
	EVT_DIRPICKER_CHANGED(PickerPage_Dir, MainFrame::OnDirChange)
	EVT_CHECKBOX(RECURSIVE, MainFrame::Recursive)
	EVT_CHECKBOX(HIDE_PATHS, MainFrame::Hide_Paths)
	EVT_DATAVIEW_SELECTION_CHANGED(SELECT_FILE, MainFrame::On_Select_File)
//	EVT_DATAVIEW_SELECTION_CHANGED(SELECT_FILE, MainFrame::OnSpecInput)
	
	//main buttons
	EVT_BUTTON(wxID_CANCEL, MainFrame::OnQuit)
	EVT_BUTTON(wxID_OK, MainFrame::OnOK)
	
	// pattern-input
	EVT_TEXT(SPEC_FIELD, MainFrame::OnSpecInput)
	EVT_BUTTON(SCAN, MainFrame::On_Scan)
//	EVT_BUTTON(HELP, MainFrame::OnHelp)
//	EVT_BUTTON(OPTIONS, MainFrame::OnOptions)

	// SpecOptions
	EVT_DATAVIEW_ITEM_VALUE_CHANGED(OPTIONSBOX, MainFrame::OnSpecOptionsChanged)
	EVT_COMMAND(wxID_ANY, RESET_EVENT, MainFrame::Reset_Results)

	// gigs_list
	EVT_DATAVIEW_SELECTION_CHANGED(GIGBOX, MainFrame::OnSelectedGig)
//	EVT_DATAVIEW_ITEM_ACTIVATED
	EVT_DATAVIEW_ITEM_START_EDITING(GIGBOX, MainFrame::StartEditGig)
	EVT_DATAVIEW_ITEM_EDITING_DONE(GIGBOX, MainFrame::StopEditGig)
	
	// Samples List
	EVT_BUTTON(SD_NODE_EXPANDRANGES, MainFrame::OnSDNodeCommand)
	EVT_BUTTON(SD_NODE_CONTRACTRANGES, MainFrame::OnSDNodeCommand)
	
	// Overwrite gigs
	EVT_CHECKBOX(OVERWRITEGIGS, MainFrame::OnOverwrite_Gigs)
	
	// help
	EVT_HTML_LINK_CLICKED(HELP, MainFrame::OnHelpLinkClick)
	

	EVT_CHOICE(SP_CHOICE, MainFrame::OnSP_TypeSelect)

wxEND_EVENT_TABLE()

//wxBEGIN_EVENT_TABLE(SPTypeRenderer, wxFrame)
//
//EVT_CHOICE(SP_CHOICE, SPTypeRenderer::OnSP_TypeSelect)
//
//wxEND_EVENT_TABLE()

wxIMPLEMENT_APP(MyApp);

// ============================================================================
// implementation
// ============================================================================

#if wxUSE_CMDLINE_PARSER

#include "wx/cmdline.h"

void MyApp::OnInitCmdLine(wxCmdLineParser& parser) {
	SetAppName("gigCreator");
	parser.SetCmdLine(argc, argv);
	parser.SetDesc(cmdLineDesc);
}

bool MyApp::OnCmdLineParsed(wxCmdLineParser& parser) {
    if (parser.Found("h")) {
    	std::cout << parser.GetUsageString() << std::endl;
    	return false;
    }
    wxString Directory;
    if (parser.Found("d", &Directory)) {
        	init_dir_path = Directory;
        	return true;
        }
    return true;
}

#endif // wxUSE_CMDLINE_PARSER


bool MyApp::OnInit()
{
    if ( !wxApp::OnInit() )
        return false;
    std::string ret = Init_SP();
    if (ret != "") {
    	std::cout << std::endl << ret << std::endl;
    }
    GetOptions();

    mainframe = new MainFrame;
    mainframe->Show(true);
    mainframe->getfiles();
//    frame->listfiles();
    return true;
}



//wxWindow* wxDataViewRenderer::CreateEditorCtrl 	( 	wxWindow *  	parent,
//		wxRect  	labelRect,
//		const wxVariant &  	value
//	) {
//
//}

// ----------------------------------------------------------------------------
// Mainframe
// ----------------------------------------------------------------------------

MainFrame::MainFrame()
       : wxFrame(NULL, wxID_ANY, "gigCreator")
{
	
	SetIcon(gigCreator_xpm);
	
#if wxUSE_MENUBAR
    // create a menu bar
    wxMenu *fileMenu = new wxMenu;

    // the "About" item should be in the help menu
    wxMenu *helpMenu = new wxMenu;
    helpMenu->Append(Menue_About, "&About\tF1", "Show about dialog");
    helpMenu->Append(HELP, "&help");

    fileMenu->Append(Menue_Quit, "E&xit\tAlt-X", "Quit this program");
    fileMenu->Append(MOPTIONS, "Options");

    // now append the freshly created menu to the menu bar...
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, "&File");
    menuBar->Append(helpMenu, "&Help");

    // ... and attach this menu bar to the frame
    SetMenuBar(menuBar);
#else // !wxUSE_MENUBAR
    // If menus are not available add a button to access the about box
    wxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    wxButton* aboutBtn = new wxButton(this, wxID_ANY, "About...");
    aboutBtn->Bind(wxEVT_BUTTON, &MyFrame::OnAbout, this);
    sizer->Add(aboutBtn, wxSizerFlags().Center());
    SetSizer(sizer);
#endif // wxUSE_MENUBAR/!wxUSE_MENUBAR
    SpecOptions = new SpecOptions_model;
    GetSpecOptions();
	
    wxBoxSizer *mainframe = new wxBoxSizer( wxHORIZONTAL );
    	// Select Files
    	wxPanel *Select_Files_Panel = new wxPanel(this, wxID_ANY);
    	wxBoxSizer* Select_Files_Panel_Sizer = new wxBoxSizer( wxVERTICAL );
			wxStaticBox *Select_Files_box = new wxStaticBox(Select_Files_Panel, wxID_ANY, "&Select Files",
					wxDefaultPosition, 
					wxDefaultSize );
			wxStaticBoxSizer* SelesctFiles_sizer = new wxStaticBoxSizer(Select_Files_box, wxVERTICAL );
//			DirValidator dirval(options.dir_path);
				dir_picker = new wxDirPickerCtrl(Select_Files_box, PickerPage_Dir,
						options.dir_path, "Select Sample-Direcoty",//  "wav files (*.wav)|*.wav",
						wxDefaultPosition, wxDefaultSize, wxDIRP_USE_TEXTCTRL | wxDIRP_SMALL);//,
//						DirValidator(options.dir_path));
				dir_picker->SetPath(options.dir_path);
			SelesctFiles_sizer->Add(dir_picker, 0, wxEXPAND|wxALL, 5);
			
				recursive = new wxCheckBox(Select_Files_box, RECURSIVE, "recursive scan",
						wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
				recursive->SetValue(options.recursive);
			SelesctFiles_sizer->Add(recursive, 0, wxGROW | wxLEFT, 5);
			
				hide_paths = new wxCheckBox(Select_Files_box, HIDE_PATHS, "hide paths",
							wxDefaultPosition, wxDefaultSize, wxCHK_2STATE);
				hide_paths->SetValue(options.hide_path);
			SelesctFiles_sizer->Add(hide_paths, 0, wxGROW | wxLEFT, 5);
			
				init_file_list = new wxDataViewListCtrl(Select_Files_box, SELECT_FILE,
									   wxDefaultPosition, wxDefaultSize);
				init_file_list->AppendTextColumn( "files (.wav)", wxDATAVIEW_CELL_INERT,
						wxCOL_WIDTH_AUTOSIZE, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE  );
				init_file_list->AppendTextColumn( "file", wxDATAVIEW_CELL_INERT,
						wxDVC_DEFAULT_WIDTH, wxALIGN_LEFT, wxDATAVIEW_COL_HIDDEN  );
			SelesctFiles_sizer->Add(init_file_list, 1, wxGROW | wxTOP | wxRIGHT | wxLEFT, 10 );

				nr_of_wav_files = new wxStaticText(Select_Files_box, wxID_ANY,
						wxString("wav-files"),
						wxDefaultPosition, wxDefaultSize,
						wxALIGN_RIGHT | wxST_NO_AUTORESIZE);
			SelesctFiles_sizer->Add(nr_of_wav_files, 0, wxGROW | wxRIGHT, 12);
		Select_Files_Panel_Sizer->Add(SelesctFiles_sizer, 1, wxGROW | wxALL, 5 );	
		Select_Files_Panel->SetSizerAndFit(Select_Files_Panel_Sizer);
	mainframe->Add(Select_Files_Panel, 3, wxGROW | wxALL, 5 );
	
//		// Control
		wxPanel *Control_Panel = new wxPanel(this, wxID_ANY);
		wxBoxSizer *Control_sizer = new wxBoxSizer( wxVERTICAL );
		
//			// Top
			wxBoxSizer *Top_sizer = new wxBoxSizer( wxHORIZONTAL );
//				// PATTERN
				wxStaticBox *Pattern_box = new wxStaticBox(Control_Panel, wxID_ANY, "&Scan-pattern",
						wxDefaultPosition, 
						wxDefaultSize );
				wxStaticBoxSizer *Pattern_sizer = new wxStaticBoxSizer(Pattern_box, wxVERTICAL );

				// Spec_Input
					specinput = new wxTextCtrl( Pattern_box, SPEC_FIELD,
							options.specinput,
							wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);
				Pattern_sizer->Add(specinput, 2, wxGROW |	wxALL, 5 );		
//						
//					// Apply Pattern
					wxBoxSizer *Apply_Pattern_sizer = new wxBoxSizer( wxHORIZONTAL );
//					Apply_Pattern_sizer->Add(0, 0, 1);
////						// Enter_Pattern_Options_Button
//						wxButton* Enter_Pattern_Options_Button = new wxButton( Pattern_box, OPTIONS,
//								"Options", wxDefaultPosition, wxDefaultSize);
//						Enter_Pattern_Options_Button->SetBitmap(wxArtProvider::GetBitmap(wxART_EDIT, wxART_BUTTON));
//					Apply_Pattern_sizer->Add(Enter_Pattern_Options_Button,
//							0, wxLEFT | wxALIGN_CENTER | wxALIGN_LEFT, 0 );
//						
//						// Scan
						Apply_Pattern = new wxButton( Pattern_box, SCAN, "Scan",
								wxDefaultPosition, wxDefaultSize);
						Apply_Pattern->SetBitmap(wxArtProvider::GetBitmap(wxART_FIND, wxART_BUTTON));
						Apply_Pattern->Enable(!options.direct_scan);
					Apply_Pattern_sizer->Add(Apply_Pattern,	0, wxALIGN_CENTER |	wxRIGHT, 0 );
//					
				Pattern_sizer->Add(Apply_Pattern_sizer, 0, wxGROW |	wxBOTTOM | wxLEFT | wxRIGHT, 5 );
			Top_sizer->Add(Pattern_sizer, 2, wxGROW |	wxALL, 5 );
//						
//						
////				// Preview
//				wxStaticBox *Preview_box = new wxStaticBox(Control_Panel, wxID_ANY, "&Preview",
//						wxDefaultPosition, wxDefaultSize );
//				wxStaticBoxSizer *Preview_vsizer = new wxStaticBoxSizer(Preview_box, wxVERTICAL );
//
//					wxBoxSizer *Preview_sizer = new wxBoxSizer(wxHORIZONTAL );
//						// Pattern
//						Pattern_Info = new wxTextCtrl( Preview_box, wxID_ANY, Init_Pattern_Info,
//								wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxHSCROLL);
//					Preview_sizer->Add(Pattern_Info, 1, wxGROW | wxLEFT | wxBOTTOM, 5 );
//
//						// Sample
//						Sample_Info = new wxTextCtrl( Preview_box, wxID_ANY, "Sample-Info",
//								wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxHSCROLL);
//					Preview_sizer->Add(Sample_Info, 1, wxGROW |	wxLEFT | wxBOTTOM, 5 );
						


						// SpecOptions
				wxStaticBox *SpecOptions_box = new wxStaticBox(Control_Panel, wxID_ANY, "&Specifiers",
									wxDefaultPosition, wxDefaultSize );
				wxStaticBoxSizer *SpecOptions_vsizer = new wxStaticBoxSizer(SpecOptions_box, wxVERTICAL );

//					wxBoxSizer *Preview_sizer = new wxBoxSizer(wxHORIZONTAL );

						wxAddRemoveCtrl* const AddRemove = new wxAddRemoveCtrl(SpecOptions_box);


						SpecOptions_ctrl = new wxDataViewCtrl(AddRemove,
								OPTIONSBOX,
								wxDefaultPosition, wxDefaultSize, 0);//wxDV_MULTIPLE);

						SpecOptions_ctrl->AssociateModel( SpecOptions.get() );

						SpecOptions_ctrl->AppendColumn(	// Set if specifier is active
								new wxDataViewColumn (
										"",
										new wxDataViewToggleRenderer("bool",
											wxDATAVIEW_CELL_ACTIVATABLE,
											wxALIGN_CENTER),
										0, 30, wxALIGN_CENTER,
										0 ));




						SpecOptions_ctrl->AppendColumn(
										new wxDataViewColumn (	// Always'%'
										"",
										new wxDataViewTextRenderer("string"),
										1,
//										wxCOL_WIDTH_DEFAULT,
										GetTextExtent("i%i").GetWidth(),
										wxALIGN_RIGHT,
										0 ));


						SpecOptions_ctrl->AppendColumn(	// Enter specifier
										new wxDataViewColumn (
										"",
//										new wxDataViewTextRenderer("string", wxDATAVIEW_CELL_EDITABLE),
										new SpecifierRenderer(wxDATAVIEW_CELL_EDITABLE),
										2,
//										wxCOL_WIDTH_DEFAULT,
										GetTextExtent("Opt").GetWidth(),
										wxALIGN_LEFT,
										0 ));

					 	wxArrayString SPtypes;
					 	for (auto& spt : SPTypes)
					 		SPtypes.Add(spt.second);
						SpecOptions_ctrl->AppendColumn(
										new wxDataViewColumn (	// Select SP_TYPE
										"SP-Type",
										new wxDataViewChoiceRenderer (SPtypes),
										3,
//										wxCOL_WIDTH_DEFAULT,
										GetTextExtent("Instrumentname").GetWidth() + 10,
//										70,
										wxALIGN_LEFT,
										wxDATAVIEW_COL_RESIZABLE ));

						SpecOptions_ctrl->AppendColumn( // button to configure Options
										new wxDataViewColumn (
										"O",
										new O_ButtonRenderer(
												wxDATAVIEW_CELL_ACTIVATABLE
//												wxDATAVIEW_CELL_EDITABLE
										),
										4, GetTextExtent("Opt").GetWidth(),
//										wxALIGN_CENTER,
										wxALIGN_BOTTOM,
										0 ));


					 	wxArrayString Gigtypes;
					 	for (auto& gt : GIG_DimName)
					 		Gigtypes.Add(gt.second);
						SpecOptions_ctrl->AppendColumn(	// if SP_DIMENSION: Select gig::dimension_t
								new wxDataViewColumn (
										"Dimension",
										new wxDataViewChoiceRenderer(Gigtypes),
										5, wxCOL_WIDTH_DEFAULT, wxALIGN_LEFT,
										wxDATAVIEW_COL_RESIZABLE ));
					 	wxArrayString SOvalue;
					 	for (auto& sov : SO_Value)
					 		SOvalue.Add(sov.second);
						SpecOptions_ctrl->AppendColumn(	// Either use Midi-values or T-matrix
										new wxDataViewColumn (
										"Values",
										new wxDataViewChoiceRenderer(SOvalue),
										6, wxCOL_WIDTH_DEFAULT, wxALIGN_LEFT,
										wxDATAVIEW_COL_RESIZABLE ));
						SpecOptions_ctrl->AppendColumn( // button to configure T-matrix
										new wxDataViewColumn (
										"T",
										new T_ButtonRenderer(
												wxDATAVIEW_CELL_ACTIVATABLE
//												wxDATAVIEW_CELL_EDITABLE
										),
										7, GetTextExtent("Opt").GetWidth(), wxALIGN_CENTER,
										0 ));
						SpecOptions_ctrl->AppendColumn(
								new wxDataViewColumn (	// preview
										"Preview",
										new wxDataViewTextRenderer("string"),
										8, wxCOL_WIDTH_DEFAULT, wxALIGN_LEFT,
										wxDATAVIEW_COL_RESIZABLE ));

						SpecOptions->Reset (SpecOptions->Rows());

						SpecOptions_ctrl->SetMinSize(wxSize(200,100));

						class ListBoxAdaptor : public wxAddRemoveAdaptor
						{
						public:
						    explicit ListBoxAdaptor(wxDataViewCtrl* SpecOptions_ctrl) : SP_c(SpecOptions_ctrl) { }
						    virtual wxWindow* GetItemsCtrl() const { return SP_c; }
						    virtual bool CanAdd() const { return true; }
						    virtual bool CanRemove() const { return SP_c->GetSelection() != NULL;}//wxNOT_FOUND; }
						    virtual void OnAdd() {

								t_SO->Add(
										true,
										"v",
										"Dimension",
										"Velocity",
										SO_Value[0],
										"blablabla");
//								SpecOptions_model* t_SO = (SpecOptions_model*) SP_c->GetModel();
//								t_SO->SpecOptions_nodes.push_back(SON);
								t_SO->Reset (t_SO->Rows());
						    }
						    virtual void OnRemove() {
						    	t_SO->DeleteItem(SP_c->GetSelection());
						    	t_SO->Reset (t_SO->Rows());}
						private:
						    wxDataViewCtrl* SP_c;
						    SpecOptions_model* t_SO = (SpecOptions_model*) SP_c->GetModel();
						};

						AddRemove->SetAdaptor(new ListBoxAdaptor(SpecOptions_ctrl));
						AddRemove->SetSize(AddRemove->GetBestSize());
					SpecOptions_vsizer->Add(AddRemove,1 , wxGROW |	wxALL, 5 );
			Top_sizer->Add(SpecOptions_vsizer, 3, wxGROW |	wxALL,	5 );




		Control_sizer->Add(Top_sizer, 1, wxGROW | wxALL, 0 );

			// GIGS
			Gigs_Result_box = new wxStaticBox(Control_Panel, wxID_ANY, "&Files / Structure",
				wxDefaultPosition, wxDefaultSize );
			wxStaticBoxSizer *Gigs_Result_box_sizer = new wxStaticBoxSizer(Gigs_Result_box, wxHORIZONTAL );

				// Gigs-List
				wxBoxSizer *Gigs_list_sizer = new wxBoxSizer( wxVERTICAL );
					SamplerFile_list = new wxDataViewCtrl( Gigs_Result_box, GIGBOX,
							wxDefaultPosition, wxDefaultSize,
//							wxDV_MULTIPLE
							wxDV_NO_HEADER);

					SamplerDisplay_model = new SamplerDisplay;
					SamplerFile_list->AssociateModel( SamplerDisplay_model.get() );

						wxDataViewColumn *column0 =
							new wxDataViewColumn( "Files / Structure",
									new wxDataViewTextRenderer( "string", wxDATAVIEW_CELL_EDITABLE ),
									0, wxCOL_WIDTH_DEFAULT, wxALIGN_LEFT,
//									wxDATAVIEW_COL_RESIZABLE,
									0);
						SamplerFile_list->AppendColumn( column0 );

						SamplerFile_list->AppendTextColumn("Items",
								1);

				Gigs_list_sizer->Add(SamplerFile_list, 1, wxGROW |	wxTOP | wxLEFT | wxRIGHT, 5 );

					// Gigs_list_info
					gigs_info = new wxStaticText(Gigs_Result_box, wxID_ANY,
						Gigs_list_info,
						wxDefaultPosition, wxDefaultSize,
						wxALIGN_RIGHT |	wxST_NO_AUTORESIZE);
				Gigs_list_sizer->Add(0, 1, 0 );
				Gigs_list_sizer->Add(gigs_info,	0, wxGROW | wxRIGHT | wxBOTTOM, 5 );
			Gigs_Result_box_sizer->Add(Gigs_list_sizer,	1, wxGROW | wxALL, 5 );

		
				// SamplerInfo
				SamplerInfo_sizer = new wxBoxSizer( wxVERTICAL);
					SamplerInfo = new wxPanel ( Gigs_Result_box, wxID_ANY);
						Ctrl_Panel_sizer = new wxBoxSizer( wxHORIZONTAL);
						Ctrl_Panel = new wxPanel ( SamplerInfo, wxID_ANY);
							wxTextCtrl* temptext = new wxTextCtrl (
								SamplerInfo,
								SAMPLERINFO,
								"Result-info",
								wxDefaultPosition,
								wxDefaultSize,
								wxTE_MULTILINE | wxTE_READONLY | wxHSCROLL);
						Ctrl_Panel_sizer->Add(temptext, 1, wxRIGHT | wxBOTTOM | wxEXPAND, 0 );
						Ctrl_Panel->SetSizerAndFit(Ctrl_Panel_sizer);

					SamplerInfo_sizer->Add( Ctrl_Panel, 1, wxRIGHT | wxBOTTOM | wxEXPAND, 0 );
					SamplerInfo->SetSizerAndFit(SamplerInfo_sizer);
			Gigs_Result_box_sizer->Add( SamplerInfo, 1,wxGROW | wxALL, 5 );
		Control_sizer->Add(Gigs_Result_box_sizer, 2, wxGROW | wxALL, 5 );

			// OK_Exit_Buttons plus overwrite checkbox
			wxBoxSizer *Create_box_sizer = new wxBoxSizer( wxHORIZONTAL );
			
				wxButton* Exit = new wxButton( Control_Panel, wxID_CANCEL, "Exit");
				Exit->SetBitmap(wxArtProvider::GetBitmap(wxART_QUIT, wxART_BUTTON));
			Create_box_sizer->Add(Exit, 0, wxALL | wxALIGN_CENTER |	wxALIGN_LEFT, 5 );
			
			Create_box_sizer->Add(20,0,0);
			
				overwrite_gigs = new wxCheckBox(Control_Panel, OVERWRITEGIGS, "Overwrite gigs",
					wxDefaultPosition, wxDefaultSize, 
					wxCHK_2STATE | wxALIGN_RIGHT);
				overwrite_gigs->SetValue(options.overwrite_gigs);
			Create_box_sizer->Add(overwrite_gigs, 0, wxALL | wxALIGN_CENTER | wxALIGN_LEFT, 5 );

				wxButton* Create = new wxButton( Control_Panel, wxID_OK, "Create Gigs");
				Create->SetBitmap(wxArtProvider::GetBitmap(wxART_TICK_MARK, wxART_BUTTON));
			Create_box_sizer->Add(Create, 0, wxALL | wxALIGN_CENTER | wxALIGN_LEFT,	5 );
			
				gigs_path = new wxStaticText(Control_Panel, wxID_ANY,
						"",
						wxDefaultPosition, wxDefaultSize,
						wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | 
						wxST_NO_AUTORESIZE | 
						wxST_ELLIPSIZE_START);
			Create_box_sizer->Add(gigs_path, 1, wxRIGHT | wxLEFT | 
					wxALIGN_CENTER_VERTICAL | wxFIXED_MINSIZE, 5 );
				gigs_path->SetLabel(options.gigs_path);

		Control_sizer->Add(Create_box_sizer, 0, wxGROW | wxALL, 5 );
		Control_Panel->SetSizerAndFit(Control_sizer);	

	mainframe->Add(Control_Panel, 8, wxGROW | wxALL, 5 );
	this->SetSizer( mainframe );
	SetClientSize(this->GetBestSize());
}

void MainFrame::listgigs(){
//	gig_list_model->DeleteAll();
//
//	if (gc->gigs.begin() != gc->gigs.end()) {
//		for (auto gig_it = gc->gigs.begin(); gig_it != gc->gigs.end(); ++gig_it) {
//			if ((&*gig_it == gc->unused_gig) and
//					(gc->unused_gig->Instruments.front().samples.size() == 0))
//					continue; // disregard empty unused gig
//			gig_model_node* m_gig = new gig_model_node( NULL,
//					gig_it->name, // title
//					gig_it->Instruments.size(), // Nr Instruments
//					gig_it->nr_samples(), // Nr Samples
//					std::to_string(gig_it->vel_range_low) + "-" + std::to_string(gig_it->vel_range_high), // Velocity-Range
//					(gig_it->note_range_low < 0 || gig_it->note_range_high < 0) ? "" :
//					std::to_string(gig_it->note_range_low) +	"-" + std::to_string(gig_it->note_range_high), // Note-Range
//					gig_it->id, // id
//					((options.expand_all_ranges == true) and
//							(gig_it->id != unused))
//						? wxCHK_CHECKED : wxCHK_UNCHECKED,
//					&*gig_it, // pointer to gig
//					true ); // is container
////			if (gig_it->id != unused)
////				nr_instr += gig_it->Instruments.size();
//			int i = 0;
//			for (auto instr_it = gig_it->Instruments.begin(); instr_it != gig_it->Instruments.end(); ++instr_it) {
//				i++;
//				gig_model_node* m_instr = new gig_model_node( m_gig,
//						instr_it->name, //Title
//						i, // Nr Instruments
//						instr_it->nr_samples(), // Nr Samples
//						std::to_string(instr_it->vel_range_low) + "-" + std::to_string(instr_it->vel_range_high), // Velocity Range
//						(instr_it->note_range_low < 0 || instr_it->note_range_high < 0) ? "" :
//						std::to_string(instr_it->note_range_low) + "-" + std::to_string(instr_it->note_range_high), // Note Range
//						instr_it->id, // id
//						((options.expand_all_ranges == true) and
//								(gig_it->id != unused))
//							? wxCHK_CHECKED : wxCHK_UNCHECKED, //expand range
//						&*instr_it, // pointer to gig
//						false );	// is container
//				m_gig->Append( m_instr );
//			}
//			gig_list_model->m_gigs.push_back(m_gig);
//		}
//		gig_list_model->Cleared();
//	}
//
//	Gigs_list_info = std::to_string(gc->nr_gigs()) + " gigs with ";
//	Gigs_list_info.append(std::to_string(gc->nr_instruments()) + " instruments containing ");
//	Gigs_list_info.append(std::to_string(gc->nr_samples()) + " samples (");
//	Gigs_list_info.append(std::to_string(gc->nr_files()) + " files).");
//	gigs_info->SetLabel(Gigs_list_info);

}

void MainFrame::List_Samples(std::string gig_id, std::string instr_id) {
//
//	Samples->DeleteAllItems();
//	int nr_files = 0;
//	for (auto gig_it = gc->gigs.begin(); gig_it != gc->gigs.end(); ++gig_it)
//		if (gig_it->id == gig_id)
//			for (auto instr_it = gig_it->Instruments.begin(); instr_it != gig_it->Instruments.end(); ++instr_it)
//				if ((instr_id == ALLINSTRUMENTS) or (instr_it->id == instr_id))
//					for (auto& sr_it : instr_it->samples) {
////						std::cout << "ListSamples01" << std::endl;
//							wxVector<wxVariant> data;
//
//							// Sample-name
//							data.push_back(sr_it->name());
////							std::cout << "ListSamples0100" << std::endl;
////							// Velocity
//							data.push_back(std::to_string(sr_it->get_velocity()));
////							std::cout << "ListSamples010" << std::endl;
////							// Note_Nr
//							data.push_back(sr_it->val_str (SP_NOTE));//sr_it.spv[sr_it.get_note()]);
//
////							// Range
//							std::string range = (sr_it->note_range_low < 0 || sr_it->note_range_high < 0) ? "" :
//									std::to_string(sr_it->note_range_low) + "-" + std::to_string(sr_it->note_range_high);
//							data.push_back(range);
////							std::cout << "ListSamples011" << std::endl;
//							//							// NoteName
//							data.push_back(sr_it->val_str (SP_NOTENAME));
//
//							// Filename
//							data.push_back( sr_it->val_str (SP_FILENAME) );
//
////							//Pattern-Info
//							data.push_back(sr_it->info);
////							std::cout << "ListSamples012" << std::endl;
////							//Sample-Info
//							data.push_back("SampleInfo");
//
//							Samples->AppendItem( data );
//
//							nr_files += sr_it->nr_files();
////							std::cout << "ListSamples02" << std::endl;
//						}
////	std::cout << "ListSamples1" << std::endl;
//	std::string sampleinfo = std::to_string(Samples->GetItemCount()) + " Samples with ";
//	sampleinfo.append(std::to_string(nr_files));
//	sampleinfo.append(" files");
//	Sample_info->SetLabel(sampleinfo);
////	std::cout << "ListSamples2" << std::endl;
}

void MainFrame::List_Results() {
//	Samples->DeleteAllItems();
//	for (auto fa_it = gc->fa_list.begin(); fa_it != gc->fa_list.end(); ++fa_it) {
//		if (fa_it->id == gigs_list_id) {
//			wxVector<wxVariant> data;
//			data.push_back( wxVariant(fa_it->name) );
//			data.push_back( wxVariant(fa_it->name2) );
//			data.push_back( wxVariant(fa_it->velocity) );
//			data.push_back( wxVariant(fa_it->note_nr) );
//			data.push_back( wxVariant(fa_it->note_name) );
//			data.push_back( wxVariant(fa_it->to_stereo) );
//			data.push_back( wxVariant(fa_it->filename) );
//			Samples->AppendItem( data );
//		}
//	}
//	Sample_info->SetLabel(std::to_string(Samples->GetItemCount()) + " Samples");
}

void MainFrame::OnSelectedGig(wxDataViewEvent& event){
	wxDataViewItem  selected_SamplerFile = SamplerFile_list->GetSelection();
	if (!selected_SamplerFile.IsOk() or SC == NULL)
		return;
	Ctrl_Panel_sizer->Clear(true);
	SamplerDisplay_node *node = (SamplerDisplay_node*) selected_SamplerFile.GetID();
		Info_Panel = SC->Sampler->NodeCtrl(SamplerInfo, node->m_DataLink);
	Ctrl_Panel_sizer->Add(Info_Panel, 1, wxRIGHT | wxBOTTOM | wxEXPAND, 0 );
//	Ctrl_Panel->SetSizerAndFit(Ctrl_Panel_sizer);
	SamplerInfo_sizer->Layout(); // force re-layout of sizer's children
}

void MainFrame::OnSDNodeCommand(wxCommandEvent& event) {
	std::cout << "OnSDNodeCommand" << std::endl;
	if (SC == NULL)
		return;
	wxDataViewItem  item = SamplerFile_list->GetSelection();
//	SamplerDisplay_node *node = (SamplerDisplay_node*) selected_SamplerFile.GetID();
	wxDataViewItemArray changed_items;
//	SamplerDisplay_model->DeleteAll();
	SC->Sampler->SDNodeCommand (event.GetId(), item, &changed_items);
//	SamplerDisplay_model->Cleared();

//	wxDataViewItemArray kids;
//	std::cout << "Kids:" << (int)
//			SamplerDisplay_model.get()->GetChildren(selected_SamplerFile, kids)
//			<< std::endl;
//	for (auto& kid : kids) {
//		SC->Sampler->UpdateSamplerDisplayNode((SamplerDisplay_node*) kid.GetID());
//	}
	std::cout << "OnCDNodeCommand:ChangedItems:" << (int) changed_items.size() << std::endl;
	SamplerDisplay_model->ItemsChanged(changed_items);
}

void MainFrame::InitSP(wxObjectDataPtr<SpecOptions_model> SO) {
	unsigned int rows = SO->Rows();
	sample_prop* sp {};
	for (unsigned int row = 0; row < rows; row++) {
		std::list<std::pair<sp_text, sp_text> > t_matrix{};
		if (SO->values[row] == SO_Value[0])
			t_matrix = {};
		else {
			for (auto& tm : SO->tmatrix[row]) {
				if (tm.active) {
					std::pair<sp_text, sp_text> tp;
					tp.first = tm.find;
					tp.second = tm.replace;
					t_matrix.push_back(tp);
				}
			}
		}
//		std::cout << "InitSP0" << std::endl;
		if (create_sample_prop (
				SO->sp_type[row].utf8_string(),
				SPtypeByName(SO->sp_type[row].utf8_string()),
				gDimByName(SO->dim_type[row].utf8_string()),
				t_matrix,
				&sp))
			options.Specifiers[SO->specstring[row].utf8_string()] = sp;
		else std::cout << "Init " << SO->sp_type[row] << " failed";
	}
//	std::cout << "InitSP1" << std::endl;
}

void MainFrame::On_Scan(wxCommandEvent& WXUNUSED(event)) {

//	wxCommandEvent event(RESET_EVENT, RESULT_RESET);
//	GetEventHandler()->ProcessEvent(event);
//	return;



	int line = 0;
	SamplerDisplay_model->DeleteAll();
	std::list<std::string> SpecstringList;
//	std::cout << "OnSacan00" << std::endl;
	while (specinput->GetLineLength(line) != -1) {
		std::string specstr = specinput->GetLineText(line).utf8_string();
		if (specstr != "") {
			SpecstringList.push_back(specstr);
		}
		line++;
	}
	if (SpecstringList.size() <= 0)
		return;
//	std::cout << "OnSacan1" << std::endl;
	WriteSpecOptions();
	InitSP(SpecOptions);
	SC = new SamplerCreator(
			SAMPLER_TYPE_GIG,
			InputFiles,
			SpecstringList,
			&*SamplerDisplay_model);//,
//	std::cout << "OnSacan10" << std::endl;
	SamplerResult* Sresult{};// = new SamplerResult;
	std::string total = SC->GetResult(&Sresult);




//	std::cout << "OnSacan2" << std::endl;
//	std::cout << "ScanResultSize:" << std::flush << Sresult->unused_Samples.size() << ":"
//			<< std::flush << static_cast<std::list<SamplerFile>*>(Sresult->samplerFiles)->size()
//			<< std::endl;
	SamplerDisplay_model->Cleared();
//	std::cout << "OnSacan3" << std::endl;
	gigs_info->SetLabel(total);
}

void MainFrame::OnSpecInput(wxCommandEvent& WXUNUSED(event)) {
	Preview();

//	SamplerDisplay_model->DeleteAll();
//
//	// create SpecstringList
//	int line = 0;
//	std::list<std::string> SpecstringList;
//	std::cout << "OnSpecInput0" << std::endl;
//	while (specinput->GetLineLength(line) != -1) {
//		std::string specstr = specinput->GetLineText(line).utf8_string();
//		if (specstr != "") {
//			SpecstringList.push_back(specstr);
//		}
//		line++;
//	}
//	if (SpecstringList.size() <= 0)
//		return;
//
//	// Get filename
//	int row = init_file_list->GetSelectedRow();
//	if (row == wxNOT_FOUND)
//		return;
//	std::string file = init_file_list->GetTextValue(row, 1).utf8_string();
//
//	// Get SpecOptions
//	InitSP(SpecOptions);
//
//	// Create SamplerCreator
//	SC = new SamplerCreator(
//			SAMPLER_TYPE_GIG,
//			InputFiles,
//			SpecstringList,
//			&*SamplerDisplay_model);
//	std::cout << "OnSpecInput:file:" << file << ":::" << std::endl;
//	std::map<std::string, std::string> preview = SC->GetPreview(SpecstringList, file);
//
//	// Evaluate selected file
//	bool found = false;
//	for (size_t row = 0; row < SpecOptions->Rows(); row++) {
//		found = false;
//		if (preview.size() > 0) {
//			for (auto& prev : preview) {
//				if (prev.first == SpecOptions->specstring[row]) {
//					SpecOptions->preview[row] = prev.second;
//					found = true;
//					break;
//				}
//			}
//			if (!found)
//				SpecOptions->preview[row] = "";
//		}
//		else
//			SpecOptions->preview[row] = "";
//	}
//	SpecOptions->Reset (SpecOptions->Rows());
}

void MainFrame::Recursive(wxCommandEvent& event) {
	options.recursive = event.GetInt();
	getfiles();
//	listfiles();
//	Samples->DeleteAllItems();
	SamplerDisplay_model->DeleteAll();
	SamplerDisplay_model->Cleared();
}


void MainFrame::Hide_Paths(wxCommandEvent& event) {
	options.hide_path = event.GetInt();
	getfiles();
//	listfiles();
//	Samples->DeleteAllItems();
	SamplerDisplay_model->DeleteAll();
	SamplerDisplay_model->Cleared();
}

void MainFrame::OnOverwrite_Gigs(wxCommandEvent& event) {
//	gc->overwrite = event.GetInt();
}

//void CreateGigs(gigcontainer gc) {
//	int nr_gigs = 0;
//	createprogress = 0;
//	createdone = false;
//	for (auto gig_it = gc.gigs.begin(); gig_it != gc.gigs.end(); ++gig_it) {
//		if (stop_create_thread) break;
//		if (gig_it->id != unused)
//			nr_gigs += Creategig(&*gig_it, options.overwrite_gigs);
//	}
//	usleep(100);
//	createlog.append("\n" + std::to_string(nr_gigs) + " gigs saved.\n");
//	createlog.append("\n--- Finished creating gigs ---");
//
//	createdone = true;
//	wxWakeUpIdle ();
//	sleep(1);
//	return;
//}

static void CreateThreadWrapper (
	SamplerCreator* SC,
	float* createprogress,
	std::string* createlog)
{
	SC->Sampler->SaveSamplerFiles(createprogress, createlog);
}

void MainFrame::OnOK(wxCommandEvent& WXUNUSED(event)) {
	Save_SamplerFiles_progress* SaveSamplerFiles_Frame = new Save_SamplerFiles_progress;
	if (SC == NULL)
	  return;
	stop_create_thread = false;
	std::thread SC_thread (CreateThreadWrapper,SC, &createprogress, &createlog);
//
//	CreateThreadWrapper(SC);
//	std::cout << "OnOK:CreateSamplerFiles" << std::endl;
//	SC->Sampler->CreateSamplerFiles(&createprogress, &createlog);
//	std::thread SC_thread = SC->CreateSamplerFiles();

	SaveSamplerFiles_Frame->ShowModal();
	SC_thread.join();
//	CreateGigsFrame->Destroy();
//	std::cout << "OnOK: Done" << std::endl;
}

void MainFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
//	delete gc;
//	delete gc_prev;
    Close(true);
}

void MainFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxMessageBox(wxString::Format
                 (
                    "Welcome to wav2gigCreator!\n"
                    "\n"
                    "OS: %s.",
                                        wxGetOsDescription()
                 ),
                 "About wav2gigCreator",
                 wxOK | wxICON_INFORMATION,
                 this);
}

void MainFrame::On_Select_File(wxDataViewEvent& event){
	
//	// Get SpecInput
//	int line = 0;
//	gc_prev->specstrings.clear();
//	while (specinput->GetLineLength(line) != -1) {
//		std::string specstr = specinput->GetLineText(line).utf8_string();
//		if (specstr != "") {
//			gc_prev->specstrings.push_back(specstr);
//		}
//		line++;
//	}
//	gc_prev->init_files.clear();
//
//	unsigned int row = init_file_list->GetSelectedRow();
//	gc_prev->init_files[init_file_list->GetTextValue(row, 1).utf8_string()] = init_file_list->GetTextValue(row, 0).utf8_string();
//	gc_prev->Scan();
//	std::string pattern = "";
//	std::string sample = "";
//	if (gc_prev->gigs.size() == 1) {
//		if (gc_prev->gigs.front().Instruments.front().Samples.size() > 0) {
//			pattern = gc_prev->gigs.front().Instruments.front().Samples.begin()->second.begin()->second.Pattern_Info;
//			sample = gc_prev->gigs.front().Instruments.front().Samples.begin()->second.begin()->second.Info();
//		}
//	} else {
//		if (gc_prev->gigs.back().Instruments.front().Samples.size() > 0) {
//			pattern = gc_prev->gigs.back().Instruments.front().Samples.begin()->second.begin()->second.Pattern_Info;
//			sample = gc_prev->gigs.back().Instruments.front().Samples.begin()->second.begin()->second.Info();
//		}
//	}
//	Preview(pattern, sample);
	Preview();
}

void MainFrame::Preview() {
//	SamplerDisplay_model->DeleteAll();

	// create SpecstringList
	int line = 0;
	std::list<std::string> SpecstringList;
//	std::cout << "OnSpecInput0" << std::endl;
	while (specinput->GetLineLength(line) != -1) {
		std::string specstr = specinput->GetLineText(line).utf8_string();
		if (specstr != "") {
			SpecstringList.push_back(specstr);
		}
		line++;
	}
	if (SpecstringList.size() <= 0)
		return;

	// Get filename
	int row = init_file_list->GetSelectedRow();
	if (row == wxNOT_FOUND)
		return;
	std::string file = init_file_list->GetTextValue(row, 1).utf8_string();

	// Get SpecOptions
	InitSP(SpecOptions);

	// Create SamplerCreator
	SamplerCreator* temp_SC = new SamplerCreator(
			SAMPLER_TYPE_GIG,
			InputFiles,
			SpecstringList,
			&*SamplerDisplay_model);
//	std::cout << "OnSpecInput:file:" << file << ":::" << std::endl;
	std::map<std::string, std::string> preview = temp_SC->GetPreview(SpecstringList, file);

	// Evaluate selected file
	bool found = false;
	for (size_t row = 0; row < SpecOptions->Rows(); row++) {
		found = false;
		if (preview.size() > 0) {
			for (auto& prev : preview) {
				if (prev.first == SpecOptions->specstring[row]) {
					SpecOptions->preview[row] = prev.second;
					found = true;
					break;
				}
			}
			if (!found)
				SpecOptions->preview[row] = "";
		}
		else
			SpecOptions->preview[row] = "";
	}
	SpecOptions->Reset (SpecOptions->Rows());

	delete temp_SC;
}

void MainFrame::StartEditGig(wxDataViewEvent& event) {
	wxDataViewItem item = event.GetItem();
	SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
	if (SC != NULL) {
		if (!SC->Sampler->IsEditable(node->m_DataLink))
			event.Veto();
	}
}

void MainFrame::StopEditGig(wxDataViewEvent& event) {
	wxDataViewItem item = event.GetItem();
	SamplerDisplay_node *node = (SamplerDisplay_node*) item.GetID();
	if (SC != NULL) {
		SC->Sampler->SetNodeName(node->m_DataLink, event.GetValue().GetString());
		SamplerInfo->Refresh();
	}
}

void MainFrame::OnDirChange(wxFileDirPickerEvent& event) {
	wxString path = event.GetPath();
	wxFileName fn(path);
	if (!fn.DirExists()) {
//        wxLogError("Invalid path!");
        InputFiles.clear();
        listfiles();
//        init_file_list->DeleteAllItems();
        return;
	}

    options.dir_path = path;
	if (options.gigs_use_wav_directory)
		options.gigs_path = options.dir_path + sep;
    getfiles();
//    listfiles();
//	gc_prev->init_files.clear();
//	if (options.direct_scan) {
//		int line = 0;
//		gc_prev->specstrings.clear();
//		while (specinput->GetLineLength(line) != -1) {
//			std::string specstr = specinput->GetLineText(line).utf8_string();
//			if (specstr != "") {
//				gc_prev->specstrings.push_back(specstr);
//			}
//			line++;
//		}
//		gc->specstrings = gc_prev->specstrings;
//		gc->Scan();
//		listgigs();
//	}
	gigs_path->SetLabel(options.gigs_path);
}

void MainFrame::OnOptions(wxCommandEvent& WXUNUSED(event)) {
	Options *OptionsFrame = new Options;
	OptionsFrame->ShowModal();
	OptionsFrame->Destroy();
	Apply_Pattern->Enable(!options.direct_scan);
	gigs_path->SetLabel(options.gigs_path);
}

void MainFrame::OnSampleSelect(wxDataViewEvent& event) {
//	int row = Samples->GetSelectedRow();
//	std::string pattern = Samples->GetTextValue(row,6).utf8_string();
//	std::string sample = Samples->GetTextValue(row,7).utf8_string();
//	Preview(pattern, sample);
}

void MainFrame::GetSpecOptions() {
    std::ifstream in;
    in.open (so_filename, std::ifstream::in);
//    std::cout << "GetSO: Start" << std::endl;
    if ( (in.rdstate() & std::ifstream::failbit ) != 0 ) {
    	std::cout << "no SpecOptionsconfig-file, creating one:" << std::endl;
    	WriteSpecOptions();
    }
    else {
    	char line[256];
//    	char value[256];
    	unsigned int row = 0;
    	std::string line_str;
    	in.getline(line, 256);
    	while ( (in.rdstate() & std::ifstream::eofbit ) == 0 ) {
    		if (strcmp( line, "<SpecOption>" ) != 0) {
//    			std::cout << "GetSO: SO not found" << std::endl;
    			continue;
    		}
    		SpecOptions->Add(false, "1", "2", "3", "4", "");
    		in.getline(line, 256);
    		line_str = line;
    		size_t pos = line_str.find(":", 0);
    		while (line_str != "<\\SpecOption>" and (in.rdstate() & std::ifstream::eofbit ) == 0 ) {
//    				(pos != line_str.npos) {
    			std::string option = line_str.substr(0, pos);
    			std::string value = line_str.substr(pos+1, line_str.npos);
//    			std::cout << "GetSO:" << (int) row << "_" << option << "_" << value << std::endl;
				if (option == "Active" ) SpecOptions->active[row] = atoi(value.c_str());
				else if ( option == "SpecString" ) SpecOptions->specstring[row] = value;
				else if ( option == "SP_Type" ) SpecOptions->sp_type[row] = value;
				else if ( option == "DimType" ) SpecOptions->dim_type[row] = value;
				else if ( option == "Values" ) SpecOptions->values[row] = value;
				else if ( option == "Tmatrix" ) {
					SpecOptions_model::tmatrix_struct TM{};
					size_t f_pos = value.find("::", 0);
					TM.active = atoi(value.substr(0, f_pos).c_str());
					unsigned int r_pos = value.find(":::", f_pos + 2);
					TM.find = value.substr(f_pos + 2, r_pos - 3);
					TM.replace = value.substr(r_pos + 3, value.npos);
					SpecOptions->tmatrix[row].push_back(TM);
				}
    			in.getline(line, 256);
    			line_str = line;
    			pos = line_str.find(":", 0);
    		}
    		row++;
    		in.getline(line, 256);
    	} // EOF
    }
    in.close();
    SpecOptions->Reset (SpecOptions->Rows());
}

void MainFrame::WriteSpecOptions() {
	std::string writestring = "";
	unsigned int rows = SpecOptions->Rows();
	for (unsigned int row = 0; row < rows; row++) {
		writestring.append("<SpecOption>\n");
		writestring.append("Active:" + std::to_string(SpecOptions->active[row]) + "\n");
		writestring.append("SpecString:" + SpecOptions->specstring[row] + "\n");
		writestring.append("SP_Type:" + SpecOptions->sp_type[row] + "\n");
		writestring.append("DimType:" + SpecOptions->dim_type[row] + "\n");
		writestring.append("Values:" + SpecOptions->values[row] + "\n");
		for (auto& tm : SpecOptions->tmatrix[row]) {
			writestring.append("Tmatrix:" + std::to_string(tm.active) + "::");
			writestring.append(tm.find + ":::" + tm.replace + "\n");
		}
		writestring.append("<\\SpecOption>\n");
	}



    std::ofstream out(so_filename);
    out << writestring;
    out.close();
}

void GetOptions() {
    std::ifstream in;
    in.open (config_filename, std::ifstream::in);
    if (options.dir_path == "")
    	options.dir_path = wxGetHomeDir().utf8_string();
    if ( (in.rdstate() & std::ifstream::failbit ) != 0 ) {
    	std::cout << "no config-file, creating one:" << std::endl;
    	WriteOptions();
    }
    else {
    	char option[256];
    	char value[256];
    	while ( (in.rdstate() & std::ifstream::eofbit ) == 0 ) {
    		in.getline(option, 256, ':');
    		if ((in.rdstate() & std::ifstream::failbit ) == 0) {
				in.getline(value, 256);
				// general
				if (strcmp ( option, "dir_path" ) == 0) {
					if (init_dir_path == "")
						{options.dir_path = value;}
					else
						options.dir_path = init_dir_path;
				}
				else if (strcmp( option, "recursive" ) == 0) {options.recursive = atoi(value);}
				else if (strcmp( option, "hide_path" ) == 0) {options.hide_path = atoi(value);}
				else if (strcmp( option, "gigs_path" ) == 0) {options.gigs_path = value;}
				else if (strcmp( option, "gigs_use_wav_directory" ) == 0) options.gigs_use_wav_directory = atoi(value);
				else if (strcmp( option, "overwrite_gigs" ) == 0) options.overwrite_gigs = atoi(value);
				else if (strcmp( option, "expand_all_ranges" ) == 0) options.expand_all_ranges = atoi(value);
				else if (strcmp( option, "direct_scan" ) == 0) options.direct_scan = atoi(value);

				else if (strcmp( option, "spec_line" ) == 0) {
					options.specinput.append(value);
					options.specinput.append("\n");
				}
				// Notes
				else if (strcmp( option, "s_sharp" ) == 0) options.s_sharp = value;
				else if (strcmp( option, "s_flat" ) == 0) options.s_flat = value;
				else if (strcmp( option, "p_sharp" ) == 0) options.p_sharp = value;
				else if (strcmp( option, "p_flat" ) == 0) options.p_flat = value;
				else if (strcmp( option, "overwride_wavInfo" ) == 0) options.overwride_wavInfo = atoi(value);
				else if (strcmp( option, "always_create_notenames" ) == 0) options.always_create_notenames = atoi(value);
				else if (strcmp( option, "transpose" ) == 0) options.transpose = atoi(value);

				// Velocity
				else if (strcmp( option, "velocity_translate" ) == 0) {
					std::string value_str = value;
					std::size_t id = value_str.find(":::");
					if (id != value_str.npos) {
					std::string find = value_str.substr(0,id);
					short replace = atoi(value_str.substr(id+3,value_str.npos).c_str());
					options.velocity_translate[find] = replace;
					}
				}
    		}
    	}
    	
    	in.close();
    }
}

void WriteOptions() {
	
	std::string writestring = "";
	// general
	writestring.append("dir_path:" + options.dir_path + "\n");
	writestring.append("recursive:" + std::to_string(options.recursive) +"\n");
	writestring.append("hide_path:" + std::to_string(options.hide_path) +"\n");
	writestring.append("gigs_path:" + options.gigs_path + "\n");
	writestring.append("gigs_use_wav_directory:" + std::to_string(options.gigs_use_wav_directory) +"\n");
	writestring.append("overwrite_gigs:" + std::to_string(options.overwrite_gigs) +"\n");
	writestring.append("expand_all_ranges:" + std::to_string(options.expand_all_ranges) +"\n");
	writestring.append("direct_scan:" + std::to_string(options.direct_scan) +"\n");
	// Spec-patterns
	for (int line = 0; line < specinput->GetNumberOfLines(); line++) {
		std::string line_str = specinput->GetLineText(line).utf8_string();
		if (line_str.length() > 0)
			writestring.append("spec_line:" + line_str +"\n");
	}

	// Notes
	writestring.append("s_sharp:" + options.s_sharp +"\n");
	writestring.append("s_flat:" + options.s_flat +"\n");
	writestring.append("p_sharp:" + options.p_sharp +"\n");
	writestring.append("p_flat:" + options.p_flat +"\n");
	writestring.append("overwride_wavInfo:" + std::to_string(options.overwride_wavInfo) +"\n");
	writestring.append("always_create_notenames:" + std::to_string(options.always_create_notenames) +"\n");
	writestring.append("transpose:"+ std::to_string(options.transpose) +"\n");
	// Velocity
	for (auto vel_trans_it : options.velocity_translate) {
		writestring.append("velocity_translate:" + vel_trans_it.first + ":::"
				+ std::to_string(vel_trans_it.second) +"\n");
	}
	std::cout << config_filename << std::endl;
	
    std::ofstream out(config_filename);
    out << writestring;
    out.close();
}

void MainFrame::OnHelp(wxCommandEvent& WXUNUSED(event)) {
	ShowHelp(0);
}


void MainFrame::ShowHelp(int commandId) {
  if (!wxLaunchDefaultBrowser ("help.html", wxBROWSER_NEW_WINDOW)) {
	wxFrame* Help_Frame = new wxFrame(this, wxID_ANY, "wav2gigCreator Help",
			wxDefaultPosition, wxSize(640, 480), wxDEFAULT_FRAME_STYLE);
	wxInitAllImageHandlers 	( 		) 	;
	wxBoxSizer *help_sizer = new wxBoxSizer(wxVERTICAL);
		wxHtmlWindow* help_window = new wxHtmlWindow(Help_Frame, HELP);
		help_window->SetStandardFonts(12);
		help_window->SetRelatedFrame(Help_Frame, _("HTML : %s"));
		help_window->ReadCustomization(wxConfig::Get());
		help_window->LoadFile(wxFileName("help.html"));

    help_sizer->Add(help_window, 1, wxGROW);
    Help_Frame->Show(TRUE);
  }

}

void MainFrame::OnHelpLinkClick(wxHtmlLinkEvent& event) {
	std::string href = event.GetLinkInfo().GetHref().utf8_string();
	if (href.substr(0,5) == "http:") {
		std::string openurl = "xdg-open ";
#ifdef _WIN32
		openurl = "start ";
#endif
#ifdef __APPLE__
		openurl = "open ";
#endif
		openurl.append(href);
		system(openurl.c_str());
	}
	else {
		event.Skip();
	}
}

class wxDirTraverserSimple : public wxDirTraverser {
public:
    wxDirTraverserSimple(wxArrayString& files) : m_files(files) { }
    virtual wxDirTraverseResult OnFile(const wxString& filename) {
        m_files.Add(filename);
        return wxDIR_CONTINUE;
    }
    virtual wxDirTraverseResult OnDir(const wxString& WXUNUSED(dirname)) {
        return wxDIR_CONTINUE;
    }
private:
    wxArrayString& m_files;
};

void MainFrame::getfiles() {
	std::cout << "Getting files..." << std::flush;
	wxDir dir(options.dir_path);
	if ( !dir.IsOpened() )
	{
	    return;
	}
	wxArrayString files;
	wxDirTraverserSimple traverser(files);
	int recursive_flag = wxDIR_FILES;
	if (options.recursive)
		recursive_flag = wxDIR_FILES | wxDIR_DIRS;
	dir.Traverse(traverser, "*.wav", recursive_flag);
	InputFiles.clear();
//	gc->init_files.clear();
//	gc->path = options.dir_path;
	for (auto file_it = files.begin(); file_it != files.end(); ++file_it) {
//		std::cout << "GetFiles:" << *file_it << std::endl;
		InputFiles.insert(file_it->utf8_string());
	}
	std::cout << "done." << std::endl;
	listfiles();
}

void MainFrame::listfiles() {
//	std::cout << "ListFiles" << std::endl;
		init_file_list->DeleteAllItems();
		for (auto& file_it : InputFiles ) {
			wxVector<wxVariant> data;
			data.push_back( wxVariant(cut_path(file_it)));
			data.push_back( wxVariant(file_it));
			init_file_list->AppendItem( data );
		}
		if (init_file_list->GetItemCount() > 0)
			init_file_list->SelectRow(0);
		nr_of_wav_files->SetLabel(std::to_string(init_file_list->GetItemCount()) + " files");
}

void MainFrame::OnSP_TypeSelect(wxCommandEvent& event) {
	std::cout << "Selected" << std::endl;
//	wxEvtHandler * const handler = SP_TypeChoice->PopEventHandler();
//
//	SP_TypeChoice->Hide();
//
//    wxPendingDelete.Append(handler);
//    wxPendingDelete.Append(SP_TypeChoice);

    // Ensure that DestroyEditControl() is not called again for this control.
//    SP_TypeChoice->Release();


//	delete SP_TypeChoice;
}

//bool DirValidator::Validate( wxWindow * parent ){
//    wxLogError("Invalid path!");
//    return false;
////	return path.DirExists();
//    wxASSERT(GetWindow()->IsKindOf(CLASSINFO(wxDirPickerCtrl)));
//
//    wxDirPickerCtrl* cb = (wxDirPickerCtrl*)GetWindow();
//    std::string t_fn = cb->GetPath();
//    if (!t_fn.DirExists())
//    {
//        wxLogError("Invalid path!");
//        return false;
//    }
//    path = t_fn;
//    return true;
//}
//bool DirValidator::TransferToWindow () {
//	wxASSERT(GetWindow()->IsKindOf(CLASSINFO(wxDirPickerCtrl)));
//
//	if (path.DirExists()) {
//		wxDirPickerCtrl* cb = (wxDirPickerCtrl*)GetWindow();
//        if ( !cb )
//            return false;
//
//        cb->SetDirName(path);
//	}
//	return true;
//}
//
//bool DirValidator::TransferFromWindow()
//{
//    wxASSERT(GetWindow()->IsKindOf(CLASSINFO(wxDirPickerCtrl)));
//
//    if (path.DirExists()) {
//    	wxDirPickerCtrl* cb = (wxDirPickerCtrl*)GetWindow();
//        if ( !cb )
//            return false;
//
//        path = cb->GetDirName();
//    }
//
//    return true;
//}

void MainFrame::OnSpecOptionsChanged(wxDataViewEvent& WXUNUSED(event)) {
	if (SC != NULL) {
		SamplerDisplay_model->DeleteAll();
		delete SC;
		SC = NULL;
	}
}

void MainFrame::Reset_Results(wxCommandEvent& WXUNUSED(event)) {
	if (SC != NULL) {
		SamplerDisplay_model->DeleteAll();
		delete SC;
		SC = NULL;
	}
}








