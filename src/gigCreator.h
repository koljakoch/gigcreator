/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/

#ifdef WIN32
# define DIR_SEPARATOR '\\'
#else
# define DIR_SEPARATOR '/'
#endif


#include <wx/cmdline.h>
#include "helper.h"
#include "SamplerDisplay_model.h"

//#include "PrepareGig.h"
#include "SamplerCreator.h"
#include "options.h"
//#include "CreateGig.h"


//std::string use_found_value = "use found value";
//std::string use_t_matrix =

wxDECLARE_EVENT(RESET_EVENT, wxCommandEvent);


class MyApp : public wxApp
{
public:
    void OnInitCmdLine(wxCmdLineParser& parser);
    bool OnCmdLineParsed(wxCmdLineParser& parser);
	
    virtual bool OnInit() wxOVERRIDE;
};

class MainFrame : public wxFrame
{
public:
    MainFrame();
    
    void OnAbout(wxCommandEvent& event);
    void OnQuit(wxCommandEvent& event);
    void OnOK(wxCommandEvent& event);
    void OnSpecInput(wxCommandEvent& event);
    void OnOverwrite_Gigs(wxCommandEvent& event);
    void On_Select_File(wxDataViewEvent& event);
    void listgigs();
    void OnSelectedGig(wxDataViewEvent& event);
    void List_Results();
    void StopEditGig(wxDataViewEvent& event);
    wxStaticText* gigs_info;
//    wxStaticText* Sample_info;
    wxButton* Apply_Pattern;
    wxStaticText* gigs_path;
    wxCheckBox* overwrite_gigs;
    wxPanel* SamplerInfo;
    wxPanel* Info_Panel;
    wxBoxSizer *Ctrl_Panel_sizer;
    wxBoxSizer* SamplerInfo_sizer;
    wxPanel* Ctrl_Panel;
    wxDataViewCtrl* SpecOptions_ctrl;
    wxStaticBox *Gigs_Result_box;
    
    void Hide_Paths(wxCommandEvent& event);
    void Recursive(wxCommandEvent& event);
    void OnAllVelocityInput(wxCommandEvent&);
    void Overwrite_all_Velocity(wxCommandEvent&);
    void OnDirChange(wxFileDirPickerEvent &ev);
    void On_Scan(wxCommandEvent&);
    void OnHelp(wxCommandEvent&);
    void ShowHelp(int commandId);
    void OnOptions(wxCommandEvent&);
    void OnSampleSelect(wxDataViewEvent& event);

    void List_Samples(std::string gig_id, std::string instr_id);
//    wxTextCtrl* Pattern_Info;
//    wxTextCtrl* Sample_Info;
    
    void listfiles();
    void getfiles();
    void InitSP(wxObjectDataPtr<SpecOptions_model> SO);
    void WriteSpecOptions();
    void GetSpecOptions();
    SamplerCreator* SC = NULL;

    void OnSP_TypeSelect(wxCommandEvent& event);

	void OnSDNodeCommand(wxCommandEvent& event);

	void Reset_Results(wxCommandEvent& WXUNUSED(event));

private:
    wxDECLARE_EVENT_TABLE();
    wxDirPickerCtrl* dir_picker;
    wxCheckBox* recursive;
    wxCheckBox* hide_paths;
    wxDataViewListCtrl* init_file_list;
    wxStaticText* nr_of_wav_files;
    
    wxObjectDataPtr<SamplerDisplay> SamplerDisplay_model;
    wxObjectDataPtr<SpecOptions_model> SpecOptions;
    

    void StartEditGig(wxDataViewEvent& event);
    void Preview();
    
    void OnIdle(wxIdleEvent& event);
    
    void OnHelpLinkClick(wxHtmlLinkEvent& event);
    void OnSpecOptionsChanged(wxDataViewEvent& WXUNUSED(event));

};


MainFrame *mainframe;



//class DirValidator : public wxValidator
//{
//public:
//	DirValidator(std::string& t_path) {
//		path = t_path; }
//	bool Validate( wxWindow * parent ) wxOVERRIDE;
//	bool TransferToWindow () wxOVERRIDE;
//	bool TransferFromWindow () wxOVERRIDE;
//	wxObject* Clone () const wxOVERRIDE { return new DirValidator(*this); }
//
//private:
//	std::string* path;
//};



wxTextCtrl* specinput;


static const wxCmdLineEntryDesc cmdLineDesc[] =
{
    { wxCMD_LINE_SWITCH, "h", "help", "show this help text" },
    { wxCMD_LINE_OPTION,  "d", "dir", "input directory", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
    { wxCMD_LINE_NONE }
};




//wxDataViewListCtrl *Samples;
wxDataViewCtrl *SamplerFile_list;
wxGauge* progress_gauge; 
wxTextCtrl* progresslog;
wxDialog* Create_gigs_Dialog;
//std::string* createlog = new std::string{};
//uint8_t* createprogress = new uint8_t{};

std::string createlog = "";
float createprogress = 0;

bool createdone = false;
bool stop_create_thread = false;

std::string temp_item_text;

void GetOptions();
void WriteOptions();

wxChoice* SP_TypeChoice;


enum
{
    Menue_Quit = wxID_EXIT,
    Menue_About = wxID_ABOUT,
	SELECT_FILE,
	SPEC_FIELD,
	PickerPage_Dir,
	GIGBOX,
	FILELIST,
	RECURSIVE,
	HIDE_PATHS,
	OVERWRITE_VELOCITY,
	APPLY_VELOCITY,
	ATTRIBUTES_HELP,
	APPLY_PATTERN,
	SCAN,
	HELP,
	OPTIONS,
	OPTIONSOK,
	OPTIONSCANCEL,
	OVERWRITEGIGS,
	DO_OVERRIDE_VELOCITY,
	CREATE_NOTE_NAMES,
	PREF_PATTERN_TO_WAV_INFO,
	OPTIONSRESTOREDEFAULTS,
	OPTIONSSAVEASDEFAULT,
	EXPAND_RANGES,
	GIGS_USE_FILES_PATH,
	DO_OVERRIDE_NOTE,
	DIR_PATH,
	GIGS_PATH,
	SAMPLERINFO,
	DIRECT_SCAN,
	MOPTIONS,
	PROGRESS_LOG,
	PROGRESS_GAUGE,
	CREATECANCEL,
	CREATEOK,
	VELOCITY_TRANSLATE,
	OPTIONSBOX,
	SO_OK,
	SO_CANCEL,
	SOT_EDIT,
	SP_CHOICE,

	SD_NODE_EXPANDRANGES,
	SD_NODE_CONTRACTRANGES,

	RESULT_RESET

};

char sep = '/';

#ifdef _WIN32
   sep = '\\';
#endif
