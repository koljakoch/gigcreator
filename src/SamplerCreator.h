/********************************************************************************
 * 																				*
 * 		Copyright (C) 2021 	Kolja Koch											*
 * 							<prog@koljakoch.de>									*
 * 																				*
 * 		This file is part of gigCreator											*
 * 																				*
 *     	This program is free software: you can redistribute it and/or modify	*
 *     	it under the terms of the GNU General Public License as published by	*
 *     	the Free Software Foundation, either version 3 of the License, or		*
 *     	(at your option) any later version										*
 *     																			*
 *     	This program is distributed in the hope that it will be useful,			*
 *     	but WITHOUT ANY WARRANTY; without even the implied warranty of			*
 *     	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the			*
 *     	GNU General Public License for more details.							*
 *     																			*
 *     	You should have received a copy of the GNU General Public License		*
 *     	along with this program.  If not, see <https://www.gnu.org/licenses/>.	*
 *     																			*
 ********************************************************************************/


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <string>
#include <set>
#include <list>
#include <regex>
#include <map>
#include <utility>

# include <gig.h>

// only libsndfile is available for Windows, so we use that for writing the sound files
#ifdef WIN32
# define HAVE_SNDFILE 1
#endif // WIN32
// abort compilation here if libsndfile is not available
#if !HAVE_SNDFILE
# error "It seems as if libsndfile is not available!"
# error "(HAVE_SNDFILE is false)"
#endif

#if HAVE_SNDFILE
# ifdef LIBSNDFILE_HEADER_FILE
#  include LIBSNDFILE_HEADER_FILE(sndfile.h)
# else
#  include <sndfile.h>
# endif
#endif // HAVE_SNDFILE

std::string cut_path(const std::string file);



//enum SAMPLER_TYPE {
//	SAMPLER_TYPE_NONE = 0,
//	SAMPLER_TYPE_GIG
//};
//
//
//enum SPTYPE {
//	SP_NONE = 0,
//	SP_FILENAME,
//	SP_SAMPLERFILE,
//	SP_INSTRUMENT,
//	SP_NOTE,
//	SP_NOTENAME,
//	SP_REGION,
//	SP_DIMENSION,
//	SP_STEREO,
//	SP_IGNORE,
//	SP_SAMPLER
//};

typedef std::string FILENAME;
typedef std::string SPECSTRING;
typedef std::string RESULT;

typedef void* sp_value;
typedef std::string sp_text;
typedef int8_t sp_midival;
typedef std::string sp_steroval;
typedef std::string sp_file;
typedef uint8_t sp_dimval;
typedef std::pair<uint8_t, std::string> file_pair;



class sample_prop;
//class sample_prop_sampler;
class sample_prop_midival ;
class sample_prop_file;
class sample_prop_dimension;
class sample_prop_notename;
class sample_prop_stereo;

bool create_sample_prop (sp_text name, SPTYPE type, sample_prop** sp);
bool create_sample_prop (sp_text name, SPTYPE type, sp_text left, sp_text right, sample_prop** sp);
bool create_sample_prop (sp_text name, SPTYPE type, gig::dimension_t dim,
		std::list<std::pair<sp_text, sp_midival> > t_matrix, sample_prop** sp);

struct SamplerResult;

class SamplerCreator {
public:
	class Sampler_class;
	class gig_sampler;

	Sampler_class* Sampler {};
	SAMPLER_TYPE sampler_type = SAMPLER_TYPE_NONE;
	SamplerCreator(
			SAMPLER_TYPE tsampler_type,
			std::set<FILENAME> files,
			std::list<SPECSTRING> specstrings,
			SamplerDisplay* SamplerDisplayModel);//,
//			uint8_t* tprogress,
//			std::string* tlog);



	struct pattern_struct {
		std::string id = "";
		int start = -1;
		int end = -1;
		int length = -1;
		std::string must_be = "";
	};

	static sample_prop_file* file_sp;

	class sample_prop_val;
	typedef std::pair <sample_prop*, std::string> spv_pair;
	typedef std::list<spv_pair> spv_list;


	class Sample;
	std::string GetWavInfo(std::string filename) const;
	Sample* CreateSample (sp_text filename, sp_text specstring);
	std::list<Sample*> Samples {};
//	sp_file* get_filename (Sample* sample) const;
	std::map<std::string, std::string> GetPreview(std::list<std::string> SpecstringList, std::string file);

	std::set<FILENAME> files;
	std::set<FILENAME> process_files;
	void init_process_files() {
		process_files.clear();
		for (auto file : files) {
//			std::cout << "InitProcessFiles:" << file << std::endl;
			process_files.insert(file);
		}
		for (auto ifile : process_files) {
//			std::cout << "ProcessFiles:" << std::flush << ifile << std::endl;
		}
	}
	std::list<SPECSTRING> specstrings;

	SamplerResult* samplerresult{};
	SamplerDisplay* SamplerDisplayModel{};

//	std::thread CreateSamplerFiles();
//	uint8_t* progress;// = NULL;
//	std::string* log;//{};

	bool GetSamples();
	RESULT GetResult(SamplerResult** result);

};

struct SamplerResult {
	SAMPLER_TYPE type = SAMPLER_TYPE_NONE;
	std::list<SamplerCreator::Sample*> unused_Samples{};
	void* samplerFiles;
};







//sample_prop_files* file_sp = NULL;












