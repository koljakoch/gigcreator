---
title: gigCreator - help
---

# Introduction
gigCreator will help you creating GIGA-Sample-Instruments out of a bunch of wav-files. It is written in c++ and started as a wrapper for libgig’s wav2gig but now has everything build in.\
An example for what gigCreator can do for you is shown in [Example 3](#example-3).

# Requirements
- libgig (<a href="http://linuxsampler.org" target="_blank">linuxsampler.org</a>)
- wxWidgets (<a href="http://wxwidgets.org" target="_blank">wxwidgets.org</a>)

# Features
- Create multiple gig-files at once
- Multiple Instruments per gig
- Assign information from the filenames to all available gig-dimensions
- Combine mono-wav-files directly into (deinterleaved) stereo-samples (by using Dimension -> Samplechannel)
- `Expand ranges` with a minimum of pitch, so created instruments are seamless
- Preview structure of to be created gigs before saving them
- Infos that are already stored in the wav-files (e.g. basenote, loop), are retained, though some of them can optionally be overwritten
- Several options for individual parameters like note-signs
- Sample-groups per Instrument

# Work-flow
1. Select a folder on the left, decide if you want to find wav-files in it recursively and if you want the corresponding file-paths (relative to the selected folder) to be displayed. If so, these paths can be used in the patterns.
2. Check available specifiers in the upper right box and change/add them as needed (see *[Specifiers](#specifiers)*).
3. Enter pattern(s) that the wav-files will be scanned against (see *[Patterns](#patterns)*).
For your convenience, a preview is shown next to the available specifiers, as soon as you select one of the wav-files
4. All gig-files that are to be created are listed in a tree-structure and can be examined.
5. Hit `Expand ranges` in the Instruments' info if you want the ranges to be expanded, so a seamless instrument is created from the lowest note to the highest.
6. When no name for the gig-file or instrument is given, they will be called `unnamed`. Still, they can be renamed by clicking on their name in the tree-structure.

# <a name="patterns"></a> Patterns
Currently, patterns are entered in the central upper text-field. One pattern per line.
These patterns are scanned in the order they are entered:

>     Pattern_1
>     Pattern_2
>     Pattern_3

Files will be scanned against `Pattern_1`, invalid results will be scanned against `Pattern_2` and so on, so it is a good advice to enter the most specific pattern first.

## Delimiters

Any string of characters, that is part of the filename and not relevant for gathering information, can be entered and will be used as a delimiter. The same behavior can be achieved by using a specifier of type `Ignore`. See [Examples](#examples) below.

# <a name="specifiers"></a> Specifiers

The specifiers entered in the pattern are used to identify the corresponding value in the filename.
Their structure is:

`%[n][specifier]`

where `[n]` is an (optional) number of characters to use and `[specifier]` the character used in the pattern. If no number `[n]` is given, the algorithm tries to guess an appropriate length.

The upper right box will show available specifiers. You can (de-)activate, change or delete them or define and add new ones.
While entering patterns, the rightmost column will show gathered values from the selected wav-file.

## Available specifier-types:

|  SP-Type | Description 	                                 |
| -------: |	:------------------------------              |
| Name 				| Name of the gig-file to be created         |
| Instrumentname 	| Name of the instrument to be created   |
| Notenumber 		| used to identify notenumbers             |
| Notename 			| used to identify notenames <br>(useful, if neither files' waf-info nor filenames contain notenumbers and notes are given like `F#3` in the filename)                       |
| Dimension 		| Select a gig-dimension in the next column. For available dimensions, see <a href="https://download.linuxsampler.org/doc/libgig/api/namespacegig.html#af9f1af3eb2a77df5fc7d0f56b3f13d3d" target="_blank">here</a>                                   |
| Ignore 			| Found Values will be ignored               |

### Specifier-Options
Some specifier-types offer options. If so, a button will appear on the right of the specifier-type. For exmaple, `Notename` lets you enter characters for the note-signs. This is useful, if your files don't contain any midi-notenumber-information in neither the wav-info itself, nor in the filename. So for a file named `Piano-F#2.wav` you would enter a '`#`' as sharp-sign here. The corresponding midi-note-number will then be calculated out of this information.

### Dimension

This specifier-type will let you select one of libgig's dimensions for these specifiers. This could be e.g. `Velocity` if your filenames contain corresponding information.

### Values

Choose `use found value` if you want to just use the found value as is, e.g. `64` for velocity dimension. When instead selecting `use T-matrix`, a button will appear where, by pressing it, you can enter a list of translations by assigning a string (e.g. `MezzoForte`) to a corresponding value (e.g. `101`).

# Options
Under `File -> Options` you'll get a dialog to enter some default options. These can be saved. A file called `.gigCreator.cfg` will be created in your `home`-directory.
This will also contain the pattern, as currently entered in the Scan-pattern-text-field.
Additionally, another configuration-file called `.SamplerCreatorSpecOpt.cfg` is saved in your `home`-directory when hitting the `Scan`-button. This contains all defined specifiers and definitions as of the Specifiers' box.

# <a name="examples"></a> Examples

Here are some examples. Except for **[Example 3](#example-3)**, they all use the following

##### Specifiers:

|   	| SP-type  	| Dimension | Values |
| --: 	| --- 		| --- 		| --- |
| %n 	| Name 		| 			| use found value 	|
| %m 	| Instrumentname |		| use found value 	|
| %v 	| Dimension | Velocity 	| use found value	|
| %r 	| Notenumber| 			| use found value	|
| %a 	| Notename 	| 			| use found value	|
| %s 	| Dimension | Samplechannel | use T-matrix
| %i 	| Ignore	|			| 					|

### Example 1
> |           |                                         |
> | ----      | ----                                    |
> | Filename: | `Name - Instrument - 64 - c1`           |
> | Pattern:  | `%n - %m - %v - %a`                     |


##### Result:
> |           |                                         |
> | ----      | ----                                    |
> | Name:     |`Name`                                   |
> | Instrumentname:| `Instrument`|
> | Dimension Velocity:| `64` |
> | Notename: | `C1`|
> | Note: | `36` (calculated from notename, but only used if no info is found in wav-file)|

### Example 2

> |           |                                         |
> | ----      | ----                                    |
> | Filename: | `PREFIXNameName2 - 127 - 23 – LPOSTFIX` |
> | Pattern:  | `PREFIX%4n%m - %v - %r - %1s%i`         |


##### Result:
> |                          |         |
> | ----                     | ----    |
> | Name:                    | `Name`  |
> | Instrumentname:              | `Name2` |
> | Dimension Velocity:      | `127`   |
> | Midi-Note-Nr:            | `23`    |
> | Dimension Samplechannel: |`0` (if entered so in corresponding T-matrix) |

### <a name="example-3"></a> Example 3

![](doc/gigCreator_screenshot_Tmatrix.png "Example 3")

This example shows how a complex filename-structure may be scanned.
Notice, that for this instrument, only the first line in the Scan-patterns' box is necessary (`%2r-%p%v%1d%m`). The other lines are for wav-files of a completely different filename-structure but don't disturb here, since all available files are already processed by the first line.

The opened `T-matrix`-dialog for entering `Dimension Velocity` demonstrates how this will help converting articulation information from the filename (`Forte` in this case) into specific values for that dimension. `Dimension - Sustain pedal` implements another such T-matrix for `PedalOn` and `PedalOff`.

Note that, as there are no wav-infos in these files and the `notenumbers` given in the filenames start with `1`, I entered a transpose-value in the `Notenumber`-Options (`+20`). This is applied to all samples and is noticed in the the samples' info box on the lower right.

3520 files were processed by hitting `Scan`.

The gig's name (`Ivy-Piano`) was entered afterwards since it is not found in the filenames.

The result is one gig-file (named `Ivy-Piano.gig`) containing two instruments (`Ambient` and `Close`), each with 88 regions (one per key), each containing 4 dimensions (`Sustain Pedal`, `Samplechannel`, `Velocity` and `Random`). This will be created and saved when hitting `Create Gigs`.
