<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>gigCreator - help</title>
  <style>
    html {
      line-height: 1.5;
      font-family: Georgia, serif;
      font-size: 20px;
      color: #1a1a1a;
      background-color: #fdfdfd;
    }
    body {
      margin: 0 auto;
      max-width: 36em;
      padding-left: 50px;
      padding-right: 50px;
      padding-top: 50px;
      padding-bottom: 50px;
      hyphens: auto;
      overflow-wrap: break-word;
      text-rendering: optimizeLegibility;
      font-kerning: normal;
    }
    @media (max-width: 600px) {
      body {
        font-size: 0.9em;
        padding: 1em;
      }
    }
    @media print {
      body {
        background-color: transparent;
        color: black;
        font-size: 12pt;
      }
      p, h2, h3 {
        orphans: 3;
        widows: 3;
      }
      h2, h3, h4 {
        page-break-after: avoid;
      }
    }
    p {
      margin: 1em 0;
    }
    a {
      color: #1a1a1a;
    }
    a:visited {
      color: #1a1a1a;
    }
    img {
      max-width: 100%;
    }
    h1, h2, h3, h4, h5, h6 {
      margin-top: 1.4em;
    }
    h5, h6 {
      font-size: 1em;
      font-style: italic;
    }
    h6 {
      font-weight: normal;
    }
    ol, ul {
      padding-left: 1.7em;
      margin-top: 1em;
    }
    li > ol, li > ul {
      margin-top: 0;
    }
    blockquote {
      margin: 1em 0 1em 1.7em;
      padding-left: 1em;
      border-left: 2px solid #e6e6e6;
      color: #606060;
    }
    code {
      font-family: Menlo, Monaco, 'Lucida Console', Consolas, monospace;
      font-size: 85%;
      margin: 0;
    }
    pre {
      margin: 1em 0;
      overflow: auto;
    }
    pre code {
      padding: 0;
      overflow: visible;
      overflow-wrap: normal;
    }
    .sourceCode {
     background-color: transparent;
     overflow: visible;
    }
    hr {
      background-color: #1a1a1a;
      border: none;
      height: 1px;
      margin: 1em 0;
    }
    table {
      margin: 1em 0;
      border-collapse: collapse;
      width: 100%;
      overflow-x: auto;
      display: block;
      font-variant-numeric: lining-nums tabular-nums;
    }
    table caption {
      margin-bottom: 0.75em;
    }
    tbody {
      margin-top: 0.5em;
      border-top: 1px solid #1a1a1a;
      border-bottom: 1px solid #1a1a1a;
    }
    th {
      border-top: 1px solid #1a1a1a;
      padding: 0.25em 0.5em 0.25em 0.5em;
    }
    td {
      padding: 0.125em 0.5em 0.25em 0.5em;
    }
    header {
      margin-bottom: 4em;
      text-align: center;
    }
    #TOC li {
      list-style: none;
    }
    #TOC a:not(:hover) {
      text-decoration: none;
    }
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header id="title-block-header">
<h1 class="title">gigCreator - help</h1>
</header>
<nav id="TOC" role="doc-toc">
<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#requirements">Requirements</a></li>
<li><a href="#features">Features</a></li>
<li><a href="#work-flow">Work-flow</a></li>
<li><a href="#patterns"><a name="patterns"></a> Patterns</a>
<ul>
<li><a href="#delimiters">Delimiters</a></li>
</ul></li>
<li><a href="#specifiers"><a name="specifiers"></a> Specifiers</a>
<ul>
<li><a href="#available-specifier-types">Available specifier-types:</a>
<ul>
<li><a href="#specifier-options">Specifier-Options</a></li>
<li><a href="#dimension">Dimension</a></li>
<li><a href="#values">Values</a></li>
</ul></li>
</ul></li>
<li><a href="#options">Options</a></li>
<li><a href="#examples"><a name="examples"></a> Examples</a>
<ul>
<li><a href="#example-1">Example 1</a></li>
<li><a href="#example-2">Example 2</a></li>
<li><a href="#example-3"><a name="example-3"></a> Example 3</a></li>
</ul></li>
</ul>
</nav>
<h1 id="introduction">Introduction</h1>
<p>gigCreator will help you creating GIGA-Sample-Instruments out of a bunch of wav-files. It is written in c++ and started as a wrapper for libgig&#x2019;s wav2gig but now has everything build in.<br />
An example for what gigCreator can do for you is shown in <a href="#example-3">Example 3</a>.</p>
<h1 id="requirements">Requirements</h1>
<ul>
<li>libgig (<a href="http://linuxsampler.org" target="_blank">linuxsampler.org</a>)</li>
<li>wxWidgets (<a href="http://wxwidgets.org" target="_blank">wxwidgets.org</a>)</li>
</ul>
<h1 id="features">Features</h1>
<ul>
<li>Create multiple gig-files at once</li>
<li>Multiple Instruments per gig</li>
<li>Assign information from the filenames to all available gig-dimensions</li>
<li>Combine mono-wav-files directly into (deinterleaved) stereo-samples (by using Dimension -&gt; Samplechannel)</li>
<li><code>Expand ranges</code> with a minimum of pitch, so created instruments are seamless</li>
<li>Preview structure of to be created gigs before saving them</li>
<li>Infos that are already stored in the wav-files (e.g.&#xA0;basenote, loop), are retained, though some of them can optionally be overwritten</li>
<li>Several options for individual parameters like note-signs</li>
<li>Sample-groups per Instrument</li>
</ul>
<h1 id="work-flow">Work-flow</h1>
<ol type="1">
<li>Select a folder on the left, decide if you want to find wav-files in it recursively and if you want the corresponding file-paths (relative to the selected folder) to be displayed. If so, these paths can be used in the patterns.</li>
<li>Check available specifiers in the upper right box and change/add them as needed (see <em><a href="#specifiers">Specifiers</a></em>).</li>
<li>Enter pattern(s) that the wav-files will be scanned against (see <em><a href="#patterns">Patterns</a></em>). For your convenience, a preview is shown next to the available specifiers, as soon as you select one of the wav-files</li>
<li>All gig-files that are to be created are listed in a tree-structure and can be examined.</li>
<li>Hit <code>Expand ranges</code> in the Instruments&#x2019; info if you want the ranges to be expanded, so a seamless instrument is created from the lowest note to the highest.</li>
<li>When no name for the gig-file or instrument is given, they will be called <code>unnamed</code>. Still, they can be renamed by clicking on their name in the tree-structure.</li>
</ol>
<h1 id="patterns"><a name="patterns"></a> Patterns</h1>
<p>Currently, patterns are entered in the central upper text-field. One pattern per line. These patterns are scanned in the order they are entered:</p>
<blockquote>
<pre><code>Pattern_1
Pattern_2
Pattern_3</code></pre>
</blockquote>
<p>Files will be scanned against <code>Pattern_1</code>, invalid results will be scanned against <code>Pattern_2</code> and so on, so it is a good advice to enter the most specific pattern first.</p>
<h2 id="delimiters">Delimiters</h2>
<p>Any string of characters, that is part of the filename and not relevant for gathering information, can be entered and will be used as a delimiter. The same behavior can be achieved by using a specifier of type <code>Ignore</code>. See <a href="#examples">Examples</a> below.</p>
<h1 id="specifiers"><a name="specifiers"></a> Specifiers</h1>
<p>The specifiers entered in the pattern are used to identify the corresponding value in the filename. Their structure is:</p>
<p><code>%[n][specifier]</code></p>
<p>where <code>[n]</code> is an (optional) number of characters to use and <code>[specifier]</code> the character used in the pattern. If no number <code>[n]</code> is given, the algorithm tries to guess an appropriate length.</p>
<p>The upper right box will show available specifiers. You can (de-)activate, change or delete them or define and add new ones. While entering patterns, the rightmost column will show gathered values from the selected wav-file.</p>
<h2 id="available-specifier-types">Available specifier-types:</h2>
<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 79%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: right;">SP-Type</th>
<th style="text-align: left;">Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">Name</td>
<td style="text-align: left;">Name of the gig-file to be created</td>
</tr>
<tr class="even">
<td style="text-align: right;">Instrumentname</td>
<td style="text-align: left;">Name of the instrument to be created</td>
</tr>
<tr class="odd">
<td style="text-align: right;">Notenumber</td>
<td style="text-align: left;">used to identify notenumbers</td>
</tr>
<tr class="even">
<td style="text-align: right;">Notename</td>
<td style="text-align: left;">used to identify notenames <br>(useful, if neither files&#x2019; waf-info nor filenames contain notenumbers and notes are given like <code>F#3</code> in the filename)</td>
</tr>
<tr class="odd">
<td style="text-align: right;">Dimension</td>
<td style="text-align: left;">Select a gig-dimension in the next column. For available dimensions, see <a href="https://download.linuxsampler.org/doc/libgig/api/namespacegig.html#af9f1af3eb2a77df5fc7d0f56b3f13d3d" target="_blank">here</a></td>
</tr>
<tr class="even">
<td style="text-align: right;">Ignore</td>
<td style="text-align: left;">Found Values will be ignored</td>
</tr>
</tbody>
</table>
<h3 id="specifier-options">Specifier-Options</h3>
<p>Some specifier-types offer options. If so, a button will appear on the right of the specifier-type. For exmaple, <code>Notename</code> lets you enter characters for the note-signs. This is useful, if your files don&#x2019;t contain any midi-notenumber-information in neither the wav-info itself, nor in the filename. So for a file named <code>Piano-F#2.wav</code> you would enter a &#x2018;<code>#</code>&#x2019; as sharp-sign here. The corresponding midi-note-number will then be calculated out of this information.</p>
<h3 id="dimension">Dimension</h3>
<p>This specifier-type will let you select one of libgig&#x2019;s dimensions for these specifiers. This could be e.g.&#xA0;<code>Velocity</code> if your filenames contain corresponding information.</p>
<h3 id="values">Values</h3>
<p>Choose <code>use found value</code> if you want to just use the found value as is, e.g.&#xA0;<code>64</code> for velocity dimension. When instead selecting <code>use T-matrix</code>, a button will appear where, by pressing it, you can enter a list of translations by assigning a string (e.g.&#xA0;<code>MezzoForte</code>) to a corresponding value (e.g.&#xA0;<code>101</code>).</p>
<h1 id="options">Options</h1>
<p>Under <code>File -&gt; Options</code> you&#x2019;ll get a dialog to enter some default options. These can be saved. A file called <code>.gigCreator.cfg</code> will be created in your <code>home</code>-directory. This will also contain the pattern, as currently entered in the Scan-pattern-text-field. Additionally, another configuration-file called <code>.SamplerCreatorSpecOpt.cfg</code> is saved in your <code>home</code>-directory when hitting the <code>Scan</code>-button. This contains all defined specifiers and definitions as of the Specifiers&#x2019; box.</p>
<h1 id="examples"><a name="examples"></a> Examples</h1>
<p>Here are some examples. Except for <strong><a href="#example-3">Example 3</a></strong>, they all use the following</p>
<h5 id="specifiers-1">Specifiers:</h5>
<table>
<thead>
<tr class="header">
<th style="text-align: right;"></th>
<th>SP-type</th>
<th>Dimension</th>
<th>Values</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">%n</td>
<td>Name</td>
<td></td>
<td>use found value</td>
</tr>
<tr class="even">
<td style="text-align: right;">%m</td>
<td>Instrumentname</td>
<td></td>
<td>use found value</td>
</tr>
<tr class="odd">
<td style="text-align: right;">%v</td>
<td>Dimension</td>
<td>Velocity</td>
<td>use found value</td>
</tr>
<tr class="even">
<td style="text-align: right;">%r</td>
<td>Notenumber</td>
<td></td>
<td>use found value</td>
</tr>
<tr class="odd">
<td style="text-align: right;">%a</td>
<td>Notename</td>
<td></td>
<td>use found value</td>
</tr>
<tr class="even">
<td style="text-align: right;">%s</td>
<td>Dimension</td>
<td>Samplechannel</td>
<td>use T-matrix</td>
</tr>
<tr class="odd">
<td style="text-align: right;">%i</td>
<td>Ignore</td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
<h3 id="example-1">Example 1</h3>
<blockquote>
<table>
<tbody>
<tr class="odd">
<td>Filename:</td>
<td><code>Name - Instrument - 64 - c1</code></td>
</tr>
<tr class="even">
<td>Pattern:</td>
<td><code>%n - %m - %v - %a</code></td>
</tr>
</tbody>
</table>
</blockquote>
<h5 id="result">Result:</h5>
<blockquote>
<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Name:</td>
<td><code>Name</code></td>
</tr>
<tr class="even">
<td>Instrumentname:</td>
<td><code>Instrument</code></td>
</tr>
<tr class="odd">
<td>Dimension Velocity:</td>
<td><code>64</code></td>
</tr>
<tr class="even">
<td>Notename:</td>
<td><code>C1</code></td>
</tr>
<tr class="odd">
<td>Note:</td>
<td><code>36</code> (calculated from notename, but only used if no info is found in wav-file)</td>
</tr>
</tbody>
</table>
</blockquote>
<h3 id="example-2">Example 2</h3>
<blockquote>
<table>
<tbody>
<tr class="odd">
<td>Filename:</td>
<td><code>PREFIXNameName2 - 127 - 23 &#x2013; LPOSTFIX</code></td>
</tr>
<tr class="even">
<td>Pattern:</td>
<td><code>PREFIX%4n%m - %v - %r - %1s%i</code></td>
</tr>
</tbody>
</table>
</blockquote>
<h5 id="result-1">Result:</h5>
<blockquote>
<table>
<tbody>
<tr class="odd">
<td>Name:</td>
<td><code>Name</code></td>
</tr>
<tr class="even">
<td>Instrumentname:</td>
<td><code>Name2</code></td>
</tr>
<tr class="odd">
<td>Dimension Velocity:</td>
<td><code>127</code></td>
</tr>
<tr class="even">
<td>Midi-Note-Nr:</td>
<td><code>23</code></td>
</tr>
<tr class="odd">
<td>Dimension Samplechannel:</td>
<td><code>0</code> (if entered so in corresponding T-matrix)</td>
</tr>
</tbody>
</table>
</blockquote>
<h3 id="example-3"><a name="example-3"></a> Example 3</h3>
<p><img src="doc/gigCreator_screenshot_Tmatrix.png" title="Example 3" /></p>
<p>This example shows how a complex filename-structure may be scanned. Notice, that for this instrument, only the first line in the Scan-patterns&#x2019; box is necessary (<code>%2r-%p%v%1d%m</code>). The other lines are for wav-files of a completely different filename-structure but don&#x2019;t disturb here, since all available files are already processed by the first line.</p>
<p>The opened <code>T-matrix</code>-dialog for entering <code>Dimension Velocity</code> demonstrates how this will help converting articulation information from the filename (<code>Forte</code> in this case) into specific values for that dimension. <code>Dimension - Sustain pedal</code> implements another such T-matrix for <code>PedalOn</code> and <code>PedalOff</code>.</p>
<p>Note that, as there are no wav-infos in these files and the <code>notenumbers</code> given in the filenames start with <code>1</code>, I entered a transpose-value in the <code>Notenumber</code>-Options (<code>+20</code>). This is applied to all samples and is noticed in the the samples&#x2019; info box on the lower right.</p>
<p>3520 files were processed by hitting <code>Scan</code>.</p>
<p>The gig&#x2019;s name (<code>Ivy-Piano</code>) was entered afterwards since it is not found in the filenames.</p>
<p>The result is one gig-file (named <code>Ivy-Piano.gig</code>) containing two instruments (<code>Ambient</code> and <code>Close</code>), each with 88 regions (one per key), each containing 4 dimensions (<code>Sustain Pedal</code>, <code>Samplechannel</code>, <code>Velocity</code> and <code>Random</code>). This will be created and saved when hitting <code>Create Gigs</code>.</p>
</body>
</html>
