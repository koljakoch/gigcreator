# gigCreator

A tool written in C++ for generating GIGA-Sampler files out of a bunch of wav-files.

## Description
gigCreator will create ready-to-play gig-files out of your wav-files collection. This is done by entering a pattern, selected files will be scanned against. These patterns will extract informations, such as gig-names or Instruments names, out of the filenames or -paths of the selected wav-files-directory. You can thereby create several gig-files containing multiple instruments.
A preview will show you, what will be created.
Some parameters my be changed in the options.

## Features
- Scan wav-filenames for information like e.g. midi-note-number and use them for creating gig-files.
- Define your own specifiers for all available gig-dimensions. E.g. velocity, pedal, random, etc
	- Define a 'translate-matrix' for translating found values into midi-values. E.g.: `MezzoForte` -> `101`
- multiple instruments per gig
- Optionally expand ranges to fill gaps between samples (with minimal pitch-correction)
- merge two mono-files into one stereo-sample (if the corresponding pattern-specifier is given)
- Transpose midi-note-numbers
- preview of gig-files to be created
- samples are grouped by instruments in gig file

See also [help.html](help.md)

## Screenshot
![screenshot](doc/gigCreator_screenshot.png)

## Installation
gigCreator is developed on linux, Archlinux that is. Theoretically it should also work on windows or Mac, but since I don't own these operating systems, I cannot tell if this really is the case. I'd very much appreciate any input from those, kind and keen enough to try to compile it and let me know the outcome. I will help, if I can.

### Requirements
- libgig [(linuxsampler.org)](https://linuxsampler.org/)
	- On Archlinux, this would be `libgig` or `libgig-svn` packages
	- On Ubuntu: `libgig` (didn't test this, yet)
- wxwidgets [(wxwidgets.org)](https://www.wxwidgets.org/)
	- On Archlinux: `wxgtk3`
	- On Ubuntu: something like `libwxgtk3`

### Procedure
- Clone the repository using\
`git clone https://gitlab.com/koljakoch/gigcreator.git`.

- `cd` into the source-directory and do the usual

      ./configure
      make
      [sudo] make install
- start `gigCreator` from command-line.

## Status

- add start-menu item, currently start only via commandline: `gigCreator`

## Support
Politely mail me at progATkoljakoch.de
